'use strict';

var sio = {};
var socket;
var callbacks;

var pingTimer,
	//// sioのacknowledgeがタイムアウトするとserverがsocketをdisconnectするので再接続を定期的にチェックするためのtimer
	reconTimer;
var isConnect;

sio.setup = function () {
	callbacks = {};
	socket = io.connect('http://localhost:'+location.port);

	isConnect = false;

	socket.on('connect'   , function () {
		isConnect = true;

		console.log('connected!');
		startCheckToReconnect();

		//// ETIMEDOUT error対策
		//// superagentのtimeout設定効果を確認するためコメントアウト
		// stopPing();
		// startPing();
	});
	socket.on('disconnect', function (client) {
		isConnect = false;
		// stopPing();
		console.log('disconnected');
	});


	// socket.on('event', function (data) {
	socket.on('event', function (data, ack) {

		//// add acknowledge callback to server(17.11.25)
		if (ack) ack();

		var callees = callbacks[ data.event ];
		if (callees) {
			for(var i=0; i<callees.length; i++) {
				callees[i]( data.param );
			}
		}
	});
}

sio.sendReq = function (to, param) {
	socket.emit('notice', {to: to, param: param});
}

sio.on = function (name, cb) {
	if (!callbacks[name]) {
		callbacks[name] = [];
	}
	callbacks[name].push(cb);

	console.log('[siohelper]('+name+') callback num = '+callbacks[name].length);
}

function startPing () {
	pingTimer = setTimeout(() => {
		$.get('/ping', function (pong) {
			// console.log(pong);
			startPing();
		});
	}, 5000);
}

function stopPing () {
	if (pingTimer) {
		clearTimeout(pingTimer);
	}
}

function startCheckToReconnect () {
	stopCheckToReconnect();
	reconTimer = setTimeout(() => {
		if ( !isConnect && socket ) {
			socket.connect();
			console.log('socketを再接続しました');
			return;
		}
		startCheckToReconnect();
	}, nodeParam.SIO_RECONNECTION_CHECK_INTERVAL_MS);
}

function stopCheckToReconnect () {
	if ( reconTimer ) clearTimeout(reconTimer);
}

//------------ shortcut

/*call from client.js*/

sio.scrapeNow = function () {
	sio.sendReq('agent', {op:'scrapeNow', param: null});
}

sio.reScrape = function (secName) {
	sio.sendReq('agent', {op:'reScrape', param: secName});
}

sio.nmDebugOff = function (secName) {
	sio.sendReq('agent', {op:'nmDebugOff', param: secName});
}
