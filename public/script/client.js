'use strict';

/**
  - <script>タグでcommon/utilをグローバル変数nodeUtilで読み込んでいる
    common/utilでグローバル変数名を指定している
  - 同じくcommon/paramをnodeParamで読み込んでいる
 */

var gfs;
var graphers;
var ui;

$(function(){
	init();
});

function init () {

	sio.setup();
	ui.setup();

	sio.on('message', function (data) {
		/**
		 data.levelにはserverサイド(www)でisPrimaryフラグ(メインclient向けか否か)が格納している
		 */
		if ( data.level ) console.log(data.message);
	});

	/**
	 * APP立ち上げ時, Reload時（※）, scrape時に通る
	 * ※　ループ対策
	 *    このイベントはagent.init()内でも呼び出される
	 *    → Reload時にagent.init()が実行されるとループしてしまう
	 *    → ループしないようにwwwでagent.init()は初期アクセス時のみに制御
	 *     （Reload時はagent.init()は実行されない）
	 */
	sio.on('reload', function (res) {

		// var type = res.reloadType;
		// var isAcceptReload = false;

		// switch (type) {
		// 	case 'init':
		// 	//// 初期アクセス時のみ
		// 	isAcceptReload = $('#user').length==0;
		// 	break;

		// }

		// if ( isAcceptReload ) {
			location.reload(true);
		// }
	});

/*
	sio.on('fetch', function (gfArray) {

	});

	sio.on('getStockInfo', function (data) {
		grapher.update(grapher.getGraphDataPeriod(data));
	});

	sio.on('getLifetimeProfit', function(profit) {
		console.log('update lifetime profit');
		ui.updateLifetimeProfit(profit);
	});
*/

}

/*
function checkLifetimeProfit () {
	var val = $('.lifetime-profit').text();
	if ( val == '-' ) {
		sio.getLifetimeProfit();
	}
}
*/