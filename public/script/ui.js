'use strict';

var ui = {};

ui.setup = function () {
	setupButtons();
}

function setupButtons () {

	$('button.scrape-now').click(function () {
		if ( confirm('Scrape Now?') ) {
			sio.scrapeNow();
		}
	});

	$('td.status').click(function () {
		var sec = $(this).attr('data-sec');
		if ( confirm('Re-scrape '+sec+'?') ) {
			sio.reScrape(sec);
		}
	});

	$('button.nm-debug-off').click(function () {
		if ( nodeParam.NM_DISPLAY ) {
			alert('nm表示を止めるにはparam.NM_DISPLAY=OFF設定が必要です');
		}
		// else {
			var sec = $(this).attr('data-sec');
			sio.nmDebugOff(sec);
		// }
	});

}

function toggleBackgroundColor ($elem, type) {
	var types = 'bg-primary bg-secondary bg-success bg-danger bg-warning bg-info bg-light bg-dark bg-white';
	$elem
	.removeClass(types)
	.addClass(type);
}
