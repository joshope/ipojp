#!/bin/sh

case $1 in
	'-start')
		pm2 delete ipojp
		#nodebrew exec v6.10.2 -- pm2 start npm --name 'ipojp' -- start
		# nodebrew exec v6.10.2 -- pm2 start pm2.json
		nodebrew exec v14.16.1 -- pm2 start pm2.json
		
		pm2 logs ipojp
		;;
	'-delete')
		# nodebrew exec v6.10.2 -- pm2 delete ipojp
		nodebrew exec v14.16.1 -- pm2 delete ipojp
		;;
	# pm2のdeleteコマンドがあるので-killオプションは不要
	'-kill')
		pid=$(lsof -n -i:3002 | grep 'node' | awk 'NR==1 {print $2}')
		#todo 複数app起動しているので3003ポートもkill必要
		#pid=$(lsof -n -i:3002 -i:3003 | grep 'node' | awk '{print $2}')
		if [ ! $pid ] ; then
			echo '---------------------------------'
			echo ' no process running in port 3002 '
			echo '---------------------------------'
		else
			echo '-------------------------'
			echo ' close port 3002 process '
			echo '-------------------------'
			kill $pid
		fi
		;;
esac

exit 0