'use strict';

/**
 reference document
 https://www.npmjs.com/package/node-google-calendar
 */

var CalendarAPI = require('node-google-calendar'),
    CONFIG      = require('../cred/calendar-setting'),
    util        = require('../common/util'),
    param       = require('../common/param'),    
    logger      = require('./logger')('calendar');


var	calendarId  = CONFIG.calendarId.ipojp
var cal;

function Calendar () {
	makeInstance();
	this.promise = Promise.resolve();
}

function makeInstance () {
	if (cal) return; // singleton

	cal = new CalendarAPI(CONFIG);

}

Calendar.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);
	return this;
	/*
	 catchは呼び出し側で実装
	 cal.catch(err=>{somethingToDo});
	*/	
}

Calendar.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

/**
　指定期間のCalendarイベントを取得
  引数: ['2017-05-20T06:00:00+08:00', '2018-12-25T22:00:00+08:00']
 */
Calendar.prototype.fetchEvents = function (period/*[String]*/) {

	return this.then(() => {
		return new Promise((resolve, reject) => {

			if ( typeof period !== 'object' || period.length !==2 ) {
				reject('不正な引数----> '+period);
				return;
			}

			var timeMin = period[0],
				timeMax = period[1];

			var params = {
				timeMin: timeMin,
				timeMax: timeMax,
				q: '', // search keyword
				singleEvents: true,
				orderBy: 'startTime'
			}; 	//Optional query parameters referencing google APIs

			cal.Events.list(calendarId, params)
			.then(calItems => {
				var items = []; 
				//// APPで追加したイベントのみ抽出
				for (var i=0; i<calItems.length; i++) {
					var item = calItems[i];
					if ( !util.isIpoEvent(item) ) continue;
					items.push(item);
				}
				resolve(items);
			})
			.catch(err => {
				reject(err);
			});
		});
	});
}

/**
 指定した銘柄オブジェクトをGoogle Calendarイベントに追加
 引数: {	name: '銘柄名',
    	code: '2971',
		rank: 'S',
		bookBillDate: { start: '01/28', end: '02/01' },
		purchaseDate: { start: '02/05', end: '02/08' }
	   }
 */
Calendar.prototype.addEvent = function (ipoObject/*Object*/) {

	return this.then(() => {
		return new Promise((resolve, reject) => {

			if ( typeof ipoObject !== 'object' ) {
				reject('不正な引数----> '+ipoObject);
				return;
			}

			var summary = ipoObject.name+'('+ipoObject.code+') '+'【'+ipoObject.rank+'】';
			var mainSec = ipoObject.mainSec;
			var secInfo = /sbi/i.test(mainSec)? '☆SBI主幹事' : mainSec;
			summary = summary + ' ' + secInfo;

			var start   = util.getCalendarDateFormat(ipoObject.bookBillDate.start);
			var end     = util.getCalendarDateFormat(ipoObject.bookBillDate.end);
			var tag     = param.CAL_DESC_TAG; // カレンダイベント取得用：APPから追加したEventの識別子(descriptionに追記)
			var link    = '<a href="'+ipoObject.url+'">銘柄記事</a>';
			var description = '購入申込: '+ipoObject.purchaseDate.start+' ~ '+ipoObject.purchaseDate.end
												+'\n'+tag+'\n'+link+'\n'+'主幹事：'+mainSec;

			var color;
			try {
				color   = param.CAL_EVENT_COLORS[ipoObject.rank];
			} catch (err) {
				color  = 1;
				logger.e(err);
			}

			var event = {
				'start': {'dateTime': start},
				'end'  : {'dateTime': end},
				'location': '',
				'summary': summary,
				'status': 'confirmed',
				'description': description,
				'colorId': color
			};

			cal.Events.insert(calendarId, event)
			.then(resp => {
				resolve(resp);
				logger.d('inserted event:');
				logger.i(resp);
			})
			.catch(err => {
				reject(err);
				logger.e('Error: insertEvent-' + err.message);
				logger.w(event);
			});
		})
	})


}

module.exports = Calendar;

