'use strict';

var socketio = require('socket.io'),
	logger   = require('./logger')('sio'),
	param    = require('../common/param');

var sockets;
var primaryID;
var callbacks = {};

var ackTimer;

exports.setup = function (server, cb){

	sockets = {};
	primaryID = null;

	socketio(server).on('connection', function (socket) { // Reload時はここからスタート

		//// 常にsocketは1本となるように変更(21.08.10)
		Object.keys(sockets).forEach(key => {
			// if ( socket.id == sockets[key].id ) return; // continue
			logger.w('socket重複回避のためdisconnect ----> '+sockets[key].id);
			sockets[key].disconnect();
			delete sockets[key];
		});

		logger.w('connect client ----> socketID: '+socket.id);
		if ( Object.keys(sockets).length == 0 ) primaryID = socket.id;

		sockets[socket.id] = socket;
		if (cb) cb( socket.id==primaryID );

		logger.w('socket num: '+Object.keys(sockets).length);

		socket.on('disconnect', function () {
			logger.w('disconnect client ----> socketID: '+socket.id);
			if (socket.id==primaryID) {
				//// 全て初期化
				sockets = {};
				primaryID = null;
			} else {
				delete sockets[socket.id];
			}
		});

		socket.on('notice', function (data) {

			//// callbackは各moduleで最大1つ
			var callee = callbacks[ data.to ];

			if (!callee) {				
				logger.e('callback is not registered: '+data.to);
				return;
			}

			if (data.param && data.param.op) {
				logger.w('send to '+data.to+' (op: '+data.param.op+')');
			} else {
				logger.w('send to '+data.to);
			}

			callee(data.param);

		});
	});
}

exports.sendMessage = function (msg, level, isToAll){
	level = level | 'normal';
	//// primaryソケット(client:メインwindow)のみに送信
	var keys = Object.keys(sockets);
	for (var i=0; i<keys.length; i++) {
		if ( !isScoketExist(keys[i]) ) continue;
		if ( !isToAll && keys[i]!=primaryID) continue; 
		sockets[keys[i]].emit('event', {
			event: 'message',
			param: { message: msg, level: level}
		});
	}
}

exports.sendEvent = function (name, param, isToAll) {
	var keys = Object.keys(sockets);
	for (var i=0; i<keys.length; i++) {
		if ( !isScoketExist(keys[i]) ) continue;
		if ( !isToAll && keys[i]!=primaryID) continue;

		// add Acknowledge callback from client(17.11.25)
		startAckTimer();
		sockets[keys[i]].emit('event', {event: name, param: param}, function () {
			logger.d('response from client in time!');
			stopAckTimer();
		});
	}
}

exports.on = function (to, cb) {
	//// callbackはmodule毎に最大1つ
	callbacks[to] = cb;
}
exports.unbind = function(to) {
	delete callbacks[to];
	logger.w('unbind callback : '+to);
}

/**
 * sioのacknowledge用timer
 */
function startAckTimer () {
	stopAckTimer();
	ackTimer = setTimeout(() => {
		var socket = getPrimarySocket();
		if ( socket ) {
			logger.e('clientから反応がtimeoutしたのでsocketを切断します ----> '+socket.id);
			socket.disconnect();
		} else {
			logger.e('clientから反応がtimeoutしたのでsocketを切断します');
		}
		//// 初期化
		sockets = {};
		primaryID = null;
	}, param.SIO_ACK_TIMEOUT_MS);
}

function stopAckTimer () {
	if ( ackTimer ) clearTimeout(ackTimer);
}

function isScoketExist (socketID) {
	if ( sockets[socketID] ) {
		return true;
	} else {
		logger.e('socketが存在しません(windowをreloadして下さい)');
		return false;
	}
}

function getPrimarySocket () {
	if ( !primaryID ) return null;
	return sockets[primaryID];
}
