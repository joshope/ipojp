'use strict'

/**
 nightmareのevaluateはブラウザスコープなのでscraping先のコンテンツにjqueryがない場合はローカルのjqueryを使用
 jqueryはトップ階層に置く必要がある
 ローカルのjqueryを使用する場合、evaluateの直前に.inject('js', jquery)を記述
 */
var jquery = 'jquery-3.2.0.slim.min.js';

var Nightmare = require('nightmare'),
	param     = require('../common/param'),
	// secInfo   = require('../cred/user').sec[param.SEC_NAME],
	logger    = require('./logger')('iposite'),
	// mongo     = require('./mongo')(),
	util      = require('../common/util');

/**
 * nightmareでiframeを使用するための設定(1)
 * iframe内のボタンクリック等に必要
 */
// require('nightmare-iframe-manager')(Nightmare);

/**
 * nightmareでpopupを操作するための設定
 * https://github.com/rosshinkley/nightmare-window-manager
 */
// require('nightmare-window-manager')(Nightmare);
 

var nightmare;
var ipoItem;
// var userId;
// var popupWindowId;


function IpoSite () {
	// userId = __userId;
	makeNightmareInstance();
	this.promise = Promise.resolve();

}

function makeNightmareInstance () {

	if (nightmare) return; // singleton

	nightmare = Nightmare({
		show   : param.NM_DISPLAY,
		width  : param.NM_W,
		height : param.NM_H,

		/**
		 * nightmareでiframeを使用するための設定(2)
		 * この設定でiframe内のscraping(read)ができるようになる
		 */
		// webPreferences: {
		// 	webSecurity: false,
		// },

 	});
}

IpoSite.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);	
	return this;
}

IpoSite.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

IpoSite.prototype.open = function (__ipoItem) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {

			ipoItem = __ipoItem;

			var url = ipoItem.url;

			if ( !util.isValidUrl(url) ) {
				reject('IPO銘柄記事のURLが正しくありません');
			}

			nightmare
		    // .windowManager() // popup window操作に必要(nightmare-window-manager)
			.goto(url)
			.then(() => {
				resolve();
			})
			.catch(err => {
				reject(err);
			});
		});
	});
}

//// 主幹事情報の取得
IpoSite.prototype.getMainSec = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(()=> {
				var row = $('.kobetudate04 th:contains("主幹事")');
				if ( row.length ) { // 要素が存在する場合
					return row.next('td').text();
				} else {
					return null;
				}
			})
			.then(mainSec => {
				ipoItem.mainSec = mainSec;
				resolve(mainSec);
			})
			.catch(err => {
				reject(err);
			});
		});
	});
}


module.exports = IpoSite;
