'use strict';

const Monex  = require('./include/monex-core'),
	  logger = require('./logger')('monex'),
	  param  = require('../common/param'),
	  util   = require('../common/util');

let monex,
	userId;
let cb;
let isSuccess; // スクレイプ成功フラグ

const SEC_NAME = 'monex';

exports.init = async (__userId, isDisp/*option:nm表示*/, isDebug/*option:nmデバッグモード*/, __cb/*option*/) => {
	userId = __userId;
	if ( monex ) { // singleton (coreのpuppeteerを破棄して再生成)
		await monex.close();
		logger.w('---- close puppeteer and recreate monex obect');
		monex = new Monex(userId, isDisp, isDebug);
		// await monex.init();
		if (__cb) __cb();
	} else {
		monex = new Monex(userId, isDisp, isDebug);
		// await monex.init();
		if (__cb) __cb();
	}
}

exports.start = async (__cb) => {
	cb = __cb;
	if ( monex.isValid ) {
		try {
			await monex.open();
			await monex.login();
			await monex.checkNotice();
			await monex.fetchMoney();
			await monex.gotoIpo();
			await monex.fetchAlreadyBookBilled();
			const items = await monex.searchIpo();
			await fetchDetail(items);
			const isSuccess = await scrapeIpo();
			if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});
		} catch (err) {
			logger.e(err);
			if (cb) cb({secName:SEC_NAME, isSuccess:false});
		}
	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}
	await monex.close();
}

async function fetchDetail(items) {
	for (let item of items) {
		if ( !item.enable ) continue;
		await monex.fetchDetail(item.code);
	}

	logger.i('[monex証券] ブックビル可能な銘柄詳細データ取得が完了しました');
	logger.w(monex.getItems());

	//// ブックビルターゲットの抽出
	monex.extractBookBillTarget();
	// scrapeIpo();
}

async function scrapeIpo() {
	const items = monex.getItemsToBookBill();
	isSuccess = true; // 初期化

	if ( !util.isExist(items) && util.isExist(monex.getErrors()) ) {
		logger.w('[monex証券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;
	}

	for (let item of items) {
		logger.w('[monex証券] ブックビル申込します ----> '+item.name+' ('+item.code+')');

		let status = await monex.checkProspectus(item);
		logger.w('[monex証券] checkProspectus status ----> '+status);
		isSuccess = isSuccess & status;

		status = await monex.clickBookBill();
		logger.w('[monex証券] clickBookBill status ----> '+status);
		isSuccess = isSuccess & status;

		status = await monex.inputBookBill();
		logger.w('[monex証券] inputBookBill status ----> '+status);
		isSuccess = isSuccess & status;
		// if ( !status ) logger.w('ブックビル入力に失敗しています ----> '+item.name+' ('+item.code+')');

		status = await monex.clickConfirm(item.code);
		logger.w('[monex証券] clickConfirm status ----> '+status);
		isSuccess = isSuccess & status;
	}

	if ( !items.length ) {
		logger.i('[monex証券] ブックビルできるアイテムはありません');
	} else {
		logger.i('[monex証券] scrapingが完了しました');
	}
	return isSuccess;
}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return monex.getBookBilled();
}

exports.getResults = function () {
	return monex.getResults();
}

exports.getErrors = function () {
	return monex.getErrors();
}

exports.getNotes = function () {
	return monex.getNotes();
}

exports.isValid = function () {
	return monex.isValid;
}

exports.isDisp = function () {
	return monex.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}


