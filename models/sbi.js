'use strict';

var Sbi    = require('./include/sbi-core'),
	// mailer = require('./mailer'),
	logger = require('./logger')('sbi'),
	param  = require('../common/param'),
	util   = require('../common/util');


var sbi,
	userId;

var cb;

var SEC_NAME = 'sbi';

var isSuccess; // スクレイプ成功フラグ

exports.init = function (__userId, isDisp/*option:nm表示*/, isDegub/*option:nmデバックモード*/, __cb/*option*/) {
	userId = __userId;
	/* sbi obj */
	// if ( !sbi ) { // singleton
	// 	sbi = new Sbi(userId);
	// }

	if ( sbi ) { // singleton (coreのnmを破棄して再生成)
		sbi
		.end()
		.then(() => {
			logger.w('---- close nm and recreate sbi obect');
			sbi = new Sbi(userId, isDisp, isDegub);
			if (__cb) __cb();
		});
	} else {
		sbi = new Sbi(userId, isDisp, isDegub);
		if (__cb) __cb();
	}


	// mailer.init();

}

exports.start = function (__cb) {

	cb = __cb;

	if ( sbi.isValid ) {
		sbi
		.open()
		.login(userId)
		.then(() => {
			if ( param.CHECK_NOTICE ) {
				scrapeNotice();
			} else {
				scrapeIpo();
			}
		})
		.catch(err => {
			logger.e(err);
			scrapeIpo();
		});

	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		// if (cb) cb(true);
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}

}


function scrapeNotice () {

	sbi
	// .open()
	// .login(userId)
	.gotoHome()
	.openNoticeWindow()
	.searchNotice()
	.then(num => {

		confirmNotice(num);

	})
	.catch(err => {
		logger.e(err);
		var msg = '[SBI証券] 重要なお知らせの確認に失敗しました: scrapeNotice()';
		// mailer.send( msg+'<br>'+util.getUserName(userId, 'sbi') );
		logger.w(msg);

		scrapeIpo(); //// お知らせ確認をスキップして先に進む(todo:popupにフォーカスしてしまっているか確認)

	});

}


function confirmNotice (num) {

	if ( !num ) {
		logger.i('[SBI証券] 確認が必要な重要なお知らせはありません');
		sbi
		.closeNoticeWindow()
		.then(() => {


			scrapeIpo();


		})
		.catch(err => {
			logger.e(err);
			var msg = '[SBI証券] 重要なお知らせのWindowクローズに失敗しました(重要件数0件)';
			// mailer.send( msg+'<br>'+util.getUserName(userId) );
			logger.w(msg);

			scrapeIpo(); //// お知らせ確認をスキップして先に進む(todo:popupにフォーカスしてしまっているか確認)

		});
		return;
	}

	logger.w('[SBI証券] '+num+'件の重要なお知らせがあります');

	var ques = [];
	for (var i=0; i<num; i++) {
		ques.push(
			sbi
			.openNoticeItem()
			.confirmNoticeItem()
			.then(title => {
				logger.w('[SBI証券] 重要なお知らせ----> '+ title);
			})
			.closeNoticeItem()
			.catch(err => {
				logger.e(err);
			})
		);
	}

	Promise.all(ques)
	.then( results => {
		logger.i('[SBI証券] 重要なお知らせの確認が完了しました');

		sbi
		.closeNoticeWindow()
		.then(() => {


			scrapeIpo();


		})
		.catch(err => {
			logger.e(err);
			var msg = '[SBI証券] 重要なお知らせのWindowクローズに失敗しました(処理は続行します)';
			// mailer.send( msg+'<br>'+util.getUserName(userId, 'sbi') );
			logger.w(msg);

			scrapeIpo(); //// お知らせ確認をスキップして先に進む(todo:popupにフォーカスしてしまっているか確認)

		})

	})
	.catch(err => {
		logger.e(err);
		var msg = '[SBI証券] 重要なお知らせの確認に失敗しました(処理は続行します): confirmNotice()';
		// mailer.send( msg+'<br>'+util.getUserName(userId, 'sbi') );
		logger.w(msg);

		scrapeIpo(); //// お知らせ確認をスキップして先に進む(todo:popupにフォーカスしてしまっているか確認)

	});

}



function scrapeIpo () {

	// bookBilled = [];
	sbi
	// .open()
	// .login(userId)
	.gotoIpo()
	.getChallengePoint()
	.then(point => {
		logger.i('[SBI証券] '+point);
	})
	.searchIpo()
	.then(__items => {
		// items = __items; // sbi.jsに移動
		logger.i(__items);
		bookBill();
	})
	.catch(err => {
		logger.e(err);
		bookBill();
	});	
}

/**
 ブックビルできるアイテムがあれば実行
*/
function bookBill () {

	// bookBilled.push( util.findItem('7039', items) );
	// bookBilled.push( util.findItem('7038', items) );
	// mailer.report(bookBilled, null, userId);

	var ques = [];
	// var isSuccess = true;
	isSuccess = true; // 初期化

	// var items = sbi.getItems();
	var items = sbi.getItemsToBookBill();

	// if ( !items || !items.length ) {
	if ( !util.isExist(items) && util.isExist(sbi.getErrors()) ) {
		// logger.w('[SBI証券] アイテムがありません');
		// return;
		logger.w('[SBI証券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;

	}


	// var enable_num = 0;
	for (var i=0; i<items.length; i++) {
		var item = items[i];

		// if ( item.enable ) {
			// enable_num++;
			logger.d('[SBI証券] ブックビルできます----> '+item.name);

			ques.push(
				sbi
				.gotoIpo()
				.clickBookBill()
				.then(code => {
					logger.w('[SBI証券] ブックビル申込します ----> '+code);
					/* todo: clickConfirm後に移動 */
					/* sbi.js内での処理に変更 */
					// bookBilled.push( util.findItem(code, items) );
				})
				.inputBookBill()
				.clickConfirm()
				.then(status => {
					logger.w('[SBI証券] bookBill status ----> '+status);
					isSuccess = isSuccess & status;
				})
				.catch(err => {
					logger.e(err);
				})
			);
		// }
	}

	// if ( !enable_num ) {
	// 	var msg = '[SBI] ブックビルできるアイテムはありません'; 
	// 	logger.w(msg);
	// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'sbi') );
	// 	mailer.report(null/*bookBilled*/, '【SBI証券】'/*subjectHeader*/, userId, 'sbi'/*証券会社名*/, getResults()/*抽選結果items*/);
	// 	return;
	// }

	Promise.all(ques)
	.then( results => {
		if ( !ques.length ) {
			logger.i('[SBI証券] ブックビルできるアイテムはありません');
		} else {
			logger.i('[SBI証券] scrapingが完了しました');
		}

		// if ( isSuccess ) {
		// 	var bookBilled = sbi.getBookBilled();
		// 	mailer.report(bookBilled, '【SBI証券】'/*subjectHeader*/, userId, 'sbi'/*証券会社名*/, sbi.getResults()/*抽選結果items*/);
		// } else {
		// 	var msg = '[SBI証券] ブックビルに失敗している可能性があります';
		// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'sbi') );
		// 	mailer.send( msg+'<br>'+util.getUserName(userId, true/*isAka*/) );
		// }

		// if (cb) cb( isSuccess );
		if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});

	})
	.catch(err => {
		logger.e(err);
		// if (cb) cb( false );
		if (cb) cb({secName:SEC_NAME, isSuccess:false});

	});

}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return sbi.getBookBilled();
}

exports.getResults = function () {
	return sbi.getResults();
}

exports.getErrors = function () {
	return sbi.getErrors();
}

exports.getNotes = function () {
	return sbi.getNotes();
}

exports.isValid = function () {
	return sbi.isValid;
}

exports.isDisp = function () {
	return sbi.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}


