'use strict';

var Smbc = require('./include/smbc-core'),
	// mailer = require('./mailer'),
	logger = require('./logger')('smbc'),
	param  = require('../common/param'),
	util   = require('../common/util');

var smbc,
	userId;

var cb;

var SEC_NAME = 'smbc';

var isSuccess; // スクレイプ成功フラグ

exports.init = function (__userId, isDisp/*option:nm表示*/, isDebug/*option:nmデバッグモード*/, __cb/*option*/) {
	userId = __userId;

	// if ( !smbc ) { // singleton
	// 	smbc = new Smbc(userId);
	// }

	if ( smbc ) { // singleton (coreのnmを破棄して再生成)
		smbc
		.end()
		.then(() => {
			logger.w('---- close nm and recreate smbc obect');
			smbc = new Smbc(userId, isDisp, isDebug);
			if (__cb) __cb();
		});
	} else {
		smbc = new Smbc(userId, isDisp, isDebug);
		if (__cb) __cb();
	}

	// mailer.init();
}


exports.start = function (__cb) {

	cb = __cb;


	if ( smbc.isValid ) {
		smbc
		.open()
		.login(userId)
		.then(() => {
			logger.i('finished login to smbc');
		})
		.fetchMoney()
		.then(money => {
			logger.w('------money----- '+money);
		})
		.gotoIpo()
		.fetchAlreadyBookBilled()
		.then(alreadyBookBilled => {
			logger.w(alreadyBookBilled);
		})
		.searchIpo()
		.then(items => {
			logger.w(items);

			scrapeIpo();

		})
		.catch(err => {
			logger.e(err);

			scrapeIpo();
		});

	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		// if (cb) cb(true);
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}

}


function scrapeIpo() {
	var items = smbc.getItemsToBookBill();

	var ques = [];
	// var isSuccess = true;
	isSuccess = true; // 初期化

	if ( !util.isExist(items) && util.isExist(smbc.getErrors()) ) {
		logger.w('[SMBC証券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;
	}

	// var enable_num = 0;
	for (var i=0; i<items.length; i++) {
		var item = items[i];

		// if (item.enable) { // smbc-coreでチェック済みのため不要(itemsToBookBillはenableのみ)
			// enable_num++;
			logger.d('[SMBC証券] ブックビルできます----> '+item.name+' ('+item.code+')');

			ques.push(
				smbc
				.gotoIpo()
				.clickBookBill(item.code)
				.then(code => {
					logger.w('[SMBC証券] ブックビル申込します ----> '+code);
				})

				.checkTerms()
				.inputBookBill(item)
				.clickConfirm()
				// .checkProspectus()
				// .confirmApplication()
				// .executeApplication()
				.then(status => {
					logger.w('[SMBC証券] bookBill status ----> '+status);
					isSuccess = isSuccess & status;
				})

				.catch(err => {
					logger.e(err);
				})
			);
		// }
	}

	// if ( !enable_num ) {
	// 	var msg = '[SMBC証券] ブックビルできるアイテムはありません'; 
	// 	logger.w(msg);
	// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'smbc') );
	// 	mailer.report(null/*bookBilled*/, '【SMBC証券】'/*subjectHeader*/, userId, 'smbc'/*証券会社名*/, getResults()/*抽選結果items*/);
	// 	return;
	// }

	Promise.all(ques)
	.then( results => {
		if ( !ques.length ) {
			logger.i('[SMBC証券] ブックビルできるアイテムはありません');
		} else {
			logger.i('[SMBC証券] scrapingが完了しました');
		}
		
		// if ( isSuccess ) {
		// 	var bookBilled = smbc.getBookBilled();
		// 	mailer.report(bookBilled, '【SMBC証券】'/*subjectHeader*/, userId, 'smbc'/*証券会社名*/, smbc.getResults()/*抽選結果items*/);
		// } else {
		// 	var msg = '[SMBC証券] ブックビルに失敗している可能性があります';
		// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'smbc') );
		// 	mailer.send( msg+'<br>'+util.getUserName(userId, true/*isAka*/) );
		// }

		// if (cb) cb( isSuccess );
		if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});

	})
	.catch(err => {
		logger.e(err);
		// if (cb) cb( false );
		if (cb) cb({secName:SEC_NAME, isSuccess:false});
	});
}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return smbc.getBookBilled();
}

exports.getResults = function () {
	return smbc.getResults();
}

exports.getErrors = function () {
	return smbc.getErrors();
}

exports.getNotes = function () {
	return smbc.getNotes();
}

exports.isValid = function () {
	return smbc.isValid;
}

exports.isDisp = function () {
	return smbc.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}

