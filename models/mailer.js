'use strict';

var	nodemailer = require('nodemailer'),
	mailInfo   = require('../cred/user').mail,
	param      = require('../common/param'),
	util       = require('../common/util'),
	logger     = require('./logger')('mailer');

var smtp;

exports.init = function () {
	setupMailer();
}

function setupMailer () {
	if ( !smtp ) { // singleton
		smtp = nodemailer.createTransport({
			host: 'smtp.gmail.com',
			port: 465,
			secure: true,
			auth: {
				user: mailInfo.user,
				pass: mailInfo.password
			}
		});
	}
}

exports.send = function (message, __subjectHeader/*option*/, isText/*option*/) {
	var subjectHeader = param.TEST_MODE? '[TEST] ' : '';
	subjectHeader = __subjectHeader? subjectHeader+__subjectHeader : subjectHeader;

	var mailOptions = {
        from   : param.REPORT_SENDER,
        to     : param.REPORT_RECEIVERS.join(','),
        subject: subjectHeader+param.REPORT_SUBJECT+' '+util.getCurrentDateString(),
        //text   : message
        //html   : '<b>Hello world</b>'
    };

    if (isText) {
    	mailOptions.text = message;
    } else {
    	mailOptions.html = message;
    }

    smtp.sendMail(mailOptions, function(err, info){
    	if(err){
    		logger.e(err);
    	}else{
    		logger.i('レポートメールを送信しました: ' + info.response);
    	}
    });
}

exports.reportAll = function (summary, isDispErr/*option*/) {

	if ( !summary ) {
		logger.e('サマリデータがありません');
		return;
	}

	var space = '&nbsp;';

	var message = '<b>ユーザ</b><br><br>'+summary.user+'<br><br>';

	Object.keys(summary).forEach(key => {

		//// 証券会社名プロパティ以外は処理除外
		var exceptKeys = ['user', 'date'];

		if ( exceptKeys.includes(key) ) return; // continue
		
		message += '<hr>';

		var sec = summary[key];

		//// 証券会社名
		var secName;
		switch( key ) {
			case 'sbi':
			secName = 'SBI証券';
			break;
			case 'smbc':
			secName = 'SMBC証券';
			break;
			case 'nomura':
			secName = '野村證券';
			break;
			case 'monex':
			secName = 'Monex証券';
			break;
			default:
			var msg = '証券会社が判別できません ----> '+key; 
			logger.e(msg);
			secName = msg;
		}

		message += '<b>'+secName+'</b>';

		//// ステータス
		var status = sec.isSuccess? space+'[成功]' : '<span style="color:red;">'+space+'[失敗]</span>';
		message += status+'<br><br>';

		//// ブックビル銘柄
		message += '<b>■ブックビル銘柄</b><br><br>';
		if ( util.isExist(sec.bookBilled) ) {
			sec.bookBilled.forEach(item => {
				message += item.name +'<br>';
			});

		} else {
			message += 'なし<br>';
		}

		//// 備考
		if ( util.isExist(sec.notes) ) {
			message += '<br><b>■抽選結果</b><br><br>';
			sec.notes.forEach(msg => {
				message += '<span style="color:orange;">' + '*' + space + msg + '</span><br>';
			});
		}

		//// 抽選結果
		message += '<br><b>■抽選結果</b><br><br>';
		if ( util.isExist(sec.results) ) {
			sec.results.forEach(item => {
				var result = item.result;
				// if ( /当選/.test(result) && /なし/.test(result)==false ) {
				if ( /落選/.test(result)==false ) {
					message += '<span style="color:green;"><strong>' + item.result + '</strong></span> : ' + item.name + '<br>';
				} else {
					message += '<span><strong>' + item.result + '</strong></span> : ' + item.name + '<br>';
				}

			});

		} else {
			message += 'なし<br>';
		}

		message += '<br>';

		//// デバッグ情報(エラー情報)
		if ( isDispErr ) {
			message += '<b>■デバッグ情報</b><br><br>';
			if ( util.isExist(sec.errors) ) {
				sec.errors.forEach(msg => {
					// message += '*' + space + msg +'<br>';
					message += '<span style="color:red;">' + '*' + space + msg + '</span><br>';
				});

			} else {
				message += 'なし<br>';
			}
		}

		message += '<br>';

	});

	this.send(message);

}

/**
 * DEPRECATED
 */
exports.report = function (bookBilled/*item array*/, subjectHeader/*option*/, userId/*option*/, secName/*option*/, results/*option, item array*/) {

	var message = '<b>ユーザ</b><br><br>';
	// var user = util.getUserName(userId, secName);
	// var user = util.getUserName(userId, secName, true/*isAka*/);
	var user = util.getUserName(userId, true/*isAka*/);

	user = user? user : 'N/A';
	message = message + user+'<br><br>';

	if ( util.isExist(bookBilled) ) {
		message = message + '<b>ブックビル銘柄</b><br><br>';
		for (var i=0; i<bookBilled.length; i++) {
			var item = bookBilled[i];
			message = message + item.name +'<br>';
		}

	} else {
		message = message + '<b>ブックビル銘柄</b><br><br>なし<br><br>';
	}

	if ( util.isExist(results) ) {
		message = message + '<br><br><b>抽選結果</b><br><br>';
		for (var i=0; i<results.length; i++) {
			var item = results[i];
			// message = message + item.name +' : '+item.result+'<br>';
			message = message + '<b>' + item.result + '</b> : ' + item.name + '<br>';
		}

	} else {
		message = message +'<br><br><b>抽選結果</b><br><br>なし';
	}

	this.send(message, subjectHeader);
}


// exports.report = function (bookBilled/*item array*/, subjectHeader/*option*/, userId/*option*/, secName/*option*/) {
// 	if ( !util.isExist(bookBilled) ) return;
// 	var user = util.isExist(userId)? util.getUserName(userId, secName): null;
// 	var message = user? user+'<br>': '';
// 	var message = message + 'ブックビルディング銘柄<br><br>';
// 	for (var i=0; i<bookBilled.length; i++) {
// 		var item = bookBilled[i];
// 		message = message + item.name +'<br>';
// 	}
// 	this.send(message, subjectHeader);
// }


// function makeReport (order) {
// 	var rep;
// 	switch (order.deal) {
// 		case 'buy':
// 		rep = '買い注文<br>';
// 		break;
// 		case 'sell':
// 		rep = '売り注文<br>';
// 		break;

// 		case 'losscut':
// 		rep = 'ロスカット注文<br>';
// 		break;
// 	}

// 	rep = rep +
// 		'価格: '+order.price+'<br>' +
// 		'数量: '+order.dealQty+'<br>' +
// 		'日時: '+util.getDateString(order.date)+'<br>';

// 	return rep;
// }



