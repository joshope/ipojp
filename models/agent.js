'use strict';

var sio       = require('./sio'),
	logger    = require('./logger')('agent'),
	param     = require('../common/param'),
	util      = require('../common/util'),
	// mailer    = require('./mailer'),
	// Sbi       = require('./sbi'),
	sbi       = require('./sbi'),
	// Nomura    = require('./nomura'),
	nomura    = require('./nomura'),
	smbc      = require('./smbc'),
	monex     = require('./monex'),
	scheduler = require('./scheduler'),
	mailer    = require('./mailer'),
	// helper = require('./agenthelper');
	fs        = require('fs'),
	fse  = require('fs-extra');


// var gfs  = [],	// 取引銘柄 (pugで読み込むためここで初期化)
var userId;
// var sbi;
// var nomura;
var timer;
// var items,
// 	bookBilled;
var backupPath;

var summary; // mailer用サマリ

//// 使用する証券会社オブジェクト
var secs = [monex, smbc, sbi, nomura];

//// clientリロード時は通らないようにwwwで制御している
exports.init = async function (instanceId) {


	/**************
	 * 動作チェック
	/**************/
	// async function injectJQuery (page) {
	// 	await page.addScriptTag({ path: 'jquery-3.2.0.slim.min.js' }); // ルートDIRのファイルが読み込まれる
	// }

	// const puppeteer = require('puppeteer');
	// const secInfo   = require('../cred/user').sec['monex'];
	// (async () => {
	// 	const browser = await puppeteer.launch({headless: false});
	// 	const page = await browser.newPage();
	//     await page.setViewport({width: param.NM_W, height:param.NM_H});
	// 	await page.goto('https://www.monex.co.jp/');
	// 	await page.waitForSelector('input#loginid');
	// 	await page.type('input#loginid', secInfo.users[0].username);
	// 	await page.type('input#passwd',  secInfo.users[0].password);
	// 	await page.click('button.btn_login_top');
	// 	await page.waitForSelector('span.user-info');
	// 	await injectJQuery(page);
	// 	await page.evaluate(() => {
	// 		$('a:contains("新規公開株")')[0].click();
	//     });
	// 	await page.waitForTimeout(param.NM_WAIT*2);
	// 	await page.evaluate(() => {
	// 		$('a:contains("プログリット")')[0].click();
	//     });
	// 	await page.waitForTimeout(param.NM_WAIT*2);
	// 	await page.evaluate(() => {
	// 		$('a:contains("目論見書")')[0].click();
	//     });

	//     ////---- popupウィンドウで目論見書確認
	//     const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
	//     const popup = await newPagePromise;
	//     await popup.waitForTimeout(param.NM_WAIT*2);
	//     ////---- popupウィンドウはframe構造のため操作するframeを取得
	//     const frame = popup.frames().find(frame => frame.name() === 'CT');
	// 	await frame.evaluate(() => {
	// 		$('a:contains("確認しました"), a:contains("閉じる")')[0].click();
	//     });
	// })();
	// return;


	/* NodeインスタンスIDをwwwから受け取ってユーザIDとして扱う */
	userId = instanceId;

	// logger.e(util.getUserName(userId, true));
	// return;

	mailer.init();

	sio.on('agent', function (req) {
		helper.handleOperation(req);
	});

	/* mongoDBのバックアップディレクトリ設定は複数app起動時も1つのユーザだけで行う */
	if (userId==0) {
	 	setupBackupPath();
	}

	// /* sbi obj */
	// if ( !sbi ) { // singleton
	// 	sbi = new Sbi(userId);
	// }

	//---- forEachはasync-awaitが使えない（無視される）
	// secs.forEach(sec => {
	// 	sec.init(userId, param.NM_DISPLAY/*isDisp*/);
	// });
	for (let sec of secs) {
		await sec.init(userId, param.NM_DISPLAY/*isDisp*/);
	}

	// sbi.init(userId);
	// nomura.init(userId);
	// smbc.init(userId);
	// monex.init(userId);


	/* Mailer setup	*/
	// mailer.init();

	/* sio setting */
	sio.on('agent', function (req) {
		handleOperation(req);
	});	

	/* Scheduler */
	scheduler.init();

	/* Timer */
	if ( param.USE_TIMER ) {
		startTimer();
	} else {
		startScraping();
		startScheduler();
	}

	initSummary();

	//// clientリロード(初期設定完了、clientにuser名を表示させる)
	//// Timerなどを確実に作動させるため、初期設定の最後に実行
	////（agent.initはpp立ち上げ時のみ実行され、Reloadでは通らない）
	reload();

}

exports.getBackupPath = function () {
	return backupPath;
}


function initSummary () {
	summary = {
		user: util.getUserName(userId,true),
		// monex: {}, // {isSuccess:Boolean, bookBilled:[], results:[], errors:[], notes:[], isDisp:Boolead, url:String}
		// smbc:  {},
		// sbi:   {},
		// nomura:{},
		date: null, // スクレイプ完了日時
	};

	secs.forEach(sec => {
		summary[sec.getSecName()] = {};
		summary[sec.getSecName()].isValid = sec.isValid();
	});

}

function startTimer () {
	stopTimer();
	var interval_min = util.getIntervalMin();
	logger.i('次回のチェック.... '+interval_min+'分後');

	timer = setTimeout(function () {
		if ( util.isInTime(null, param.CHECK_OPEN, param.CHECK_CLOSE) ) {
			startScraping();
			startScheduler();
			startTimer(); // interval設定を十分に取ること
		} else {
			logger.i('wait to scrape....');
			startTimer();
		}
	}, interval_min*60*1000);
}

function stopTimer () {
	if ( timer ) {
		clearTimeout(timer);
		timer = null;
	}
}

function startScheduler () {
	//// google Calendarへの登録チェックは複数app起動時も1つのユーザだけで行う
	if (userId==0) {
		scheduler.start();
	}
}



function startScraping (secObjArray/*option*/) {
	// monex.start();
	// smbc.start();
	// sbi.start();
	// nomura.start();

	//// 特定の証券会社が指定された場合はRescrapeと判定
	var isRescrape = secObjArray? true: false;

	var __secs = secObjArray || secs;

	// summary = {
	// 	user: util.getUserName(userId,true),
	// 	// monex: {}, // {isSuccess:Boolean, bookBilled:[], results:[], errors:[]}
	// 	// smbc:  {},
	// 	// sbi:   {},
	// 	// nomura:{},
	// 	date: null, // スクレイプ完了日時
	// };

	// // secs.forEach(sec => {
	// secs.forEach(sec => {
	// 	summary[sec.getSecName()] = {};
	// })

	var ques = [];

	// secs.forEach(sec => {
	__secs.forEach(sec => {
		ques.push(
			new Promise((resolve, reject) => {
				sec
				.start(res => {
					logger.w('finished ----> '+res.secName+' ('+res.isSuccess+')');
					// summary[res.secName].isSuccess = res.isSuccess;
					resolve();
				});
			})
		);
	});

	// var ques = [
	// 	new Promise((resolve, reject) => {
	// 		monex
	// 		.start(isSuccess => {
	// 			logger.w('finished monex!');
	// 			summary.monex.isSuccess = isSuccess;
	// 			resolve();
	// 		});
	// 	}),
	// 	new Promise((resolve, reject) => {
	// 		smbc
	// 		.start(isSuccess => {
	// 			logger.w('finished smbc!');
	// 			summary.smbc.isSuccess = isSuccess;
	// 			resolve();
	// 		});
	// 	}),
	// 	new Promise((resolve, reject) => {
	// 		sbi
	// 		.start(isSuccess => {
	// 			logger.w('finished sbi!');
	// 			summary.sbi.isSuccess = isSuccess;
	// 			resolve();
	// 		});
	// 	}),
	// 	new Promise((resolve, reject) => {
	// 		nomura
	// 		.start(isSuccess => {
	// 			logger.w('finished nomura!');
	// 			summary.nomura.isSuccess = isSuccess;
	// 			resolve();
	// 		});
	// 	}),
	// ];

	Promise
	.all(ques)
	.then(__results => {
		logger.w('finished all');

		secs.forEach(sec => {

			if ( sec.isValid() ) {
				summary[sec.getSecName()].bookBilled = sec.getBookBilled();
				summary[sec.getSecName()].results    = sec.getResults();
				summary[sec.getSecName()].errors     = sec.getErrors();
				summary[sec.getSecName()].notes      = sec.getNotes();
				summary[sec.getSecName()].isSuccess  = sec.isSuccess();
				summary[sec.getSecName()].isDisp     = sec.isDisp(); // nmが表示状態であればtrue
				summary[sec.getSecName()].url        = util.getTopUrl(sec.getSecName()); // 証券会社URL
				// summary[sec.getSecName()].isValid    = sec.isValid(); // initSummaryで設定済み(index.pugで表示判定に使用)
				
			} else {
				//// 証券口座がないユーザの場合はプロパティを削除
				delete summary[sec.getSecName()];
			}

		});

		summary.date = util.getCurrentDateString();

		logger.i(summary);

		if ( !isRescrape ) {
			mailer.reportAll(summary, true/*isDispErr*/);
		}

		reload(); // clientリロード

	});

	
}

async function handleOperation (req) {
	var op = req.op;

	switch (op) {

		case 'scrapeNow':
		/* timerを無視して直ちにscrape開始 */
		startScraping();
		startScheduler();
		startTimer(); // interval設定を十分に取ること
		break;

		case 'reScrape':
		var secName = req.param,
		    isDisp  = param.NM_DISPLAY_AT_RESCRAPE,
		    isDebug = isDisp && param.NM_DEBUG_AT_RESCRAPE; // nm表示が前提条件

		switch (secName) {
			case 'sbi':
			sbi.init(userId, isDisp, isDebug);
			startScraping([sbi]);
			break;
			case 'nomura':
			await nomura.init(userId, isDisp, isDebug);
			startScraping([nomura]);
			break;
			case 'smbc':
			smbc.init(userId, isDisp, isDebug);
			startScraping([smbc]);
			break;
			case 'monex':
			await monex.init(userId, isDisp, isDebug);
			startScraping([monex]);
			break;
			default:
			logger.e('証券会社名が判別できません ----> '+secName);
			break;
		}
		break;

		case 'nmDebugOff':
		var secName = req.param;
		var sec;
		switch (secName) {
			case 'sbi':
			sec = sbi;
			break;
			case 'nomura':
			sec = nomura;
			break;
			case 'smbc':
			sec = smbc;
			break;
			case 'monex':
			sec = monex;
			break;
			default:
			logger.e('証券会社名が判別できません ----> '+secName);
			break;
		}
		if (sec) {
			sec.init(userId, param.NM_DISPLAY/*isDisp:nm表示*/, false/*isDebug:nmデバッグモード*/, function () {
				//// client更新 (index.pugはsummaryでDOM生成している)
				summary[sec.getSecName()].isDisp = sec.isDisp();
				reload();
			});
		}
		break;

		default:
		logger.e('登録されていないoperation ----> '+ op);
		break;

	}	
}

function setupBackupPath() {
	var root = param.DB_BACKUP_DIR.path;
	fs.stat(root, (err, stats) => {
		if (err) {
	    	if (err.code === 'ENOENT') {
	      		var alt = param.DB_BACKUP_DIR.alt;
	      		if ( alt ) {
	      			logger.w('バックアップDIRが存在しません。代替DIRで置き換えます---->'+alt);
		      		backupPath = alt + '/' + param.DB_BACKUP_SUBDIR;
		      		ensurePath();
	      		} else {
		      		logger.e('バックアップディレクトリが存在しません。パスを確認して下さい---->'+root);
	      		}
		    } else {
				logger.e(err);
			}
		} else {
      		backupPath = root + '/' + param.DB_BACKUP_SUBDIR;
      		ensurePath();
		}
	});

	var ensurePath = function () {
		//// ディレクトリがなければ作成
		fse.ensureDir(backupPath)
		.then(() => {
			//// 存在していてる場合もここを通る
			logger.i('backup path checked : '+backupPath);
		})
		.catch(err => {
			logger.e(err);
		});
	};
}

// function reload(reloadType) {
function reload() {
	// sio.sendEvent('reload', {reloadType:reloadType});
	sio.sendEvent('reload', {}/*reserved*/);
}

exports.getSummary = function () {
	return summary || {user:util.getUserName(userId,true)}; // userプロパティはcliend.pugで使用するためデフォルト付与
}



