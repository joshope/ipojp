var log4js        = require('log4js'),
    log4js_extend = require('log4js-extend');

function makeInstance (tag) {
	var logger = log4js.getLogger(tag);

	//// メソッドのショートカット
	logger.m = logger.mark;  // 通常のconsole.log (log4js_extendも無効)
	logger.t = logger.trace;
	logger.d = logger.debug;
	logger.i = logger.info;
	logger.w = logger.warn;
	logger.e = logger.error;
	logger.f = logger.fatal;

	return logger;
}

module.exports = makeInstance;

/*
log4jsのフォーマット指定
	(作者ページ)
	https://github.com/nomiddlename/log4js-node
	https://github.com/nomiddlename/log4js-node/wiki/Layouts

	例)  pattern: "[%r] [%[%5.5p%]] - %m%n"

   	%r - time in toLocaleTimeString format
	%p - log level
	%c - log category
	%h - hostname
	%m - log data
	%d - date in various formats
	%% - %
	%n - newline
	%x{<tokenname>} - add dynamic tokens to your log. Tokens are specified in the tokens parameter
	%[ and %] - define a colored bloc

	(その他参考 日本語)
	http://modalsoul.github.io/report/2012/10/14/node-js-log4js/
*/
log4js.configure({
   //// ログ出力のフォーマット
	appenders: { trade: { type: 'console', layout: { type: 'pattern', pattern: '%r - %[[%c] %m%]' } } },
	categories: { default: { appenders: ['trade'], level: 'debug' } }
});

/*
log4jsに行番号等を追加
	(作者ページ)
	https://github.com/ww24/log4js-node-extend

	@name : 関数名
	@file : ファイル名
	@line : 行番号
	@column : 列番号

	例) format: "at @name (@file:@line:@column)"
*/
log4js_extend(log4js, {
	path: __dirname,
	format: "(at @name @file:@line:@column)"
});



/*

	Usage :
	var logger = require('./models/include/logger')('[app]');

	color set :
	   black
	   red
	   green
	   yellow
	   blue
	   magenta
	   cyan
	   white
	   gray
   	   grey

	https://github.com/Marak/colors.js

*/


// var colors = require('colors');

// colors.setTheme({
//     info    : 'green',
//     debug   : 'blue',
//     warn    : 'yellow',
//     error   : 'red',
//     silly   : 'rainbow',
//     input   : 'grey',
//     verbose : 'cyan',
//     prompt  : 'grey',
//     data    : 'grey',
//     help    : 'cyan'
// });

// function makeInstance (tag) {
// 	var instance = new Logger (tag);
// 	return instance;
// }

// function Logger (tag) {
// 	this._tag_ = '['+tag+'] ';
// }

// Logger.prototype.i = function (msg) {
// 	console.log ( colors.info (this._tag_ + msg) );
// }

// Logger.prototype.d = function (msg) {
// 	console.log ( colors.debug (this._tag_ + msg) );
// }

// Logger.prototype.w = function (msg) {
// 	console.log ( colors.warn (this._tag_ + msg) );
// }

// Logger.prototype.e = function (msg) {
// 	console.log ( colors.error (this._tag_ + msg) );
// }

// Logger.prototype.b = function (msg) {
// 	console.log ( colors.bold (this._tag_ + msg) );	
// }

// module.exports = makeInstance;