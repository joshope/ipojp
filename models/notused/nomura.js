'use strict';

var Nomura = require('./include/nomura-core'),
	// mailer = require('./mailer'),
	logger = require('./logger')('nomura'),
	param  = require('../common/param'),
	util   = require('../common/util');

var nomura,
	userId;

var cb;

var SEC_NAME = 'nomura';

var isSuccess; // スクレイプ成功フラグ


exports.init = function (__userId, isDisp/*option:nm表示*/, isDebug/*option:nmデバッグモード*/, __cb/*option*/) {
	userId = __userId;

	// if ( !nomura ) { // singleton
	// 	nomura = new Nomura(userId);
	// }

	if ( nomura ) { // singleton (coreのnmを破棄して再生成)
		nomura
		.end()
		.then(() => {
			logger.w('---- close nm and recreate nomura obect');
			nomura = new Nomura(userId, isDisp, isDebug);
			if (__cb) __cb();
		});
	} else {
		nomura = new Nomura(userId, isDisp, isDebug);
		if (__cb) __cb();
	}

	// mailer.init();
}


exports.start = function (__cb) {

	cb = __cb;


	if ( nomura.isValid ) {
		nomura
		.open()
		.login(userId)
		.then(() => {
			logger.i('finished login to Nomura');
		})
		.gotoIpo()
		.searchIpo()
		.then(items => {
			logger.w(items);
			scrapeIpo();
		})
		.catch(err => {
			logger.e(err);
			scrapeIpo();
		});

	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		// if (cb) cb(true);
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}

}


function scrapeIpo() {
	// var items = nomura.getItems();
	var items = nomura.getItemsToBookBill();

	var ques = [];
	// var isSuccess = true;
	isSuccess = true; // 初期化

	if ( !util.isExist(items) && util.isExist(nomura.getErrors()) ) {
		logger.w('[野村證券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;
	}


	// var enable_num = 0;
	for (var i=0; i<items.length; i++) {
		var item = items[i];

		// if (item.enable) {
			// enable_num++;
			logger.d('[野村證券] ブックビルできます----> '+item.name);

			ques.push(
				nomura
				.gotoIpo()
				.clickBookBill()
				.then(code => {
					logger.w('[野村證券] ブックビル申込します ----> '+code);
					/* todo: clickConfirm後に移動 */
					/* sbi.js内での処理に変更 */
					// bookBilled.push( util.findItem(code, items) );
				})
				.checkTerms()
				.checkProspectus()
				.confirmApplication()
				.executeApplication()
				.then(status => {
					logger.w('[野村證券] bookBill status ----> '+status);
					isSuccess = isSuccess & status;
				})
				.catch(err => {
					logger.e(err);
				})
			);
		// }
	}

	// if ( !enable_num ) {
	// 	var msg = '[野村證券] ブックビルできるアイテムはありません'; 
	// 	logger.w(msg);
	// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'nomura') );
	// 	mailer.report(null/*bookBilled*/, '【野村證券】'/*subjectHeader*/, userId, 'nomura'/*証券会社名*/, getResults()/*抽選結果items*/);
	// 	return;
	// }

	Promise.all(ques)
	.then( results => {
		if ( !ques.length ) {
			logger.i('[野村證券] ブックビルできるアイテムはありません');
		} else {
			logger.i('[野村證券] scrapingが完了しました');
		}	

		// if ( isSuccess ) {
		// 	var bookBilled = nomura.getBookBilled();
		// 	// mailer.report(bookBilled, '【Nomura】'/*subjectHeader*/, userId, 'nomura'/*証券会社名*/);
		// 	mailer.report(bookBilled, '【野村證券】'/*subjectHeader*/, userId, 'nomura'/*証券会社名*/, nomura.getResults()/*抽選結果items*/);
		// } else {
		// 	var msg = '[野村證券] ブックビルに失敗している可能性があります';
		// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'nomura') );
		// 	mailer.send( msg+'<br>'+util.getUserName(userId, true/*isAka*/) );
		// }

		// if (cb) cb( isSuccess );
		if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});


	})
	.catch(err => {
		logger.e(err);
		// if (cb) cb( false );
		if (cb) cb({secName:SEC_NAME, isSuccess:false});
	});
}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return nomura.getBookBilled();
}

exports.getResults = function () {
	return nomura.getResults();
}

exports.getErrors = function () {
	return nomura.getErrors();
}

exports.getNotes = function () {
	return nomura.getNotes();
}

exports.isValid = function () {
	return nomura.isValid;
}

exports.isDisp = function () {
	return nomura.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}
