'use strict'

/**
 nightmareのevaluateはブラウザスコープなのでscraping先のコンテンツにjqueryがない場合はローカルのjqueryを使用
 jqueryはトップ階層に置く必要がある
 ローカルのjqueryを使用する場合、evaluateの直前に.inject('js', jquery)を記述
 */
var jquery = 'jquery-3.2.0.slim.min.js';

var Nightmare = require('nightmare'),
	param     = require('../../common/param'),
	secInfo   = require('../../cred/user').sec['monex'],
	logger    = require('../logger')('monex-core'),
	// mongo     = require('./mongo')(),
	util      = require('../../common/util'),
	path      = require('path'),
	scheduler = require('../scheduler');


/**
 * nightmareでpopupを操作するための設定
 * https://github.com/rosshinkley/nightmare-window-manager
 */
require('nightmare-window-manager')(Nightmare);
 

var nightmare;
var userId;
var popupWindowId;

var domain = 'https://mxp2.monex.co.jp';

/*オブジェクトプロパティにしてもいいかもしれない(this)
  Sbiオブジェクトを複数生成した場合は一般的な変数定義では共用されてしまう?*/
var items;
var itemsToBookBill; // ブックビル候補（優先順位でソートした配列）
var bookBilled; 　　　// ブックビルした銘柄配列
var errs; // エラーメッセージ配列
var notes; // 備考配列

// var isDisp; // nm表示フラグ

var isDebug; // nmデバッグモードフラグ

var money; // 買付可能額
var alreadyBookBilled; // 申込済み銘柄倍列(SMBCは申込済みでも申込ボタンが表示されるためフィルタリングに使用)

function Monex ( __userId, __isDisp/*option:nm表示*/, __isDebug/*option:nmデバッグモード*/ ) {

	userId = __userId;
	isDebug = __isDebug;
	
	// this.isDisp = __isDisp || param.NM_DISPLAY;
	this.isDisp = __isDisp;

	// makeNightmareInstance(isDispNm);
	makeNightmareInstance(this.isDisp);
	this.promise = Promise.resolve();

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;	

}

function makeNightmareInstance (__isDisp) {
// function makeNightmareInstance () {

	if (nightmare) return; // singleton

	// var isDispNm = __isDispNm || param.NM_DISPLAY;
	// isDisp = isDisp || param.NM_DISPLAY;

	nightmare = Nightmare({
		// show   : param.NM_DISPLAY,
		// show   : isDispNm,
		show   : __isDisp,
		width  : param.NM_W,
		height : param.NM_H,

		/**
		 *  nightmare(v2.10.0)のデフォルトのelectron(v1.8.8)では野村證券にログインできない
		 *  これは、野村證券はchrome51~66は使用できないため
		 *  electron(v1.8.8)はchrome59 (対比表:https://github.com/electron/releases)
		 *  最新のnightmare(v3.0.2)でもelectronが古く野村證券に対応できない
		 *  ---->
		 * 　electron(v4.0.0)を個別にインストールしてnightmareでelectronを指定して使用（electron最新版はv15だがnightmareがうまく動作しない）
		 * 　(https://github.com/segmentio/nightmare)
		 * 
		 *   nightmareのバージョンはv2.10.0をそのまま使用
		 * 　nightmare-iframe-manager(sbi.jsで使用)がこのバージョンを必要としているため
		 * 
		 */
		// electronPath: require('electron'),

		/**
		 * 目論見書のリンクをクリックすると保存ダイアログが表示されて処理が止まるため
		 * 保存ダイアログを出さないように（実際にはwindow.openイベントで何もしないように）するため
		 * nm-preload.jsでイベント処理をカスタムしている
		 * 
		 * todo: 目論見書以外でwindow.openイベントを処理したい場合はどうするか
		 *       設定あり・なしのnightmareインスタンスを2つ生成して対応とか
		 * 
		 */
		// webPreferences: {
		// 	preload: path.resolve(__dirname, 'nm-preload.js'),
		// },

 	});
}

//// 変数初期化
function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
	alreadyBookBilled = [];
}

Monex.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);	
	return this;
}

Monex.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

Monex.prototype.open = function () {

	init();

	// errs = []; // 適当にここで初期化
	// notes = []; // 適当にここで初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.windowManager() // popup window操作に必要(nightmare-window-manager)
			/**
			 * Access Deniedが表示されるためuseragentを設定
			 */ 
			.useragent('Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36')
			.goto(secInfo.url)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[open]'+err);
				errs.push('[open]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Monex.prototype.login = function () {

	// bookBilled = []; // 適当にここで初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate( (secInfo, userId) => {
				$('input#loginid').val(secInfo.users[userId].username);
				$('input#passwd').val(secInfo.users[userId].password);
			}, secInfo, userId)
			.wait(param.NM_WAIT*2)
			.click('button.btn_login_top')
			.wait('span.user-info')
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[login]'+err);
				errs.push('[login]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Monex.prototype.fetchMoney = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				return $('#buyingpower').text();
			})
			.then(__money => {
				money = util.extractNumber(__money);
				resolve(money);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[fetchMoney]'+err);
				errs.push('[fetchMoney]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Monex.prototype.gotoIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("新規公開株")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("ブックビルディング参加")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[gotoIpo]'+err);
				errs.push('[gotoIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}


Monex.prototype.fetchAlreadyBookBilled = function () {

	// alreadyBookBilled = []; // 初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("需要申告・購入申込状況")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var __items = [];
				$('th:contains("受付番号")').closest('thead').next('tbody').find('th:contains("課税")').each(function () {
					var item = {};
					item.name = $(this).prev().text();
					item.result = $(this).next().next().next().next().next().text();
					__items.push(item);
				});
				return __items;
			})
			.then(__items => {
				//// コードの抽出
				for (var i=0; i<__items.length; i++) {
					var item = __items[i];
					item.code = getCode(item.name);
					// item.name = getName(item.name);
				}
				return __items;
			})			
			.then(__items => {
				for (var i=0; i<__items.length; i++) {
					var item = __items[i];
					alreadyBookBilled.push({
						code: item.code,
						name: scheduler.getName(item.code),
						result: item.result,
					});
				}
				resolve(alreadyBookBilled);

			})
			.catch(err => {
				logger.e(err);
				// errs.push('[fetchAlreadyBookBilled]'+err);
				errs.push('[fetchAlreadyBookBilled]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/**
 * DOMが検索しにくいので銘柄名のみ抽出
 * ブックビルできる銘柄のみ詳細取得(fetchDetail)
 */ 
Monex.prototype.searchIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var btn = $('h2:contains("新規公開株")').next().find('a:contains("取扱銘柄一覧")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(() => {
				var __items = [];
				// var infoLinks = [];
				/** ブロック抽出 **/
				$('h2:contains("新規公開株式")').next().find('a:contains("詳細情報")').each(function () {
					var item = {};
					item.infoLink = $(this).attr('href');
					item.name = $(this).parent().prev().text().trim();
					//// 申込ボタン
					var btn = $(this).closest('tr').next().find('a:contains("需要申告")')[0];
					if ( btn ) {
						item.enable = true;
					}
					__items.push(item);
				});
				return __items;
			})
			.then(__items => {
				items = __items;
				extractInfo();
				resolve(items);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[searchIpo]'+err);
				errs.push('[searchIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Monex.prototype.fetchDetail = function (code) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			var item = util.findItem(code, items);
			if ( !item ) {
				reject('itemが存在しません'); 
				return;
			}
			nightmare
			.wait(param.NM_WAIT*2)
			.goto(domain+item.infoLink)
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(item => {
				// var info = {};
				// item.name = $('th:contains("銘柄名")').next().find('p').first().text();
				item.period = $('th:contains("ブックビルディング（需要申告）期間")').next().text();
				item.price  = $('th:contains("仮条件")').not(':contains("提示")').next().text();
				item.resultDate = {};
				item.resultDate.text = $('th:contains("抽選日")').next().text();

				/**
				 * 目論見書のURLを取得
				 * 要素を文字列に変換して取得
				 */
				var prospectus = $('a:contains("目論見書")').get(0);
				var dummy = $('<div>');
				item.prospectusUrl = dummy.append(prospectus.cloneNode(true)).html();

				return item;

			}, item)
			.then(__item => {
				//// items配列の更新
				updateItem(__item);
				// extractInfo(true/*isExtractBookBillTarget*/);
				extractInfo();				
				resolve(__item);
				logger.w(items);
				// resolve(items);
			})			
			.catch(err => {
				logger.e(err);
				// errs.push('[fetchDetail]'+err);
				errs.push('[fetchDetail]'+JSON.stringify(err));
				resolve(null); // Promise.allで途切れないようにrejectしていない（確か）
				// reject(err);
			});
		});
	});
}

Monex.prototype.clickBookBill = function (__code) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate(__code => {
				var code;
				/** ブロック抽出 **/
				$('h2:contains("新規公開株式")').next().find('a:contains("詳細情報")').each(function () {
					/* 申込ボタン */
					var btn = $(this).closest('tr').next().find('a:contains("需要申告")')[0];
					/* 銘柄コード*/
					code = $(this).parent().prev().text().trim();

					if ( btn && code.indexOf(__code) !== -1 ) {
						// name = $code.prev().text().trim();
						btn.click();
						return false; // break
					} else {
						code = null;
					}
				});
				return code; // 整形必要
			}, __code)
			.then(code__ => {
				if ( code__ ) {
					var code = getCode(code__);
					/* todo: clickConfirm後に移動 */
					bookBilled.push( util.findItem(code, items) );
					resolve(code);
				} else {
					errs.push('[clickBookBill] 銘柄コード抽出に失敗しています');
					resolve(null);
				}
				// var code = getCode(code__);
				// /* todo: clickConfirm後に移動 */
				// bookBilled.push( util.findItem(code, items) );
				// resolve(code);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickBookBill]'+err);
				errs.push('[clickBookBill]'+JSON.stringify(err));
				resolve(null); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

/**
 * TODO: 実装
 */
Monex.prototype.checkTerms = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate( (secInfo, userId) => {
				/* 数量 */
				// $('input[name="suryo"]').val('10000');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/*規約チェックボックス*/
				$('input[name="confirmCheck"]').prop("checked",true);

				/* 確認ボタン */
				var btn = $('input[alt="次へ"]')[0];
				if ( btn ) btn.click();
			}, secInfo, userId)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkTerms]'+err);
				errs.push('[checkTerms]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Monex.prototype.inputBookBill = function (item) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			// .inject('js', jquery)
			.evaluate( item => {
				/* 数量 */
				$('input#orderNominal').val('100');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/* 申告価格 */
				// $('select[name="snkokKakaku"]').prop("selectedIndex", 1);
				$('input[name="orderNari"]').prop('checked', true);

				/* 確認ボタン */
				var btn = $('input[value="次へ（申告内容確認）"]')[0];
				if ( btn ) btn.click();
			}, item)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[inputBookBill]'+err);
				errs.push('[inputBookBill]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

/**
 * 目論見書確認画面
 * 実装ペンディング(popupでAccess Denied表示される問題解決必要)
 * Monexの場合、目論見書を事前に確認すればサーバー側で閲覧履歴が保存される仕組み
 * 一度確認しておけば目論見書確認ページは表示されない
 * 目論見書を通常ブラウザで確認することでこの処理をスキップできる
 */ 
Monex.prototype.checkProspectus = function (item) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {

			if ( !item.prospectusUrl ) {
				var msg = '目論見書URLが取得できていません ----> '+item.name;
				logger.e(msg);
				errs.push('[checkProspectus]'+msg);
				resolve(false);
				return;
			}

			nightmare
			.wait(param.NM_WAIT)
			.goto(domain+item.prospectusUrl) //// popupせずに同一画面で表示 -> PDF保存ダイアログが表示されて固まる
			.wait(param.NM_WAIT)



			//// ここから実装


			.evaluate(() => {
				/**
				 * 目論見書を強制的に閲覧状態化
				 * ----> 初回は閲覧状態として判定されない(cookieとかで判定しているのかも)
				 */
				// $('a.apl-js-cmspsp').addClass('apl-js-read');

				/**
				 * 目論見書のリンクをクリックするとnightmare(electron)では
				 * 保存ダイアログが表示されてしまう
				 * ----> 保存ダイアログを表示させないようにnm-preload.jsでカスタムしている
				 * 
				 */
				$('a.apl-js-cmspsp').click();

				/* 同意ボタン */
				var btn = $('.btn-proceed');
				if ( btn ) {
					/**
					 * 目論見書を見ずに強制的にenable化した場合の処理
					 * ----> 目論見書リンクをクリックする処理に変更したのでこの処理は不要かもしれないが入れていても動作する
					 * 
					 */
					btn.prop('disabled', false);
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkProspectus]'+err);
				errs.push('[checkProspectus]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

//// 申込内容確認
Monex.prototype.confirmApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('button[name="buttonConfirm"]')[0];
				if ( btn ) {
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[confirmApplication]'+err);
				errs.push('[confirmApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Monex.prototype.executeApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate( (secInfo, userId) => {
				/* 取引パスワード */
				$('input#passwd').val(secInfo.users[userId].trade_password);

				/* 確認ボタン */
				var btn = $('button[name="buttonAccept"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			}, secInfo, userId)
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[executeApplication]'+err);
				errs.push('[executeApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


/* 目論見書の確認 */
/**
 * 
 * 目論見書の確認
 * PDFリンクを開くとnightmareが「保存」ダイアログを開き進まなくなるので
 * スキップ（PDFを開かず同意ボタンを強制的にenable化して対応）
 */
Monex.prototype.openProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var pdf = $('th:contains("閲覧書類")').next().find('a:contains("目論見書")')[0];
				if ( pdf ) 	pdf.click();
			})
			/**
			 popup window
			 **/
			.waitWindowLoad() // plugin 'nightmare-window-manager'
		    .currentWindow()  // plugin 'nightmare-window-manager'
			.then(window => {
				popupWindowId = window.id;
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[openProspectus]'+err);
				errs.push('[openProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Monex.prototype.closeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			/**
			 popup window close
			 **/
			.closeWindow(popupWindowId) // plugin 'nightmare-window-manager'
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[closeProspectus]'+err);
				errs.push('[closeProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Monex.prototype.agreeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var btn = $('.btn-proceed')[0];
				if ( btn ) btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[agressProspectus]'+err);
				errs.push('[agressProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}


Monex.prototype.clickConfirm = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			// .inject('js', jquery)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('input[value="実行する"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			})
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickConfirm]'+err);
				errs.push('[clickConfirm]'+JSON.stringify(err));
				resolve(false);
			});
		});
	});
}

/**
 * nightmare終了
 */
Monex.prototype.end = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.end()
			.then(() => {
				resolve();
				nightmare = null;
			})
			.catch(err => {
				logger.e(err);
				errs.push('[end]'+JSON.stringify(err));
				resolve(err); // resolveとして処理
				nightmare = null;
			});
		});
	});
}

Monex.prototype.getItems = function () {
	// items = items || [];
	return items || [];
}

/* BookBillできるItems(優先順位でソートしたもの) */
Monex.prototype.getItemsToBookBill = function () {
	// itemsToBookBill = itemsToBookBill || [];
	return itemsToBookBill || [];
}

/* BookBillしたItems */
Monex.prototype.getBookBilled = function () {
	// bookBilled = bookBilled || [];
	return bookBilled || [];
}

/* 既にBookBillしているItems */
Monex.prototype.getAlreadyBookBilled = function () {
	// alreadyBookBilled = alreadyBookBilled || [];
	return alreadyBookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Monex.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Monex.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Monex.prototype.getNotes = function () {
	return notes || [];
}

/* nm表示フラグ */
// Monex.prototype.isDisp = function () {
// 	return isDisp;
// }


/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
// function extractInfo (items) {
// function extractInfo (isExtractBookBillTarget) {
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードと銘柄名の抽出
		if ( !item.code ) {
			item.code = getCode(item.name);
			item.name = getName(item.name);
		}

		//// 抽選日の抽出
		//// todo: 年をまたぐ場合に対応
		//// monexの場合は複数回extractInfoを実行するためextract済みのitemはスキップ
		// if ( item.resultDate ) {
		if ( item.resultDate && !item.resultDate.unixTime ) {			
			var date = /([0-9]+月[0-9]+日)/gim
			// var date = /([0-9]+[月/][0-9]+日?)/gim
			.check( item.resultDate.text );

			if ( date ) {
				var dateText = date[0].captures[0].replace(/月/, '/').replace('日', '').trim();
				item.resultDate.text = dateText;
				var unixTime = util.getUnixTimeFromText(dateText);
				if ( unixTime ) item.resultDate.unixTime = unixTime;
			} else {
				logger.w('抽選日が抽出できていません ----> '+item.name+'('+item.code+') : 抽選日='+item.resultDate.text);
			}
			// try {
			// 	var dateText = date[0].captures[0].replace(/月/, '/').replace('日', '').trim();
			// 	item.resultDate.text = dateText;
			// 	var unixTime = util.getUnixTimeFromText(dateText);
			// 	if ( unixTime ) item.resultDate.unixTime = unixTime;
			// } catch (err) {
			// 	logger.e(err);
			// 	// errs.push('[extractInfo]'+err);
			// 	errs.push('[extractInfo]'+JSON.stringify(err));
			// }
		}

		//// 価格の抽出（仮条件の上限価格）
		// logger.w(item.price);
		if ( item.price ) {
			var price = /([0-9,]+)\s*円?/gim
			.check( item.price );
			if ( price ) {
				if (price.length == 2 ) {
					item.price = util.extractNumber(price[1].captures[0].trim());
				} else {
					item.price = util.extractNumber(price[0].captures[0].trim());
				}
			} else {
				delete item.price;
			}
		}

		//// 目論見書URLの抽出
		//// 目論見書はpopupで開くため事前に閲覧しておくしかなく自動化困難のためitem.prospectusUrlは実質未使用
		//// monexの場合は複数回extractInfoを実行するためextract済みのitemはスキップ
		// if ( item.prospectusUrl ) {
		if ( item.prospectusUrl && /openWin/gim.test(item.prospectusUrl) ) {
			var url = /(\/pc\/.+?)\'/gim
			.check ( item.prospectusUrl );
			if ( url ) {
				url = url[0].captures[0].replace(/\&amp\;/gim, '&');
				item.prospectusUrl = url;
			} else {
				errs.push('[extractInfo] 目論見書URLが取得できていません');
			}
			// try {
			// 	url = url[0].captures[0].replace(/\&amp\;/gim, '&');
			// 	item.prospectusUrl = url;
			// } catch (err) {
			// 	logger.e(err);
			// 	// errs.push('[extractInfo]'+err);
			// 	errs.push('[extractInfo]'+JSON.stringify(err));
			// }
		}
	}

	// if ( isExtractBookBillTarget ) {
	// 	extractBookBillTarget();
	// }

}

/**
 * ブックビル対象の抽出・ソート
 * 主幹事銘柄が最優先、次にランクで優先順位を設定（主幹事銘柄もランク順）
 * 
 * monexの場合、agentから呼び出す仕様に変更(21.09.08)(他の証券coreの場合は内部処理で実行)
 * 理由)
 * 銘柄情報取得のためにfetchDetailをバッチ処理(agentでPromise.all)しており、
 * fetchDetail毎にextractBookBillTargetを行っていたためにitemsToBookBillに重複が発生していたため
 * バッチ処理終了後にextractBookBillTargetを1回だけ呼び出す仕様に変更
 */
Monex.prototype.extractBookBillTarget = extractBookBillTarget;
function extractBookBillTarget () {	
	var items_mainSec = []; // SMBCが主幹事の銘柄
	var items_not_mainSec = []; // 主幹事ではない銘柄

	// itemsToBookBill = [];

	for ( var i=0; i<items.length; i++) {
		var item = items[i];

		//// 申込可能判定 
		if ( !item.enable ) continue;

		item.rank = scheduler.getRank(item.code);
		item.mainSec = scheduler.getMainSec(item.code);

		//// 申込済み判定
		// if ( alreadyBookBilledCodes.includes(item.code) ) {
		// 	logger.w('既にブックビル済みの銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
		// 	continue;
		// }

		//// RANK判定
		if ( item.rank && util.isTargetIpoRank(item.rank, true/*isForBookBill*/) ) {
			//// 主幹事判定
			if ( /マネックス/.test(item.mainSec) ) {
				items_mainSec.push(item);
			} else {
				items_not_mainSec.push(item);
			}
		} else {
			logger.w('ターゲットRankではない銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
			continue;
		}

	}

	//// ランク順にソート
	sortItems(items_mainSec);
	sortItems(items_not_mainSec);

	var targets = items_mainSec.concat(items_not_mainSec);

	//// 予算判定
	if ( !money ) {
		var msg = '口座残高が取得できていないので予算判定をスキップ';
		logger.e(msg);
		errs.push('[extractBookBillTarget]'+msg);
		itemsToBookBill = targets;
	} else {
		for (var i=0; i<targets.length; i++) {
			var item = targets[i];
			if ( !item.price ) {
				var msg = '仮条件価格が取得できていないのでブックビル対象から除外 ----> '+item.name+' ('+item.code+')';
				logger.e(msg);
				errs.push('[extractBookBillTarget]'+msg)
				continue;
			}
			if ( money >= (item.price*100) ) {
				itemsToBookBill.push(item);
				money = money - item.price*100;
				logger.d('予算残高: '+money);
			} else {
				var msg = '予算オーバーのためブックビル対象から除外 ----> 予算残高: '+money+'円 '+item.name+'('+item.code+') Rank【'+item.rank+'】'+item.price+'円';
				logger.w(msg);
				notes.push(msg);
			}
		}
	}

	logger.w('BookBill対象銘柄: '+itemsToBookBill.length+'銘柄');
	logger.i(itemsToBookBill);

}

/**
 * Rankでソート
 */
function sortItems (__items) {
	for (var i=0; i<__items.length; i++) {
		var item = __items[i];
		//// 評価用に一時的なプロパティ追加		
		switch(item.rank) {
			case 'S':
			item.score = 100;
			break;
			case 'A':
			item.score = 80;
			break;
			case 'B':
			item.score = 60;
			break;
			case 'C':
			item.score = 40;
			break;
			case 'D':
			item.score = 20;
			break;
			default:
			item.score = 0;
		}
	}
	__items.sort( function (a, b) {
	    if ( a.score > b.score ) return -1;
	    if ( a.score < b.score ) return 1;
	    return 0;
	});	
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];
	var target = alreadyBookBilled || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄コードを含むテキストからコードを抽出
*/
function getCode (text) {
	if ( !text ) return null;
		//// 銘柄コードの抽出
		var code =
		// /^\s*([0-9][0-9][0-9][0-9])/gim
		/([0-9][0-9][0-9][0-9])/gim
		.check( text );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}

function getName (text) {
	if ( !text ) return null;
	text = text.replace(/\n/gim, '').replace(/\t/gim, '');
	var name =
	/^(.+?)\s*[0-9]+/gim
	.check( text );
	if ( name ) name = name[0].captures[0].trim();
	return name;
}

/**
 * items配列の中のitemを引数のものに差し替え
 */
function updateItem (item) {
	var index = getItemIndex(item);
	if ( index != -1 ) {
		items.splice(index, 1, item);
	} else {
		logger.w('not found item to update ----> '+item.name+' ('+item.code+')');
		items.push(item);
	}
}

function getItemIndex (item) {
	var index = -1;
	for (var i=0; i<items.length; i++) {
		if (item.code === items[i].code) {
			index = i;
			break;
		}
	}
	return index;
}


module.exports = Monex;
