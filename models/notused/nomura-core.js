'use strict'

/**
 nightmareのevaluateはブラウザスコープなのでscraping先のコンテンツにjqueryがない場合はローカルのjqueryを使用
 jqueryはトップ階層に置く必要がある
 ローカルのjqueryを使用する場合、evaluateの直前に.inject('js', jquery)を記述
 */
var jquery = 'jquery-3.2.0.slim.min.js';

var Nightmare = require('nightmare'),
	param     = require('../../common/param'),
	secInfo   = require('../../cred/user').sec['nomura'],
	logger    = require('../logger')('nomura-core'),
	// mongo     = require('./mongo')(),
	util      = require('../../common/util'),
	path      = require('path'),
	scheduler = require('../scheduler');


/**
 * nightmareでpopupを操作するための設定
 * https://github.com/rosshinkley/nightmare-window-manager
 */
require('nightmare-window-manager')(Nightmare);
 

var nightmare;
var userId;
var popupWindowId;

/*オブジェクトプロパティにしてもいいかもしれない(this)
  Sbiオブジェクトを複数生成した場合は一般的な変数定義では共用されてしまう?*/
var items;
var itemsToBookBill;
var bookBilled;
var errs; // エラーメッセージ配列
var notes; // 備考配列

// var isDisp; // nm表示フラグ

var isDebug; // nmデバッグモードフラグ

function Nomura ( __userId, __isDisp/*option:nm表示*/, __isDebug/*option:nmデバッグモード*/ ) {

	userId  = __userId;
	isDebug = __isDebug;
	
	// this.isDisp = __isDisp || param.NM_DISPLAY;
	this.isDisp = __isDisp;

	makeNightmareInstance(this.isDisp);
	// makeNightmareInstance(isDispNm);
	this.promise = Promise.resolve();

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;

}

// function makeNightmareInstance () {
function makeNightmareInstance (__isDisp) {	

	if (nightmare) return; // singleton

	// var isDispNm = __isDispNm || param.NM_DISPLAY;
	// isDisp = isDisp || param.NM_DISPLAY;


	nightmare = Nightmare({
		// show   : param.NM_DISPLAY,
		// show   : isDispNm,
		show   : __isDisp,
		width  : param.NM_W,
		height : param.NM_H,

		/**
		 *  nightmare(v2.10.0)のデフォルトのelectron(v1.8.8)では野村證券にログインできない
		 *  これは、野村證券はchrome51~66は使用できないため
		 *  electron(v1.8.8)はchrome59 (対比表:https://github.com/electron/releases)
		 *  最新のnightmare(v3.0.2)でもelectronが古く野村證券に対応できない
		 *  ---->
		 * 　electron(v4.0.0)を個別にインストールしてnightmareでelectronを指定して使用（electron最新版はv15だがnightmareがうまく動作しない）
		 * 　(https://github.com/segmentio/nightmare)
		 * 
		 *   nightmareのバージョンはv2.10.0をそのまま使用
		 * 　nightmare-iframe-manager(sbi.jsで使用)がこのバージョンを必要としているため
		 * 
		 */
		electronPath: require('electron'),

		/**
		 * 目論見書のリンクをクリックすると保存ダイアログが表示されて処理が止まるため
		 * 保存ダイアログを出さないように（実際にはwindow.openイベントで何もしないように）するため
		 * nm-preload.jsでイベント処理をカスタムしている
		 * 
		 * https://stackoverflow.com/questions/33520361/nightmarejs-to-click-the-window-confirms-button
		 * 
		 * todo: 目論見書以外でwindow.openイベントを処理したい場合はどうするか
		 *       設定あり・なしのnightmareインスタンスを2つ生成して対応とか
		 * 
		 * np-preloadを通しても目論見書の閲覧状態として判定されないことが判明（PDFリンクをクリックしても閲覧状態として判定されない）
		 * 閲覧状態はサーバー側で保存されるので通常ブラウザで事前に閲覧しておけば対応可能
		 * ----> 目論見書PDFの擬似的なダウンロード処理を追加することで閲覧済み判定される模様のためpreloadもコメントアウト
		 */
		 // webPreferences: {
		 //		preload: path.resolve(__dirname, 'nm-preload.js'),
		 // },


 	});
}

function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
}

Nomura.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);	
	return this;
}

Nomura.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

Nomura.prototype.open = function () {

	init();

	// errs = []; // 適当にここで初期化
	// notes = []; // 適当にここで初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
		    .windowManager() // popup window操作に必要(nightmare-window-manager)
			.goto(secInfo.url)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[open]'+err);
				errs.push('[open]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Nomura.prototype.login = function () {
	// bookBilled = []; // 適当にここで初期化
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			// .inject('js', jquery)
			.evaluate( (secInfo, userId) => {
				$('input#branchNo').val(secInfo.users[userId].branch_code);
				$('input#accountNo').val(secInfo.users[userId].username);
				$('input#passwd1').val(secInfo.users[userId].password);
				// $('input[name="user_id"]').val(secInfo.users[userId].username);
				// $('input#passwd1').val(secInfo.users[userId].password);
			}, secInfo, userId)
			.wait(param.NM_WAIT)
			.click('button[name="buttonLogin"]')
			.wait('.customer')
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[login]'+err);
				errs.push('[login]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Nomura.prototype.gotoIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.evaluate(() => {
				var btn = $('.direct-link a:contains("IPO")')[0];
				if ( btn ) 	btn.click();
			})
			.wait(param.NM_WAIT*2)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[gotoIpo]'+err);
				errs.push('[gotoIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Nomura.prototype.searchIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var __items = [];
				/** ブロック抽出 **/
				$('.anchor-pt').each(function() {
					var item = {};
					item.name   = $(this).find('.ttl-stock').find('span').first().text().trim();
					//// 抽選日					
					item.resultDate = {};
					item.resultDate.text = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("売出価格決定日")').next().text().trim();
					//// 抽選結果
					// item.result = $(this).find('.ttl-lvl-3:contains("購入情報")').next().find('th:contains("当選数量")').next().text().trim();
					var $result = $(this).find('.result-label');
					if ( $result.length ) {
						item.result = $result.find('span').text().trim();
					}
					//// 申込期間
					item.period = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("抽選参加申込期間")').next().text().trim();
					/* 申込ボタン */
					//// 申し込み完了した後は"抽選申込取消へ"に変わる
					var btn = $(this).find('.action-btn a:contains("抽選申込へ")')[0];

					if ( btn ) {
						item.enable = true;
						/* 申込ボタン mongoには保存しない bookBill実行用 */
						// item.btn = btn;
					}
					__items.push(item);
				});
				return __items;
			})
			.then(__items => {
				items = __items;
				// extractInfo(items);
				extractInfo();
				//logger.w(util.getUnixTimeFromText(items[0].result.text));
				resolve(items);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[searchIpo]'+err);
				errs.push('[searchIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Nomura.prototype.clickBookBill = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var name;
				/** ブロック抽出 **/
				$('.anchor-pt').each(function() {
					/* 申込ボタン */
					//// 申し込み完了した後は"抽選申込取消へ"に変わる
					var btn = $(this).find('.action-btn a:contains("抽選申込へ")')[0];
					if ( btn ) {
						name = $(this).find('.ttl-stock').find('span').first().text().trim();
						btn.click();
						/* 最初のアイテムをクリックしたらループ終了しておく */
						return false; // break
					}
				});
				return name;
			})
			.then(name => {
				var code = getCode(name);
				/* todo: clickConfirm後に移動 */
				bookBilled.push( util.findItem(code, items) );
				resolve(code);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickBookBill]'+err);
				errs.push('[clickBookBill]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

// Nomura.prototype.inputBookBill = function () {
Nomura.prototype.checkTerms = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate( (secInfo, userId) => {
				/* 数量 */
				// $('input[name="suryo"]').val('10000');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/*規約チェックボックス*/
				$('input[name="diKn1"]').prop("checked",true);
				$('input[name="diKn2"]').prop("checked",true);
				$('input[name="diKn3"]').prop("checked",true);
				/* 確認ボタン */
				var btn = $('button[name="buttonAgree"]')[0];
				if ( btn ) btn.click();
			}, secInfo, userId)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkTerms]'+err);
				errs.push('[checkTerms]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

//// 目論見書確認画面
Nomura.prototype.checkProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			/**
			 * 目論見書PDFを擬似的にダウロード処理
			 * (閲覧済みとして判定させるため)
			 */
			.evaluate(function ev(){
			    var el = document.querySelector("a.apl-js-cmspsp");
			    var xhr = new XMLHttpRequest();
			    xhr.open("GET", el.getAttribute('data-filepath'), true);
			    xhr.overrideMimeType("text/plain; charset=x-user-defined");
			    xhr.send();
			    return xhr.responseText;
			}, function cb(data){
			    // var fs = require("fs");
			    // fs.writeFileSync("book.epub", data, "binary");
			})
			.wait(param.NM_WAIT)
			// .evaluate(() => {
			.evaluate(isDebug => {
				/**
				 * 目論見書を強制的に閲覧状態化
				 * ----> 初回は閲覧状態として判定されない(cookieとかで判定しているのかも)
				 */
				// $('a.apl-js-cmspsp').addClass('apl-js-read');

				/**
				 * 目論見書のリンクをクリックするとnightmare(electron)では
				 * 保存ダイアログが表示されてしまう
				 * ----> 保存ダイアログを表示させないようにnm-preload.jsでカスタムしている
				 * ----> nm-preload.jsを通しても目論見書は閲覧状態として判定されない
				 * 　　　 ただし、閲覧状態はサーバー側で保存されるので通常ブラウザで事前に閲覧しておけば対応可能
				 * ----> 前段のevaluateで擬似的なPDFダウンロード処理を追加することで閲覧状態になる模様
				 *       PDFリンクのクリック処理 $('a.apl-js-cmspsp').click()をコメントアウトしても動作している
 				 *       やはり閲覧状態にならない模様なのでデバッグモードで目論見書リンクをクリックして半手動で対応
				 */
				if ( isDebug ) {
					$('a.apl-js-cmspsp').click();
					//// MacOSのPDF保存ダイアログを処理するまで次に進まない
				}
			}, isDebug)
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 同意ボタン */
				var btn = $('.btn-proceed');
				if ( btn ) {
					/**
					 * 目論見書を見ずに強制的にenable化した場合の処理
					 * ----> 目論見書リンクをクリックする処理に変更したのでこの処理は不要かもしれないが入れていても動作する
					 * 
					 */
					btn.prop('disabled', false);
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkProspectus]'+err);
				errs.push('[checkProspectus]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

//// 申込内容確認
Nomura.prototype.confirmApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('button[name="buttonConfirm"]')[0];
				if ( btn ) {
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[confirmApplication]'+err);
				errs.push('[confirmApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Nomura.prototype.executeApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate( (secInfo, userId) => {
				/* 取引パスワード */
				$('input#passwd').val(secInfo.users[userId].trade_password);

				/* 確認ボタン */
				var btn = $('button[name="buttonAccept"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			}, secInfo, userId)
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[executeApplication]'+err);
				errs.push('[executeApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


/* 目論見書の確認 */
/**
 * 
 * 目論見書の確認
 * PDFリンクを開くとnightmareが「保存」ダイアログを開き進まなくなるので
 * スキップ（PDFを開かず同意ボタンを強制的にenable化して対応）
 */
Nomura.prototype.openProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var pdf = $('th:contains("閲覧書類")').next().find('a:contains("目論見書")')[0];
				if ( pdf ) 	pdf.click();
			})
			/**
			 popup window
			 **/
			.waitWindowLoad() // plugin 'nightmare-window-manager'
		    .currentWindow()  // plugin 'nightmare-window-manager'
			.then(window => {
				popupWindowId = window.id;
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[openProspectus]'+err);
				errs.push('[openProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Nomura.prototype.closeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			/**
			 popup window close
			 **/
			.closeWindow(popupWindowId) // plugin 'nightmare-window-manager'
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[closeProspectus]'+err);
				errs.push('[closeProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Nomura.prototype.agreeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var btn = $('.btn-proceed')[0];
				if ( btn ) btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[agreeProspectus]'+err);
				errs.push('[agreeProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}



Nomura.prototype.clickConfirm = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('input[name="order_btn"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			})
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickConfirm]'+err);
				errs.push('[clickConfirm]'+JSON.stringify(err));
				resolve(false);
			});
		});
	});
}

/**
 * nightmare終了
 */
Nomura.prototype.end = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.end()
			.then(() => {
				resolve();
				nightmare = null;
			})
			.catch(err => {
				logger.e(err);
				errs.push('[end]'+JSON.stringify(err));
				resolve(err); // resolveとして処理
				nightmare = null;
			});
		});
	});
}


Nomura.prototype.getItems = function () {
	return items || [];
}

/* BookBillできるItems */
Nomura.prototype.getItemsToBookBill = function () {
	return itemsToBookBill || [];
}

/* BookBilledしたItems */
Nomura.prototype.getBookBilled = function () {
	return bookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Nomura.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Nomura.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Nomura.prototype.getNotes = function () {
	return notes || [];
}

/* nm表示フラグ */
// Nomura.prototype.isDisp = function () {
// 	return isDisp;
// }


/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
// function extractInfo (items) {
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードの抽出
		item.code = getCode(item.name);

		//// 銘柄コードの抽出
		item.name = getName(item.name);

		//// 抽選日の抽出
		var date = /[0-9]+\/([0-9]+\/[0-9]+)/gim
		.check( item.resultDate.text );

		if ( date ) {
			var unixTime = util.getUnixTimeFromText(date[0].captures[0].trim());
			if ( unixTime ) item.resultDate.unixTime = unixTime;
		} else {
			logger.w('抽選日が抽出できていません ----> '+item.name+'('+item.code+') : 抽選日='+item.resultDate.text);
		}

		// try {
		// 	var unixTime = util.getUnixTimeFromText(date[0].captures[0].trim());
		// 	if ( unixTime ) item.resultDate.unixTime = unixTime;
		// } catch (err) {
		// 	logger.e(err);
		// 	// errs.push('[extractInfo]'+err);
		// 		errs.push('[extractInfo]'+JSON.stringify(err));
		// }

	}

	extractBookBillTarget();

}

/**
 * ブックビル対象の抽出・ソート
 * 
 */
function extractBookBillTarget () {

	// itemsToBookBill = [];

	if ( !items ) return;

	items.forEach(item => {
		if ( item.enable ) {
			itemsToBookBill.push(item);
		}
	});
	
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];	
	var target = items || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄名からcodeを抽出
*/
function getCode (name) {
	if ( !name ) return null;
		//// 銘柄コードの抽出
		var code =
		// /^\s*([0-9][0-9][0-9][0-9])/gim
		/^\s*([0-9]{4})/gim
		.check( name );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}

/**
 銘柄名のみ抽出（スクレイプ直後は銘柄名だけでなくコードが混ざっている）
*/
function getName (__name) {
	if ( !__name ) return null;
	//// 銘柄名のみ抽出
	var name =
	/[0-9]{4}(.+?)$/gim
	.check( __name );
	if ( name ) name = name[0].captures[0].trim();
	return name;
}


module.exports = Nomura;
