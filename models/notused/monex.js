'use strict';

var Monex = require('./include/monex-core'),
	// mailer = require('./mailer'),
	logger = require('./logger')('monex'),
	param  = require('../common/param'),
	util   = require('../common/util');

var monex,
	userId;

var cb;

var SEC_NAME = 'monex';

var isSuccess; // スクレイプ成功フラグ

exports.init = function (__userId, isDisp/*option:nm表示*/, isDebug/*option:nmデバッグモード*/, __cb/*option*/) {
	userId = __userId;

	// if ( !monex ) { // singleton
	// 	monex = new Monex(userId);
	// }

	if ( monex ) { // singleton (coreのnmを破棄して再生成)
		monex
		.end()
		.then(() => {
			logger.w('---- close nm and recreate monex obect');
			monex = new Monex(userId, isDisp, isDebug);
			if (__cb) __cb();
		});
	} else {
		monex = new Monex(userId, isDisp, isDebug);
		if (__cb) __cb();
	}	

	// mailer.init();
}


exports.start = function (__cb) {

	cb = __cb;

	if ( monex.isValid ) {
		monex
		.open()
		.login(userId)
		.then(() => {
			logger.i('finished login to monex');
		})
		.fetchMoney()
		.then(money => {
			logger.w('------money----- '+money);
		})
		.gotoIpo()
		.fetchAlreadyBookBilled()
		.then(items => {
			logger.w(items);
		})
		.searchIpo()
		.then(items => {
			logger.w(items);
			fetchDetail(items);
		})


		/* ここから実装 */

		// .fetchMoney()
		// .then(money => {
		// 	logger.w('------money----- '+money);
		// })
		// .gotoIpo()
		// .fetchAlreadyBookBilledCodes()
		// .then(codes => {
		// 	logger.w(codes);
		// })
		// .searchIpo()
		// .then(items => {
		// 	logger.w(items);

		// 	scrapeIpo();

		// })
		.catch(err => {
			logger.e(err);
			scrapeIpo();
		});
	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		// if (cb) cb(true);
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}
}

function fetchDetail(items) {
	var ques = [];
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.enable ) continue;
		ques.push(
			monex
			.fetchDetail(item.code)
			.catch(err => {
				logger.e(err);　//// Promise.allを中断させないために実際にはすべてresolve処理している
			})
		);
	}

	Promise.all(ques)
	.then( results => {
		logger.i('[monex証券] ブックビル可能な銘柄詳細データ取得が完了しました');
		logger.w(monex.getItems());

		//// ブックビルターゲットの抽出
		monex.extractBookBillTarget();


		//// ここから実装
		scrapeIpo();


	})
	.catch(err => {
		logger.e(err);
		scrapeIpo();
	});
}

function scrapeIpo() {
	var items = monex.getItemsToBookBill();

	var ques = [];
	// var isSuccess = true;
	isSuccess = true; // 初期化

	if ( !util.isExist(items) && util.isExist(monex.getErrors()) ) {
		logger.w('[monex証券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;
	}

	// var enable_num = 0;
	for (var i=0; i<items.length; i++) {
		var item = items[i];

		// if (item.enable) { // monex-coreでチェック済みのため不要(itemsToBookBillはenableのみ)
			// enable_num++;
			logger.d('[monex証券] ブックビルできます----> '+item.name+' ('+item.code+')');

			ques.push(
				monex
				.gotoIpo()
				.clickBookBill(item.code)
				.then(code => {
					logger.w('[monex証券] ブックビル申込します ----> '+code);
				})


				/**
				 * Monexの場合、目論見書を事前に確認すればサーバー側で閲覧履歴が保存される仕組み
				 * 一度確認しておけば目論見書確認ページは表示されない
				 * 目論見書を通常ブラウザで確認することでcheckProspectus処理をスキップできる
				 */

				// .checkProspectus(item)　// <----　Access Denied問題解決必要(popup window) 
				// .checkTerms()


				.inputBookBill(item)
				.then(status => {
					if ( !status ) logger.w('ブックビル入力に失敗しています（目論見書確認ができていない可能性があります）');
					isSuccess = isSuccess & status;
				})
				.clickConfirm()
				.then(status => {
					logger.w('[monex証券] bookBill status ----> '+status);
					isSuccess = isSuccess & status;
				})

				.catch(err => {
					logger.e(err); //// Promise.allを中断させないために実際にはすべてresolve処理している
				})
			);
		// }
	}

	// if ( !enable_num ) {
	// 	var msg = '[monex証券] ブックビルできるアイテムはありません'; 
	// 	logger.w(msg);
	// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'monex') );
	// 	mailer.report(null/*bookBilled*/, '【monex証券】'/*subjectHeader*/, userId, 'monex'/*証券会社名*/, getResults()/*抽選結果items*/);
	// 	return;
	// }

	Promise.all(ques)
	.then( results => {
		if ( !ques.length ) {
			logger.i('[monex証券] ブックビルできるアイテムはありません');
		} else {
			logger.i('[monex証券] scrapingが完了しました');
		}

		// if ( isSuccess ) {
		// 	var bookBilled = monex.getBookBilled();
		// 	mailer.report(bookBilled, '【monex証券】'/*subjectHeader*/, userId, 'monex'/*証券会社名*/, monex.getResults()/*抽選結果items*/);
		// } else {
		// 	var msg = '[monex証券] ブックビルに失敗している可能性があります';
		// 	// mailer.send( msg+'<br>'+util.getUserName(userId, 'monex') );
		// 	mailer.send( msg+'<br>'+util.getUserName(userId, true) );
		// }

		// if (cb) cb( isSuccess );
		if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});

	})
	.catch(err => {
		logger.e(err);
		// if (cb) cb( false );
		if (cb) cb({secName:SEC_NAME, isSuccess:false});
	});
}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return monex.getBookBilled();
}

exports.getResults = function () {
	return monex.getResults();
}

exports.getErrors = function () {
	return monex.getErrors();
}

exports.getNotes = function () {
	return monex.getNotes();
}

exports.isValid = function () {
	return monex.isValid;
}

exports.isDisp = function () {
	return monex.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}


