'use strict'

const puppeteer = require('puppeteer'),
	  param     = require('../../common/param'),
	  secInfo   = require('../../cred/user').sec['nomura'],
	  logger    = require('../logger')('nomura-core'),
	  util      = require('../../common/util'),
	  path      = require('path'),
	  scheduler = require('../scheduler');

async function injectJQuery (page) {
	await page.addScriptTag({ path: 'jquery-3.2.0.slim.min.js' }); // ルートDIRのファイルが読み込まれる
}

let browser;
let page;
let userId;

let items;
let itemsToBookBill;
let bookBilled;
let errs;    // エラーメッセージ配列
let notes;   // 備考配列
let isDebug; // nmデバッグモードフラグ

function Nomura ( __userId, __isDisp/*option:nm表示*/, __isDebug/*option:nmデバッグモード*/ ) {
	userId  = __userId;
	isDebug = __isDebug;
	this.isDisp = __isDisp;

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;
}

////---- thisを使う場合は　"=>"の記述は使用不可
Nomura.prototype.init = async function () {	
	if ( browser ) { // singleton
	    await browser.close();
	}
	if ( this.isValid ) {
		await makeBrowser(this.isDisp);
	}
}

async function makeBrowser (isDisp) {
	browser = await puppeteer.launch({
		/***********************************************
		 * NomuraはHeadlessモード（ブラウザ非表示）の場合、
		 * フリーズするため常にブラウザ表示設定に固定
		 ***********************************************/
		// headless: !isDisp, // 反転設定
		headless: false,
		slowMo:   param.BROWSER.DELAY,
	});
	page = await browser.newPage();

	await page.setViewport({
		width: param.BROWSER.W,
		height:param.BROWSER.H
	});

	//---- evaluate内でconsole.logを使用するための設定
	page.on('console', msg => {
		for (let i = 0; i < msg._args.length; ++i)
		console.log(`${i}: ${msg._args[i]}`);
	});

	//---- puppeteerエラーの検知
	page.on('pageerror', error => {
		logger.e('**Puppeteerエラー**', error.message)
	});

  //---- サイトがtimeout多発の場合に設定
	await page.setDefaultNavigationTimeout(60000);
}

function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
}

////---- thisを使う場合は　"=>"の記述は使用不可
// Nomura.prototype.open = async () => {
Nomura.prototype.open = async function () {
	
	//---- 野村證券はブラウザを閉じない場合、
	//---- 2回目以降のログインではlogginAdditionalの画面が表示されないので毎回ブラウザを閉じる
	await this.init();

	init();

	try {
		logger.d(secInfo.url);
		await page.goto(secInfo.url);
	} catch (err) {
		logger.e(err);
		errs.push('[open]'+JSON.stringify(err));
	}
}

Nomura.prototype.login = async () => {
	try {
		// await page.screenshot({path: 'example.png'});
		await page.waitForSelector('input#branchNo');
		await page.type('input#branchNo', secInfo.users[userId].branch_code);
		await page.type('input#accountNo', secInfo.users[userId].username);
		await page.type('input#passwd1', secInfo.users[userId].password);
		await page.click('button[value="buttonLogin"]');
	} catch (err) {
		logger.e(err);
		errs.push('[login]'+JSON.stringify(err));
	}
}

/*********************
 * ログイン追加認証
 * cookieを保存していないため郵便番号、生年月日の入力が必要
 *********************/
Nomura.prototype.loginAdditional = async () => {
	try {
		// await page.screenshot({path: 'example.png'});
		await page.waitForSelector('input[name="postCodeZhn"]');
		await page.type('input[name="postCodeZhn"]', secInfo.users[userId].post_code_1);
		await page.type('input[name="postCodeKhn"]', secInfo.users[userId].post_code_2);
		await page.type('input[name="birthDayNen"]', secInfo.users[userId].birthday_year);
		await page.type('input[name="birthDayGe"]', secInfo.users[userId].birthday_month);
		await page.type('input[name="birthDayHi"]', secInfo.users[userId].birthday_day);
		await page.click('button[value="buttonAccept"]');
	} catch (err) {
		logger.e(err);
		errs.push('[loginAdditional]'+JSON.stringify(err));
	}
}

Nomura.prototype.gotoIpo = async () => {
	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.evaluate(() => {
			var btn = $('.direct-link a:contains("IPO")')[0];
			if ( btn ) 	btn.click();
		});
		// await page.waitForTimeout(param.NM_WAIT*2);
		// await page.screenshot({path: 'example.png'});
	} catch (err) {
		logger.e(err);
		errs.push('[gotoIpo]'+JSON.stringify(err));
	}
}

Nomura.prototype.searchIpo = async () => {
	try {
		await page.waitForSelector('#main');
		await injectJQuery(page);
		items = await page.evaluate(() => {
			let __items = [];

			const $tables = [];
			//---- IPOテーブル
			$tables.push($('h3:contains("新規公開株式")').next().find('table'));
			//---- 結果テーブル
			$tables.push($('h2:contains("抽選申込済銘柄")').next().find('table'));

			for ( $table of $tables ) {
				if ( !$table.length ) continue;

				let __itemsBuffer = [];

				//---- 抽選日
				let indexResultDate = $table.find('th:contains("抽選日")').index(); // index()は0からカウント(該当なしの場合は-1)
				let indexResult = $table.find('th:contains("抽選結果")').index();

				//---- 最初の銘柄はstripe-odd、次がstripe-evenとクラスが交互に付与されている(さらに銘柄毎に同じクラスが2つ並んでいる)
				//---- 例) 最初の銘柄はtr.stripe-oddが2つ存在(最初のtr.stripe-oddに銘柄名、2つ目のtr.stripe-oddに日程関係が格納されている)

				//---- 銘柄名
				$table.find('tbody.list tr.stripe-odd:nth-child(odd), tr.stripe-even:nth-child(odd)').each(function() {
					let item = {};
					item.name = $(this).find('.txt-code').parent().text().trim();

					//----申込ボタン
					let $btn = $(this).find('a:contains("抽選申込"):not(:contains("取消"))');
					if ( $btn.length ) {
						item.enable = true;
					}
					__itemsBuffer.push(item);
				});

				//---- 申込期間、抽選日、抽選結果
				$table.find('tbody.list tr.stripe-odd:nth-child(even), tr.stripe-even:nth-child(even)').each(function(idx) {
					let item = __itemsBuffer[idx];
					let $target = $(this).find('td:nth-child('+(indexResultDate+1)+')'); // nth-child()は1からカウント

					//---- 申込期間
					item.period = $target.find('p:nth-child(1)').text().trim();

					//---- 抽選日
					item.resultDate = {};
					item.resultDate.text = $target.find('p:nth-child(2)').text().trim();

					//---- 抽選結果
					if ( indexResult !== -1 ) {
						$target = $(this).find('td:nth-child('+(indexResult+1)+')'); // nth-child()は1からカウント
						let $result = $target.find('span');
						if ( $result.length ) item.result = $result.text().trim();
					}
				});
				__items = __items.concat(__itemsBuffer);
			}
			return __items;
		});
		extractInfo();
		return items;
	} catch (err) {
		logger.e(err);
		errs.push('[searchIpo]'+JSON.stringify(err));
	}
}


//---- Legacy(詳細画面で流用可能)
Nomura.prototype.__searchIpo__ = async () => {
	try {
		await page.waitForSelector('.anchor-pt');
		await injectJQuery(page);
		items = await page.evaluate(() => {
			const __items = [];
			/** ブロック抽出 **/
			$('.anchor-pt').each(function() {
				var item = {};
				item.name   = $(this).find('.ttl-stock').find('span').first().text().trim();
				//---- 抽選日
				item.resultDate = {};
				item.resultDate.text = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("売出価格決定日")').next().text().trim();
				//---- 抽選結果
				var $result = $(this).find('.result-label');
				if ( $result.length ) {
					item.result = $result.find('span').text().trim();
				}
				//---- 申込期間
				item.period = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("抽選参加申込期間")').next().text().trim();
				/* 申込ボタン */
				//---- 申し込み完了した後は"抽選申込取消へ"に変わる
				var btn = $(this).find('.action-btn a:contains("抽選申込へ")')[0];
				if ( btn ) {
					item.enable = true;
				}
				__items.push(item);
			});
			return __items;
		});
		extractInfo();
		return items;
	} catch (err) {
		logger.e(err);
		errs.push('[searchIpo]'+JSON.stringify(err));
	}
}

Nomura.prototype.clickBookBill = async () => {
	try {
		await page.waitForSelector('#main');
		const name = await page.evaluate(() => {
			let name;
			//---- IPOテーブル
			const $table = $('h3:contains("新規公開株式")').next().find('table');
			if ( !$table.length ) return null;

			//---- 申込ボタン
			$table.find('a:contains("抽選申込")').each(function () {
				name = $(this).parent().next().find('.txt-code').parent().text().trim();
				$(this)[0].click();
				//----最初のアイテムをクリックしたらループ終了しておく
				return false; // break
			});
			return name;
		});
		const code = getCode(name);
		if ( code ) {
			//---- todo: clickConfirm後に移動
			bookBilled.push( util.findItem(code, items) );
		}
		return code;
	} catch (err) {
		logger.e(err);
		errs.push('[clickBookBill]'+JSON.stringify(err));
	}
}

Nomura.prototype.checkTerms = async () => {
	try {
		await page.waitForSelector('input[name="diKn1"]');
		await page.evaluate((secInfo, userId) => {
			/*規約チェックボックス*/
			$('input[name="diKn1"]').prop("checked",true);
			$('input[name="diKn2"]').prop("checked",true);
			$('input[name="diKn3"]').prop("checked",true);
			/* 確認ボタン */
			var btn = $('button[value="buttonAgree"]')[0];
			if ( btn ) btn.click();
		}, secInfo, userId);
	} catch (err) {
		logger.e(err);
		errs.push('[checkTerms]'+JSON.stringify(err));
	}
}

//// 目論見書確認
Nomura.prototype.checkProspectus = async () => {
	try {
		await page.waitForSelector('a.apl-js-cmspsp');
		await page.click('a.apl-js-cmspsp');

	    ////---- popupウィンドウで目論見書確認
	    const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
	    const popup = await newPagePromise;
   	    await popup.waitForTimeout(param.NM_WAIT*2);
   	    await popup.close();

		////---- 同意ボタン
		await page.click('.btn-proceed');
	} catch (err) {
		logger.e(err);
		errs.push('[checkProspectus]'+JSON.stringify(err));
	}
}

//// 申込内容確認
Nomura.prototype.confirmApplication = async () => {
	try {
		await page.waitForSelector('button[value="buttonConfirm"]');
		await page.click('button[value="buttonConfirm"]');

	} catch (err) {
		logger.e(err);
		errs.push('[confirmApplication]'+JSON.stringify(err));
	}
}

Nomura.prototype.executeApplication = async () => {
	try {
		await page.waitForSelector('input#passwd');
		//---- 取引パスワード
		await page.type('input#passwd', secInfo.users[userId].trade_password);
		//---- 確認ボタン
		await page.click('button[value="buttonAccept"]');
	} catch (err) {
		logger.e(err);
		errs.push('[executeApplication]'+JSON.stringify(err));
	}
}


Nomura.prototype.close = async () => {
	if ( browser ) await browser.close();
}

Nomura.prototype.getItems = function () {
	return items || [];
}

/* BookBillできるItems */
Nomura.prototype.getItemsToBookBill = function () {
	return itemsToBookBill || [];
}

/* BookBilledしたItems */
Nomura.prototype.getBookBilled = function () {
	return bookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Nomura.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Nomura.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Nomura.prototype.getNotes = function () {
	return notes || [];
}


/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードの抽出
		item.code = getCode(item.name);

		//// 銘柄コードの抽出
		item.name = getName(item.name);

		//// 抽選日の抽出
		var date = /(?:[0-9]+\/)?([0-9]+\/[0-9]+)/gim
		.check( item.resultDate.text );

		if ( date ) {
			var unixTime = util.getUnixTimeFromText(date[0].captures[0].trim());
			if ( unixTime ) item.resultDate.unixTime = unixTime;
		} else {
			logger.w('抽選日が抽出できていません ----> '+item.name+'('+item.code+') : 抽選日='+item.resultDate.text);
		}
	}

	extractBookBillTarget();

}

/**
 * ブックビル対象の抽出・ソート
 * 
 */
function extractBookBillTarget () {
	if ( !items ) return;
	items.forEach(item => {
		if ( item.enable ) {
			itemsToBookBill.push(item);
		}
	});
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];	
	var target = items || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄名からcodeを抽出
*/
function getCode (name) {
	if ( !name ) return null;
		//// 銘柄コードの抽出
		var code =
		// /^\s*([0-9][0-9][0-9][0-9])/gim
		/^\s*([0-9]{4})/gim
		.check( name );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}

/**
 銘柄名のみ抽出（スクレイプ直後は銘柄名だけでなくコードが混ざっている）
*/
function getName (__name) {
	if ( !__name ) return null;
	//// 銘柄名のみ抽出
	var name =
	/[0-9]{4}(.+?)$/gim
	.check( __name );
	if ( name ) name = name[0].captures[0].trim();
	return name;
}


module.exports = Nomura;
