'use strict'

/**
 nightmareのevaluateはブラウザスコープなのでscraping先のコンテンツにjqueryがない場合はローカルのjqueryを使用
 jqueryはトップ階層に置く必要がある
 ローカルのjqueryを使用する場合、evaluateの直前に.inject('js', jquery)を記述
 */
var jquery = 'jquery-3.2.0.slim.min.js';

var Nightmare = require('nightmare'),
  	param     = require('../../common/param'),
  	secInfo   = require('../../cred/user').sec['sbi'],
  	logger    = require('../logger')('sbi-core'),
  	// mongo     = require('./mongo')(),
	  util      = require('../../common/util');

/**
 * nightmareでiframeを使用するための設定(1)
 * iframe内のボタンクリック等に必要
 */
require('nightmare-iframe-manager')(Nightmare);

/**
 * nightmareでpopupを操作するための設定
 * https://github.com/rosshinkley/nightmare-window-manager
 */
require('nightmare-window-manager')(Nightmare);
 

var nightmare;
var userId;
var popupWindowId;

/*オブジェクトプロパティにしてもいいかもしれない(this)
  Sbiオブジェクトを複数生成した場合は一般的な変数定義では共用されてしまう?*/
var items;
var itemsToBookBill;
var bookBilled;
var errs; // エラーメッセージ配列
var notes; // 備考配列

// var isDisp; // nm表示フラグ

var isDebug; // nmデバッグモードフラグ

function Sbi ( __userId, __isDisp/*option:nm表示*/, __isDebug/*nmデバッグモード*/ ) {

	userId = __userId;
	isDebug = __isDebug;
	
	// this.isDisp = __isDisp || param.NM_DISPLAY;
	this.isDisp = __isDisp;

	makeNightmareInstance(this.isDisp);
	// makeNightmareInstance(isDispNm);
	this.promise = Promise.resolve();

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;	


}

// function makeNightmareInstance () {
function makeNightmareInstance (__isDisp) {

	if (nightmare) return; // singleton

	// var isDispNm = __isDispNm || param.NM_DISPLAY;
	// isDisp = isDisp || param.NM_DISPLAY;

	nightmare = Nightmare({
		// show   : param.NM_DISPLAY,
		// show   : isDispNm,
		show   : __isDisp,
		width  : param.NM_W,
		height : param.NM_H,

		/**
		 * nightmareでiframeを使用するための設定(2)
		 * この設定でiframe内のscraping(read)ができるようになる
		 */
		webPreferences: {
			webSecurity: false,
		},

 	});
}

function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
}


Sbi.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);	
	return this;
}

Sbi.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

Sbi.prototype.open = function () {

	init();

	// errs = []; // 適当にここで初期化
	// notes = []; // 適当にここで初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
		    .windowManager() // popup window操作に必要(nightmare-window-manager)
			.goto(secInfo.url)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[open]'+err);
				errs.push('[open]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.login = function () {
	// bookBilled = []; // 適当にここで初期化
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			// .inject('js', jquery)
			.evaluate( (secInfo, userId) => {
				$('#user_input input[name="user_id"]').val(secInfo.users[userId].username);
				$('#password_input input[name="user_password').val(secInfo.users[userId].password);
			}, secInfo, userId)
			.click('input[name="ACT_login"]')
			.wait(param.NM_WAIT)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[login]'+err);
				errs.push('[login]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}


Sbi.prototype.gotoHome = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*4)
			.inject('js', jquery)			
			.evaluate(() => {
				var btn = $('#navi01').find('a').has('img[alt="ホーム"]')[0];
				if ( btn ) 	btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[gotoHome]'+err);
				errs.push('[gotoHome]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.openNoticeWindow = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.evaluate(() => {
				var btn = $('.tp-list-importantlink a')[0];
				if ( btn ) 	btn.click();
			})
			/**
			 popup window
			 **/
			.waitWindowLoad() // plugin 'nightmare-window-manager'
		    .currentWindow()  // plugin 'nightmare-window-manager'
			.then(window => {
				popupWindowId = window.id;
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[openNoticeWindow]'+err);
				errs.push('[openNoticeWindow]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.searchNotice = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			// .focusWindow(popupWindowId)
			.wait(param.NM_WAIT*2)
			.evaluateWindow(() => { // plugin 'nightmare-window-manager'
				var num = 0;
				/** Table抽出 **/
				$('td:contains("メッセージ有効期限")').closest('tbody').children('tr').each(function() {
					var status = $(this).children('td').eq(2).text().trim();
					if ( status == '未確認' ) {
						num++;
					}
				});
				return num;
			})
			.then(num => {
				resolve(num);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[searchNotice]'+err);
				errs.push('[searchNotice]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.openNoticeItem = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			// .focusWindow(popupWindowId)
			.wait(param.NM_WAIT*2)
			.evaluateWindow(() => { // plugin 'nightmare-window-manager'
				// var title;
				/** Table抽出 **/
				$('td:contains("メッセージ有効期限")').closest('tbody').children('tr').each(function() {
					var status = $(this).children('td').eq(2).text().trim();
					if ( status == '未確認' ) {
						var link = $(this).children('td').eq(1).find('a')[0];
						// title = link.text();
						if ( link ) link.click();
						return false; // break
					}
				});
				// return title; // 動作が止まるためコメントアウト
			})
			// .then(title => {
			// 	resolve(title);
			// })
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[openNoticeItem]'+err);
				errs.push('[openNoticeItem]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Sbi.prototype.confirmNoticeItem = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			// .focusWindow(popupWindowId)
			.wait(param.NM_WAIT*2)
			.evaluateWindow(() => { // plugin 'nightmare-window-manager'
				// 確認ボタン
				var btn = $('input[name="ACT_estimate"]')[0];
				if ( btn ) btn.click();

				var title = $('.mtext').has('b').first().text().trim();
				return title;
			})
			.then(title => {
				resolve(title);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[confirmNoticeItem]'+err);
				errs.push('[confirmNoticeItem]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Sbi.prototype.closeNoticeItem = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			// .focusWindow(popupWindowId)
			.wait(param.NM_WAIT*2)
			.evaluateWindow(() => { // plugin 'nightmare-window-manager'
				// メッセージを閉じる
				var btn = $('input[name="ACT_backViewInfoList"]')[0];
				if ( btn ) btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[closeNoticeItem]'+err);
				errs.push('[closeNoticeItem]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


Sbi.prototype.closeNoticeWindow = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			/**
			 popup window close
			 **/
			.closeWindow(popupWindowId) // plugin 'nightmare-window-manager'
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[closeNoticeWindow]'+err);
				errs.push('[closeNoticeWindow]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.gotoIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.evaluate(() => {
				var btn = $('#navi01_domestic_menu a:contains("IPO")')[0];
				if ( btn ) 	btn.click();
			})
			.wait(param.NM_WAIT*2)
			.evaluate(() => {
				var btn = $('img[alt*="新規上場株式ブックビルディング"]').parent('a')[0];
				if ( btn ) btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[gotoIpo]'+err);
				errs.push('[gotoIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/**
 IPOページで実行する
*/
Sbi.prototype.getChallengePoint = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.evaluate(() => {
				var point = $('td:contains("目論見書電子交付サービス承諾状況")').parent().prev().find('td:contains("IPOチャレンジポイント")').next().text().trim();
				return point;
			})
			.then(point => {
				resolve(point);
			})
			.catch(err => {
				reject(err);
			});
		});
	});
}

Sbi.prototype.searchIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var __items = [];
				/** Table抽出 **/
				$('td:contains("ブックビル申込内容")').parent().next(':contains("抽選結果")').closest('tbody').each(function() {
					var item = {};
					item.name   = $(this).find('table:contains("目論見書")').find('td').first().text().trim();
					// 抽選日・結果（同じ欄に抽選日と結果が表示されるタイプ。extractInfoで整形）
					item.resultDate = {};
					item.resultDate.text = $(this).find('td:contains("抽選結果")').next().text().trim();
					item.period = $(this).find('td:contains("ブックビル期間")').next().text().trim();
					/* 申込ボタン */
					// var btn = $(this).find('td:contains("ブックビル申込")').next().find('a')[0];
					var btn = $(this).find('td:contains("ブックビル申込")').next().find('a').has('img[alt="申込"]')[0];

					if ( btn ) {
						item.enable = true;
						/* 申込ボタン mongoには保存しない bookBill実行用 */
						// item.btn = btn;
					}
					__items.push(item);
				});
				return __items;
			})
			.then(__items => {
				items = __items;
				// extractInfo(items);
				extractInfo();
				//logger.w(util.getUnixTimeFromText(items[0].result.text));
				resolve(items);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[searchIpo]'+err);
				errs.push('[searchIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Sbi.prototype.clickBookBill = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var name;
				/** Table抽出 **/
				$('td:contains("ブックビル申込内容")').parent().next(':contains("抽選結果")').closest('tbody').each(function() {
					/* 申込ボタン */
					var btn  = $(this).find('td:contains("ブックビル申込")').next().find('a').has('img[alt="申込"]')[0];
					if ( btn ) {
						name = $(this).find('table:contains("目論見書")').find('td').first().text().trim();
						btn.click();
						/* 最初のアイテムをクリックしたらループ終了しておく */
						return false; // break
					}
				});
				return name;
			})
			.then(name => {
				// resolve(getCode(name));
				var code = getCode(name);
				/* todo: clickConfirm後に移動 */
				bookBilled.push( util.findItem(code, items) );
				resolve(code);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickBookBill]'+err);
				errs.push('[clickBookBill]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Sbi.prototype.inputBookBill = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate( (secInfo, userId) => {
				/* 数量 */
				$('input[name="suryo"]').val('10000');
				/* ストライクプライス */
				$('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				$('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);
				/* 確認ボタン */
				var btn = $('input[name="order_kakunin"]')[0];
				if ( btn ) btn.click();
			}, secInfo, userId)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[inputBookBill]'+err);
				errs.push('[inputBookBill]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Sbi.prototype.clickConfirm = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('input[name="order_btn"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			})
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickConfirm]'+err);
				errs.push('[clickConfirm]'+JSON.stringify(err));
				resolve(false);
			});
		});
	});
}

/**
 * nightmare終了
 */
Sbi.prototype.end = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.end()
			.then(() => {
				resolve();
				nightmare = null;
			})
			.catch(err => {
				logger.e(err);
				errs.push('[end]'+JSON.stringify(err));
				resolve(err); // resolveとして処理
				nightmare = null;
			});
		});
	});
}


Sbi.prototype.getItems = function () {
	return items || [];
}

/* BookBillできるItems */
Sbi.prototype.getItemsToBookBill = function () {
	return itemsToBookBill || [];
}

/* BookBilledしたItems */
Sbi.prototype.getBookBilled = function () {
	return bookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Sbi.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Sbi.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Sbi.prototype.getNotes = function () {
	return notes || [];
}

/* nm表示フラグ */
// Sbi.prototype.isDisp = function () {
// 	return isDisp;
// }




/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
// function extractInfo (items) {
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードの抽出
		item.code = getCode(item.name);

		//// 銘柄名の抽出
		item.name = getName(item.name);

		//// 抽選日(UnixTime)の抽出
		var unixTime = util.getUnixTimeFromText(item.resultDate.text);
		if ( unixTime ) {
			item.resultDate.unixTime = unixTime;
		} else {
			//// 抽選日が抽出できない場合は抽選結果が表示されていると判断			

			//// 抽選結果
			item.result = item.resultDate.text;
			item.resultDate = null;
		}
	}

	extractBookBillTarget();

}


/**
 * ブックビル対象の抽出・ソート
 * 
 */
function extractBookBillTarget () {

	// itemsToBookBill = [];

	if ( !items ) return;

	items.forEach(item => {
		if ( item.enable ) {
			itemsToBookBill.push(item);
		}
	});
	
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];	
	var target = items || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄名からcodeを抽出
*/
function getCode (name) {
	if ( !name ) return null;
		//// 銘柄コードの抽出
		var code =
		/[(（]\s*([0-9]+)\s*[)）]/gim
		.check( name );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}

/**
 銘柄名のみ抽出（スクレイプ直後は銘柄名だけでなくコードや上場市場名が混ざっている）
*/
function getName (__name) {
	if ( !__name ) return null;
	//// 銘柄名のみ抽出
	var name =
	/^(.+?)[(（][0-9]{4}[)）]/gim
	.check( __name );
	if ( name ) name = name[0].captures[0].trim();
	return name;
}

module.exports = Sbi;
