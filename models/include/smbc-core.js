'use strict'

/**
 nightmareのevaluateはブラウザスコープなのでscraping先のコンテンツにjqueryがない場合はローカルのjqueryを使用
 jqueryはトップ階層に置く必要がある
 ローカルのjqueryを使用する場合、evaluateの直前に.inject('js', jquery)を記述
 */
var jquery = 'jquery-3.2.0.slim.min.js';

var Nightmare = require('nightmare'),
	param     = require('../../common/param'),
	secInfo   = require('../../cred/user').sec['smbc'],
	logger    = require('../logger')('smbc-core'),
	// mongo     = require('./mongo')(),
	util      = require('../../common/util'),
	path      = require('path'),
	scheduler = require('../scheduler');


/**
 * nightmareでpopupを操作するための設定
 * https://github.com/rosshinkley/nightmare-window-manager
 */
require('nightmare-window-manager')(Nightmare);
 

var nightmare;
var userId;
var popupWindowId;

/*オブジェクトプロパティにしてもいいかもしれない(this)
  Sbiオブジェクトを複数生成した場合は一般的な変数定義では共用されてしまう?*/
var items;
var itemsToBookBill; // ブックビル候補（優先順位でソートした配列）
var bookBilled; 　　　// ブックビルした銘柄配列
var errs; // エラーメッセージ配列
var notes; // 備考配列

// var isDisp; // nm表示フラグ

var isDebug; // nmデバッグモードフラグ

var money; // 買付可能額
// var alreadyBookBilledCodes; // 申込済み銘柄コード(SMBCは申込済みでも申込ボタンが表示されるためフィルタリングに使用)
var alreadyBookBilled; // 申込済み銘柄倍列(SMBCは申込済みでも申込ボタンが表示されるためフィルタリングに使用)


function Smbc ( __userId, __isDisp/*option:nm表示*/, __isDebug/*option:nmデバッグモード*/ ) {

	userId = __userId;
	isDebug = __isDebug;
	
	// this.isDisp = __isDisp || param.NM_DISPLAY;
	this.isDisp = __isDisp;

	// makeNightmareInstance(isDispNm);
	makeNightmareInstance(this.isDisp);
	this.promise = Promise.resolve();

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;	

}

function makeNightmareInstance (__isDisp) {
// function makeNightmareInstance () {

	if (nightmare) return; // singleton

	// var isDispNm = __isDispNm || param.NM_DISPLAY;
	// isDisp = isDisp || param.NM_DISPLAY;

	nightmare = Nightmare({
		// show   : param.NM_DISPLAY,
		// show   : isDispNm,
		show   : __isDisp,
		width  : param.NM_W,
		height : param.NM_H,

		/**
		 *  nightmare(v2.10.0)のデフォルトのelectron(v1.8.8)では野村證券にログインできない
		 *  これは、野村證券はchrome51~66は使用できないため
		 *  electron(v1.8.8)はchrome59 (対比表:https://github.com/electron/releases)
		 *  最新のnightmare(v3.0.2)でもelectronが古く野村證券に対応できない
		 *  ---->
		 * 　electron(v4.0.0)を個別にインストールしてnightmareでelectronを指定して使用（electron最新版はv15だがnightmareがうまく動作しない）
		 * 　(https://github.com/segmentio/nightmare)
		 * 
		 *   nightmareのバージョンはv2.10.0をそのまま使用
		 * 　nightmare-iframe-manager(sbi.jsで使用)がこのバージョンを必要としているため
		 * 
		 */
		// electronPath: require('electron'),

		/**
		 * 目論見書のリンクをクリックすると保存ダイアログが表示されて処理が止まるため
		 * 保存ダイアログを出さないように（実際にはwindow.openイベントで何もしないように）するため
		 * nm-preload.jsでイベント処理をカスタムしている
		 * 
		 * todo: 目論見書以外でwindow.openイベントを処理したい場合はどうするか
		 *       設定あり・なしのnightmareインスタンスを2つ生成して対応とか
		 * 
		 */
		// webPreferences: {
		// 	preload: path.resolve(__dirname, 'nm-preload.js'),
		// },

 	});
}

//// 変数初期化
function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
	alreadyBookBilled = [];
}


Smbc.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);	
	return this;
}

Smbc.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

Smbc.prototype.open = function () {

	init();

	// errs = []; // 適当にここで初期化
	// notes = []; // 適当にここで初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
		    .windowManager() // popup window操作に必要(nightmare-window-manager)
			.goto(secInfo.url)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[open]'+err);
				errs.push('[open]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Smbc.prototype.login = function () {
	// bookBilled = []; // 適当にここで初期化
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.inject('js', jquery) // SMBCはjQueryがないページが多いのでデフォルトでinjectしておく
			.evaluate( (secInfo, userId) => {
				$('input[name="koza1"]').val(secInfo.users[userId].branch_code);
				$('input[name="koza2"]').val(secInfo.users[userId].username);
				$('input[name="passwd"]').val(secInfo.users[userId].password);
			}, secInfo, userId)
			.wait(param.NM_WAIT)
			.click('input[name="logIn"]')
			.wait('img[name="btn_logout"]')
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[login]'+err);
				errs.push('[login]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Smbc.prototype.fetchMoney = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('img[alt="残高詳細"]').parent('a')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("MRF・お預り金")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				return $('th:contains("買付可能額")').next().text();
			})
			.then(__money => {
				money = util.extractNumber(__money);
				resolve(money);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[fetchMoney]'+err);
				errs.push('[fetchMoney]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Smbc.prototype.gotoIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('img[alt="お取引"]').parent('a')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("新規公開株/公募・売出")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[gotoIpo]'+err);
				errs.push('[gotoIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

// Smbc.prototype.fetchAlreadyBookBilledCodes = function () {
Smbc.prototype.fetchAlreadyBookBilled = function () {

	// alreadyBookBilled = []; // 初期化

	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('div:contains("申告状況一覧")').parent('a')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var __items = [];
				$('td span:contains("申告済")').each(function () {
					var item = {};
					// var item = $(this).closest('tr').find('td').first().text();
					item.code   = $(this).closest('tr').find('td').first().text(); // 整形必要
					var $result = $(this).parent().next().find('img');
					if ( $result.length ) {
						item.result = $result.attr('alt');
					}

					__items.push(item);
				});
				return __items;
			})
			.then(__items => {
				//// コードの抽出
				// var codes = [];
				for (var i=0; i<__items.length; i++) {
					var item = __items[i];
					item.code = getCode(item.code);
					// codes.push(getCode(__items[i]));
				}
				// return codes;
				return __items;
			})
			// .then(codes => {
			.then(__items => {				
				// for (var i=0; i<codes.length; i++) {
				for (var i=0; i<__items.length; i++) {
					// var code = codes[i];
					var item = __items[i];
					alreadyBookBilled.push({
						code: item.code,
						name: scheduler.getName(item.code),
						result: item.result,
					});
				}
				resolve(alreadyBookBilled);
				// alreadyBookBilledCodes = codes;
				// resolve(alreadyBookBilledCodes);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[fetchAlreadyBookBilled]'+err);
				errs.push('[fetchAlreadyBookBilled]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Smbc.prototype.searchIpo = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var btn = $('a:contains("新規公開株/公募・売出")')[0];
				if ( btn ) btn.click();
			})
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(() => {
				var __items = [];
				/** ブロック抽出 **/
				// $('img[alt="需要申告受付中"]').each(function() {
				$('span:contains("受付状況")').each(function () {
					var $info = $(this).closest('table');
					var $code = $info.prev().find('span:contains("銘柄コード")');

					var item = {};
					item.name = $code.prev().text().trim();
					item.code = $code.text().trim(); // 整形必要

					//// 抽選日
					item.resultDate = {};
					item.resultDate.text = $info.find('span:contains("抽選日")').parent().next().text().trim();

					//// 申込期間
					item.period = $info.find('span:contains("需要申告期間")').parent().next().text().trim();

					//// 価格
					item.price = $info.find('span:contains("仮条件提示日")').parent().next().text().trim();

					//// 申込ボタン
					var btn = $info.find('img[alt="需要申告受付中"]').parent()[0];
					if ( btn ) {
						item.enable = true;
					}

					__items.push(item);
				})
				return __items;

				// 	var item = {};
				// 	item.name   = $(this).find('.ttl-stock').find('span').first().text().trim();
				// 	item.result = {};
				// 	//// 抽選日
				// 	item.result.text = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("売出価格決定日")').next().text().trim();
				// 	//// 抽選結果（当選数量）
				// 	var winning = 
				// 	item.winning = $(this).find('.ttl-lvl-3:contains("購入情報")').next().find('th:contains("当選数量")').next().text().trim();
				// 	item.period = $(this).find('.ttl-lvl-3:contains("募集情報")').next().find('th:contains("抽選参加申込期間")').next().text().trim();
				// 	/* 申込ボタン */
				// 	//// 申し込み完了した後は"抽選申込取消へ"に変わる
				// 	var btn = $(this).find('.action-btn a:contains("抽選申込へ")')[0];

				// 	if ( btn ) {
				// 		item.enable = true;
				// 		/* 申込ボタン mongoには保存しない bookBill実行用 */
				// 		// item.btn = btn;
				// 	}
				// 	__items.push(item);
				// });
				// return __items;
			})
			.then(__items => {
				items = __items;
				extractInfo();
				resolve(itemsToBookBill);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[searchIpo]'+err);
				errs.push('[searchIpo]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

Smbc.prototype.clickBookBill = function (__code) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate(__code => {
				var code;
				/** ブロック抽出 **/
				$('img[alt="需要申告受付中"]').each(function() {
					/* 申込ボタン */
					var btn = $(this).parent()[0];
					/* 銘柄コード*/
					code = $(this).closest('table').prev().find('span:contains("銘柄コード")').text().trim();

					if ( code.indexOf(__code) !== -1 ) {
						// mame = $code.prev().text().trim();
						btn.click();
						return false; // break
					} else {
						code = null;
					}
				});
				return code; // 整形必要
			}, __code)
			.then(code__ => {
				if ( code__ ) {
					var code = getCode(code__);
					/* todo: clickConfirm後に移動 */
					bookBilled.push( util.findItem(code, items) );
					resolve(code);
				} else {
					errs.push('[clickBookBill] 銘柄コード抽出に失敗しています');
					resolve(null);
				}
				// var code = getCode(code__);
				// /* todo: clickConfirm後に移動 */
				// bookBilled.push( util.findItem(code, items) );
				// resolve(code);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickBookBill]'+err);
				errs.push('[clickBookBill]'+JSON.stringify(err));
				resolve(null); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


Smbc.prototype.checkTerms = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate( (secInfo, userId) => {
				/* 数量 */
				// $('input[name="suryo"]').val('10000');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/*規約チェックボックス*/
				$('input[name="confirmCheck"]').prop("checked",true);

				/* 確認ボタン */
				var btn = $('input[alt="次へ"]')[0];
				if ( btn ) btn.click();
			}, secInfo, userId)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkTerms]'+err);
				errs.push('[checkTerms]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Smbc.prototype.inputBookBill = function (item) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			.inject('js', jquery)
			.evaluate( item => {
				/* 数量 */
				$('input[name="snkokSu"]').val('100');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/* 申告価格 */
				// $('select[name="snkokKakaku"]').prop("selectedIndex", 1);
				$('select[name="snkokKakaku"]').val(item.price);

				/* 確認ボタン */
				var btn = $('input[alt="申告内容確認"]')[0];
				if ( btn ) btn.click();
			}, item)
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[inputBookBill]'+err);
				errs.push('[inputBookBill]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


//// 目論見書確認画面
Smbc.prototype.checkProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/**
				 * 目論見書を強制的に閲覧状態化
				 * ----> 初回は閲覧状態として判定されない(cookieとかで判定しているのかも)
				 */
				// $('a.apl-js-cmspsp').addClass('apl-js-read');

				/**
				 * 目論見書のリンクをクリックするとnightmare(electron)では
				 * 保存ダイアログが表示されてしまう
				 * ----> 保存ダイアログを表示させないようにnm-preload.jsでカスタムしている
				 * 
				 */
				$('a.apl-js-cmspsp').click();

				/* 同意ボタン */
				var btn = $('.btn-proceed');
				if ( btn ) {
					/**
					 * 目論見書を見ずに強制的にenable化した場合の処理
					 * ----> 目論見書リンクをクリックする処理に変更したのでこの処理は不要かもしれないが入れていても動作する
					 * 
					 */
					btn.prop('disabled', false);
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[checkProspectus]'+err);
				errs.push('[checkProspectus]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

//// 申込内容確認
Smbc.prototype.confirmApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('button[name="buttonConfirm"]')[0];
				if ( btn ) {
					btn.click();
				}
			})
			.then(() => {
				resolve(true);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[confirmApplication]'+err);
				errs.push('[confirmApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}

Smbc.prototype.executeApplication = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate( (secInfo, userId) => {
				/* 取引パスワード */
				$('input#passwd').val(secInfo.users[userId].trade_password);

				/* 確認ボタン */
				var btn = $('button[name="buttonAccept"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			}, secInfo, userId)
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[executeApplication]'+err);
				errs.push('[executeApplication]'+JSON.stringify(err));
				resolve(false); // Promise.allで途切れないようにrejectしていない（確か）
			});
		});
	});
}


/* 目論見書の確認 */
/**
 * 
 * 目論見書の確認
 * PDFリンクを開くとnightmareが「保存」ダイアログを開き進まなくなるので
 * スキップ（PDFを開かず同意ボタンを強制的にenable化して対応）
 */
Smbc.prototype.openProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var pdf = $('th:contains("閲覧書類")').next().find('a:contains("目論見書")')[0];
				if ( pdf ) 	pdf.click();
			})
			/**
			 popup window
			 **/
			.waitWindowLoad() // plugin 'nightmare-window-manager'
		    .currentWindow()  // plugin 'nightmare-window-manager'
			.then(window => {
				popupWindowId = window.id;
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[openProspectus]'+err);
				errs.push('[openProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Smbc.prototype.closeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT*2)
			/**
			 popup window close
			 **/
			.closeWindow(popupWindowId) // plugin 'nightmare-window-manager'
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[closeProspectus]'+err);
				errs.push('[closeProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}

/*未使用*/
Smbc.prototype.agreeProspectus = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.evaluate(() => {
				var btn = $('.btn-proceed')[0];
				if ( btn ) btn.click();
			})
			.then(() => {
				resolve();
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[agreeProspectus]'+err);
				errs.push('[agreeProspectus]'+JSON.stringify(err));
				reject(err);
			});
		});
	});
}


Smbc.prototype.clickConfirm = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.wait(param.NM_WAIT)
			.inject('js', jquery)
			.evaluate(() => {
				/* 確認ボタン */
				var btn = $('input[alt="需要申告"]')[0];
				if ( btn ) {
					btn.click();
					return true;
				} else {
					return false;
				}
			})
			.then(status => {
				resolve(status);
			})
			.catch(err => {
				logger.e(err);
				// errs.push('[clickConfirm]'+err);
				errs.push('[clickConfirm]'+JSON.stringify(err));
				resolve(false);
			});
		});
	});
}

/**
 * nightmare終了
 */
Smbc.prototype.end = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			nightmare
			.end()
			.then(() => {
				resolve();
				nightmare = null;
			})
			.catch(err => {
				logger.e(err);
				errs.push('[end]'+JSON.stringify(err));
				resolve(err); // resolveとして処理
				nightmare = null;
			});
		});
	});
}


Smbc.prototype.getItems = function () {
	// items = items || [];
	return items || [];
}

/* BookBillできるItems(優先順位でソートしたもの) */
Smbc.prototype.getItemsToBookBill = function () {
	// itemsToBookBill = itemsToBookBill || [];
	return itemsToBookBill || [];
}

/* BookBillしたItems */
Smbc.prototype.getBookBilled = function () {
	// bookBilled = bookBilled || [];
	return bookBilled || [];
}

/* 既にBookBillしているItems */
Smbc.prototype.getAlreadyBookBilled = function () {
	// alreadyBookBilled = alreadyBookBilled || [];
	return alreadyBookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Smbc.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Smbc.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Smbc.prototype.getNotes = function () {
	return notes || [];
}

/* nm表示フラグ */
// Smbc.prototype.isDisp = function () {
// 	return isDisp;
// }


/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
// function extractInfo (items) {
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードの抽出
		item.code = getCode(item.code);

		//// 抽選日の抽出
		//// todo: 年をまたぐ場合に対応
		if ( item.resultDate ) {
			//// 何らかの理由でスクレイプ済み（日付整形済み）のitemの場合を考慮(xx月xx日が整形済みでxx/xx形式になっている場合)
			// var date = /([0-9]+月[0-9]+日)/gim
			var date = /([0-9]+[月/][0-9]+日?)/gim
			.check( item.resultDate.text );

			if ( date ) {
				var dateText = date[0].captures[0].replace(/月/, '/').replace('日', '').trim();
				var unixTime = util.getUnixTimeFromText(dateText);
				if ( unixTime ) item.resultDate.unixTime = unixTime;
			} else {
				logger.w('抽選日が抽出できていません ----> '+item.name+'('+item.code+') : 抽選日='+item.resultDate.text);
			}
			// try {
			// 	var dateText = date[0].captures[0].replace(/月/, '/').replace('日', '').trim();
			// 	var unixTime = util.getUnixTimeFromText(dateText);
			// 	if ( unixTime ) item.resultDate.unixTime = unixTime;
			// } catch (err) {
			// 	logger.e(err);
			// 	// errs.push('[extractInfo]'+err);
			// 	errs.push('[extractInfo]'+JSON.stringify(err));
			// }
		}

		//// 価格の抽出（仮条件の上限価格）
		logger.w(item.price);

		var price = /([0-9,]+)\s*円/gim
		var price = /([0-9,]+)\s*円?/gim
		.check( item.price );
		if ( price ) {
			if (price.length == 2 ) {
				item.price = util.extractNumber(price[1].captures[0].trim());
			} else {
				item.price = util.extractNumber(price[0].captures[0].trim());
			}
		}
	}

	extractBookBillTarget();

}

/**
 * ブックビル対象の抽出・ソート
 * 主幹事銘柄が最優先、次にランクで優先順位を設定（主幹事銘柄もランク順）
 * 
 */
function extractBookBillTarget () {
	var items_mainSec = []; // SMBCが主幹事の銘柄
	var items_not_mainSec = []; // 主幹事ではない銘柄

	// itemsToBookBill = [];
	
	for ( var i=0; i<items.length; i++) {
		var item = items[i];

		//// 申込可能判定 
		if ( !item.enable ) continue;

		item.rank = scheduler.getRank(item.code);
		item.mainSec = scheduler.getMainSec(item.code);

		//// 申込済み判定
		// if ( alreadyBookBilledCodes.includes(item.code) ) {
		if ( util.findItem(item.code, alreadyBookBilled) ) {
			logger.w('既にブックビル済みの銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
			continue;
		}

		//// RANK判定
		if ( item.rank && util.isTargetIpoRank(item.rank, true/*isForBookBill*/) ) {
			//// 主幹事判定
			if ( /SMBC/.test(item.mainSec) ) {
				items_mainSec.push(item);
			} else {
				items_not_mainSec.push(item);
			}
		} else {
			logger.w('ターゲットRankではない銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
			continue;
		}

	}

	//// ランク順にソート
	sortItems(items_mainSec);
	sortItems(items_not_mainSec);

	var targets = items_mainSec.concat(items_not_mainSec);

	//// 予算判定
	if ( !money ) {
		var msg = '口座残高が取得できていないので予算判定をスキップ'; 
		logger.e(msg);
		errs.push('[extractBookBillTarget]'+msg);
		itemsToBookBill = targets;
	} else {
		for (var i=0; i<targets.length; i++) {
			var item = targets[i];
			if ( !item.price ) {
				var msg = '仮条件価格が取得できていないのでブックビル対象から除外 ----> '+item.name+' ('+item.code+')'; 
				logger.e(msg);
				errs.push('[extractBookBillTarget]'+msg);
				continue;
			}
			if ( money >= (item.price*100) ) {
				itemsToBookBill.push(item);
				money = money - item.price*100;
				logger.d('予算残高: '+money);
			} else {
				var msg = '予算オーバーのためブックビル対象から除外 ----> 予算残高: '+money+'円 '+item.name+'('+item.code+') Rank【'+item.rank+'】'+item.price+'円';
				logger.w(msg);
				notes.push(msg);
			}
		}
	}

	logger.w('BookBill対象銘柄: '+itemsToBookBill.length+'銘柄');
	logger.i(itemsToBookBill);

}

/**
 * Rankでソート
 */
function sortItems (__items) {
	for (var i=0; i<__items.length; i++) {
		var item = __items[i];
		//// 評価用に一時的なプロパティ追加		
		switch(item.rank) {
			case 'S':
			item.score = 100;
			break;
			case 'A':
			item.score = 80;
			break;
			case 'B':
			item.score = 60;
			break;
			case 'C':
			item.score = 40;
			break;
			case 'D':
			item.score = 20;
			break;
			default:
			item.score = 0;
		}
	}
	__items.sort( function (a, b) {
	    if ( a.score > b.score ) return -1;
	    if ( a.score < b.score ) return 1;
	    return 0;
	});	
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];	
	var target = alreadyBookBilled || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄コードを含むテキストからコードを抽出
*/
function getCode (text) {
	if ( !text ) return null;
		//// 銘柄コードの抽出
		var code =
		// /^\s*([0-9][0-9][0-9][0-9])/gim
		/([0-9][0-9][0-9][0-9])/gim
		.check( text );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}


module.exports = Smbc;
