'use strict'

const puppeteer = require('puppeteer'),
	  param     = require('../../common/param'),
	  secInfo   = require('../../cred/user').sec['monex'],
	  logger    = require('../logger')('monex-core'),
	  util      = require('../../common/util'),
	  path      = require('path'),
	  scheduler = require('../scheduler');

/****************
 * MonexはHeadlessモード（ブラウザ非表示）の場合Access Deniedとなる(ロボットと判定される)
 * ロボット判定を回避するpluginを使用(puppeteer-extra, puppeteer-extra-plugin-stealth)
 * https://github.com/berstend/puppeteer-extra/tree/master/packages/puppeteer-extra-plugin-stealth
 * 
 * 結局、popupはAccess Deniedとなるため使用しない
 * ----> MonexはHeadlessモートを使用せずブラウザ表示で運用

// const puppeteer = require('puppeteer-extra'),
// const StealthPlugin = require('puppeteer-extra-plugin-stealth')
// puppeteer.use(StealthPlugin());
 ****************/ 

async function injectJQuery (page) {
	await page.addScriptTag({ path: 'jquery-3.2.0.slim.min.js' }); // ルートDIRのファイルが読み込まれる
}

// var nightmare;
// var popupWindowId;

const domain = 'https://mxp2.monex.co.jp';

let browser;
let page;
let userId;

/*
 オブジェクトプロパティにしてもいいかもしれない(this)
 Sbiオブジェクトを複数生成した場合は一般的な変数定義では共用されてしまう?
*/
let items;
let itemsToBookBill; // ブックビル候補（優先順位でソートした配列）
let bookBilled;	// ブックビルした銘柄配列
let errs;		// エラーメッセージ配列
let notes;		// 備考配列

let isDebug;	// nmデバッグモードフラグ
let money;		// 買付可能額
let alreadyBookBilled; // 申込済み銘柄倍列(SMBCは申込済みでも申込ボタンが表示されるためフィルタリングに使用)

function Monex ( __userId, __isDisp/*option:nm表示*/, __isDebug/*option:nmデバッグモード*/ ) {
	userId      = __userId;
	isDebug     = __isDebug;
	this.isDisp = __isDisp;

	// makeNightmareInstance(this.isDisp);
	// this.promise = Promise.resolve();

	//// userが存在するかの判定フラグ(agentで使用)
	this.isValid = secInfo.users[userId]? true: false;
}

////---- thisを使う場合は　"=>"の記述は使用不可
// Monex.prototype.init = async () => {
Monex.prototype.init = async function () {	
	if ( browser ) { // singleton
	    await browser.close();
	}
	if ( this.isValid ) {
		await makeBrowser(this.isDisp);
	}
}

async function makeBrowser (isDisp) {
	browser = await puppeteer.launch({
		/***********************************************
		 * MonexはHeadlessモード（ブラウザ非表示）の場合、
		 * Access Deniedとなるため常にブラウザ表示設定に固定
		 ***********************************************/
		// headless: !isDisp, // 反転設定
		headless: false,
		slowMo:   param.BROWSER.DELAY,
	});
	page = await browser.newPage();

	await page.setViewport({
		width: param.BROWSER.W,
		height:param.BROWSER.H
	});

    //---- evaluate内でconsole.logを使用するための設定
    page.on('console', msg => {
      for (let i = 0; i < msg._args.length; ++i)
        console.log(`${i}: ${msg._args[i]}`);
    });	

	//---- puppeteerエラーの検知
	page.on('pageerror', error => {
		logger.e('**Puppeteerエラー**', error.message)
	});

	//---- サイトがtimeout多発の場合に設定
	// await page.setDefaultNavigationTimeout(60000);
}

//// 変数初期化
function init () {
	items = [];
	itemsToBookBill = [];
	bookBilled = [];
	errs  = [];
	notes = [];
	alreadyBookBilled = [];
}

////---- thisを使う場合は　"=>"の記述は使用不可
// Monex.prototype.open = async () => {
Monex.prototype.open = async function () {

	//---- 毎回ブラウザを再起動しておく（野村證券の場合は必要だがMonexでは必須ではない）
	await this.init();

	init();

	try {
		await page.goto(secInfo.url);
	} catch (err) {
		logger.e(err);
		errs.push('[open]'+JSON.stringify(err));
	}
}

Monex.prototype.login = async () => {
	try {
		// await page.screenshot({path: 'example.png'});
		await page.waitForSelector('input#loginid');
		await page.type('input#loginid', secInfo.users[userId].username);
		await page.type('input#passwd',  secInfo.users[userId].password);
		await page.click('button.btn_login_top');
	} catch (err) {
		logger.e(err);
		errs.push('[login]'+JSON.stringify(err));
	}
}

//// 重要書面確認がある場合
Monex.prototype.checkNotice = async () => {
	await page.waitForTimeout(param.NM_WAIT*2);
	////---- 重要書面確認済みボタンの存在チェック
	const btns = await page.$x('.//a[contains(text(), "書面を確認済")]');
	if ( !btns || !btns.length ) {
		logger.d('重要書面確認はありません');
		return;
	}
	const checkedBtn = btns[0];
	////---- 重要書面リンク
	const notices = await page.$$('.f-16 a');
	if ( !notices || !notices.length ) {
		let msg = '重要書面のDOMが見つかりません';
		logger.w(msg);
		errs.push(msg);
		return;
	}
	for ( let notice of notices ) {
		await notice.click();
		let newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
		let popup = await newPagePromise;
		await popup.waitForTimeout(param.NM_WAIT*2);
	  ////---- popupウィンドウはframe構造のため操作するframeを取得
	  let frame = popup.frames().find(frame => frame.name() === 'CT');
		await frame.evaluate(() => {
			$('a:contains("確認しました"), a:contains("閉じる")')[0].click();
		});
		await page.waitForTimeout(param.NM_WAIT*2);
	}
	////---- popupウィンドウで目論見書確認
	await checkedBtn.click();
}

Monex.prototype.fetchMoney = async () => {
	try {
		await page.waitForSelector('span.user-info');
		await injectJQuery(page);
		const __money = await page.evaluate(() => {
			return $('#buyingpower').text();
		});
		money = util.extractNumber(__money);
		logger.d('買付可能額: '+money);
		return money;
	} catch (err) {
		logger.e(err);
		errs.push('[fetchMoney]'+JSON.stringify(err));
	}
}

Monex.prototype.gotoIpo = async () => {
	try {
		await page.evaluate(() => {
			const btn = $('a:contains("新規公開株")')[0];
			if ( btn ) btn.click();
		});
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.evaluate(() => {
			const btn = $('a:contains("ブックビルディング参加")')[0];
			if ( btn ) btn.click();
		});
	} catch (err) {
		logger.e(err);
		errs.push('[gotoIpo]'+JSON.stringify(err));
	}
}


Monex.prototype.fetchAlreadyBookBilled = async () => {
	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.evaluate(() => {
			const btn = $('a:contains("需要申告・購入申込状況")')[0];
			if ( btn ) btn.click();
		});
		await page.waitForTimeout(param.NM_WAIT*2);
		const __items = await page.evaluate(() => {
			const __items = [];
			$('th:contains("受付番号")').closest('thead').next('tbody').find('th:contains("課税")').each(function () {
				var item = {};
				item.name = $(this).prev().text();
				item.result = $(this).next().next().next().next().next().text();
				__items.push(item);
			});
			return __items;
		});
		for (let item of __items) {
			////---- コードの抽出
			const code = getCode(item.name);
			alreadyBookBilled.push({
				code: code,
				name: scheduler.getName(code),
				result: item.result,
			});
		}
		logger.d('ブックビル済み銘柄 :'+JSON.stringify(alreadyBookBilled));
		return alreadyBookBilled;
	} catch (err) {
		logger.e(err);
		errs.push('[fetchAlreadyBookBilled]'+JSON.stringify(err));
	}
}


/**
 * DOMが検索しにくいので銘柄名のみ抽出
 * ブックビルできる銘柄のみ詳細取得(fetchDetail)
 */ 
Monex.prototype.searchIpo = async () => {
	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.evaluate(() => {
			var btn = $('h2:contains("新規公開株")').next().find('a:contains("取扱銘柄一覧")')[0];
			if ( btn ) btn.click();
		});
		await page.waitForTimeout(param.NM_WAIT*2);
		items =	await page.evaluate(() => {
			const __items = [];
			/** ブロック抽出 **/
			$('h2:contains("新規公開株式")').next().find('a:contains("詳細情報")').each(function () {
				let item = {};
				item.infoLink = $(this).attr('href');
				item.name = $(this).parent().prev().text().trim();
				//// 申込ボタン
				let btn = $(this).closest('tr').next().find('a:contains("需要申告")')[0];
				if ( btn ) {
					item.enable = true;
				}
				__items.push(item);
			});
			return __items;
		});
		extractInfo();
		return items;
	} catch (err) {
		logger.e(err);
		errs.push('[searchIpo]'+JSON.stringify(err));
	}
}

Monex.prototype.fetchDetail = async (code) => {
	const item = util.findItem(code, items);
	if ( !item ) {
		logger.e('itemが存在しません');
		return;
	}

	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.goto(domain+item.infoLink);
		await page.waitForTimeout(param.NM_WAIT*2);
		const __item = await page.evaluate(item => {
			item.period = $('th:contains("ブックビルディング（需要申告）期間")').next().text();
			item.price  = $('th:contains("仮条件")').not(':contains("提示")').next().text();
			item.resultDate = {};
			item.resultDate.text = $('th:contains("抽選日")').next().text();
			return item;
		}, item);
		//// items配列の更新
		updateItem(__item);
		extractInfo();
		return items;
	} catch (err) {
		logger.e(err);
		errs.push('[fetchDetail]'+JSON.stringify(err));
		// resolve(null); // Promise.allで途切れないようにrejectしていない（確か）
	}
}

Monex.prototype.clickBookBill = async () => {
	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.evaluate(() => {
			$('a:contains("ブックビルディングに参加")')[0].click();
	    });
	    return true;
	} catch (err) {
		logger.e(err);
		errs.push('[clickBookBill]'+JSON.stringify(err));
		return false;
	}
}

Monex.prototype.inputBookBill = async (item) => {
	try {
		await page.waitForSelector('input#orderNominal');
		await page.evaluate(() => {
				/* 数量 */
				$('input#orderNominal').val('100');
				/* ストライクプライス */
				// $('input#strPriceRadio').prop("checked",true);
				/* 取引パスワード */
				// $('input[name="tr_pass"]').val(secInfo.users[userId].trade_password);

				/* 申告価格 */
				// $('select[name="snkokKakaku"]').prop("selectedIndex", 1);
				$('input[name="orderNari"]').prop('checked', true);

				/* 確認ボタン */
				var btn = $('input[value="次へ（申告内容確認）"]')[0];
				if ( btn ) btn.click();
		});
		return true;
	} catch (err) {
		logger.e(err);
		errs.push('[inputBookBill]'+JSON.stringify(err));
		return false;
	}
}

/**
 * 目論見書確認画面
 * 実装ペンディング(popupでAccess Denied表示される問題解決必要)
 * Monexの場合、目論見書を事前に確認すればサーバー側で閲覧履歴が保存される仕組み
 * 一度確認しておけば目論見書確認ページは表示されない
 * 目論見書を通常ブラウザで確認することでこの処理をスキップできる
 * 
 * ----> puppeteerで対応
 * 
 */ 
Monex.prototype.checkProspectus = async (item) => {
	try {
		await page.waitForTimeout(param.NM_WAIT*2);
		await page.goto(domain+item.infoLink);
		await page.waitForTimeout(param.NM_WAIT*2);		
		await page.evaluate(() => {
			$('a:contains("目論見書")')[0].click();
	    });

	    ////---- popupウィンドウで目論見書確認
	    const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
	    const popup = await newPagePromise;
	    await popup.waitForTimeout(param.NM_WAIT*2);
		// await popup.screenshot({path: 'example2.png'});
	    ////---- popupウィンドウはframe構造のため操作するframeを取得
	    const frame = popup.frames().find(frame => frame.name() === 'CT');
		await frame.evaluate(() => {
			$('a:contains("確認しました"), a:contains("閉じる")')[0].click();
	    });
	    return true;
	} catch (err) {
		logger.e(err);
		errs.push('[checkProspectus]'+JSON.stringify(err));
		return false;
	}
}

Monex.prototype.clickConfirm = async (code) => {
	try {
		await page.waitForSelector('input[value="実行する"]');
		let status = await page.evaluate(() => {
			/* 確認ボタン */
			var btn = $('input[value="実行する"]')[0];
			if ( btn ) {
				btn.click();
				return true;
			} else {
				return false;
			}
		});
		if ( status ) bookBilled.push( util.findItem(code, items) );
		return status;
	} catch (err) {
		logger.e(err);
		errs.push('[clickConfirm]'+JSON.stringify(err));
		return false;
	}
}


Monex.prototype.close = async () => {
    if ( browser ) await browser.close();
}

Monex.prototype.getItems = function () {
	// items = items || [];
	return items || [];
}

/* BookBillできるItems(優先順位でソートしたもの) */
Monex.prototype.getItemsToBookBill = function () {
	// itemsToBookBill = itemsToBookBill || [];
	return itemsToBookBill || [];
}

/* BookBillしたItems */
Monex.prototype.getBookBilled = function () {
	// bookBilled = bookBilled || [];
	return bookBilled || [];
}

/* 既にBookBillしているItems */
Monex.prototype.getAlreadyBookBilled = function () {
	// alreadyBookBilled = alreadyBookBilled || [];
	return alreadyBookBilled || [];
}

/* 抽選結果があるItemsを抽出して返す */
Monex.prototype.getResults = function () {
	return getResults();
}

/* エラーメッセージ配列 */
Monex.prototype.getErrors = function () {
	return errs || [];
}

/* 備考配列 */
Monex.prototype.getNotes = function () {
	return notes || [];
}

/* nm表示フラグ */
// Monex.prototype.isDisp = function () {
// 	return isDisp;
// }


/**
 IPOアイテムから株価コードなどを抽出してプロパティに追加
 */
function extractInfo () {	
	if ( !items || items.length == 0 ) return;
	for (var i=0; i<items.length; i++) {
		var item = items[i];
		if ( !item.name ) {
			var msg = 'IPO銘柄名が抽出できていません ----> '+JSON.stringify(item);
			logger.e(msg);
			errs.push('[extractInfo]'+msg);
			continue;
		}
		//// 銘柄コードと銘柄名の抽出
		if ( !item.code ) {
			item.code = getCode(item.name);
			item.name = getName(item.name);
		}

		//// 抽選日の抽出
		//// todo: 年をまたぐ場合に対応
		//// monexの場合は複数回extractInfoを実行するためextract済みのitemはスキップ
		// if ( item.resultDate ) {
		if ( item.resultDate && !item.resultDate.unixTime ) {			
			var date = /([0-9]+月[0-9]+日)/gim
			// var date = /([0-9]+[月/][0-9]+日?)/gim
			.check( item.resultDate.text );

			if ( date ) {
				var dateText = date[0].captures[0].replace(/月/, '/').replace('日', '').trim();
				item.resultDate.text = dateText;
				var unixTime = util.getUnixTimeFromText(dateText);
				if ( unixTime ) item.resultDate.unixTime = unixTime;
			} else {
				logger.w('抽選日が抽出できていません ----> '+item.name+'('+item.code+') : 抽選日='+item.resultDate.text);
			}
		}

		//// 価格の抽出（仮条件の上限価格）
		// logger.w(item.price);
		if ( item.price ) {
			var price = /([0-9,]+)\s*円?/gim
			.check( item.price );
			if ( price ) {
				if (price.length == 2 ) {
					item.price = util.extractNumber(price[1].captures[0].trim());
				} else {
					item.price = util.extractNumber(price[0].captures[0].trim());
				}
			} else {
				delete item.price;
			}
		}

		//// 目論見書URLの抽出
		//// 目論見書はpopupで開くため事前に閲覧しておくしかなく自動化困難のためitem.prospectusUrlは実質未使用
		//// monexの場合は複数回extractInfoを実行するためextract済みのitemはスキップ
		// if ( item.prospectusUrl ) {
		if ( item.prospectusUrl && /openWin/gim.test(item.prospectusUrl) ) {
			var url = /(\/pc\/.+?)\'/gim
			.check ( item.prospectusUrl );
			if ( url ) {
				url = url[0].captures[0].replace(/\&amp\;/gim, '&');
				item.prospectusUrl = url;
			} else {
				errs.push('[extractInfo] 目論見書URLが取得できていません');
			}
		}
	}
}

/**
 * ブックビル対象の抽出・ソート
 * 主幹事銘柄が最優先、次にランクで優先順位を設定（主幹事銘柄もランク順）
 * 
 * monexの場合、agentから呼び出す仕様に変更(21.09.08)(他の証券coreの場合は内部処理で実行)
 * 理由)
 * 銘柄情報取得のためにfetchDetailをバッチ処理(agentでPromise.all)しており、
 * fetchDetail毎にextractBookBillTargetを行っていたためにitemsToBookBillに重複が発生していたため
 * バッチ処理終了後にextractBookBillTargetを1回だけ呼び出す仕様に変更
 */
Monex.prototype.extractBookBillTarget = extractBookBillTarget;
function extractBookBillTarget () {	
	var items_mainSec = []; // SMBCが主幹事の銘柄
	var items_not_mainSec = []; // 主幹事ではない銘柄

	// itemsToBookBill = [];

	for ( var i=0; i<items.length; i++) {
		var item = items[i];

		//// 申込可能判定 
		if ( !item.enable ) continue;

		item.rank = scheduler.getRank(item.code);
		item.mainSec = scheduler.getMainSec(item.code);

		//// 申込済み判定
		// if ( alreadyBookBilledCodes.includes(item.code) ) {
		// 	logger.w('既にブックビル済みの銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
		// 	continue;
		// }

		//// RANK判定
		if ( item.rank && util.isTargetIpoRank(item.rank, true/*isForBookBill*/) ) {
			//// 主幹事判定
			if ( /マネックス/.test(item.mainSec) ) {
				items_mainSec.push(item);
			} else {
				items_not_mainSec.push(item);
			}
		} else {
			logger.w('ターゲットRankではない銘柄のためブックビル対象から除外 ----> '+item.name+'('+item.code+') Rank【'+item.rank+'】');
			continue;
		}

	}

	//// ランク順にソート
	sortItems(items_mainSec);
	sortItems(items_not_mainSec);

	var targets = items_mainSec.concat(items_not_mainSec);

	//// 予算判定
	if ( !money ) {
		var msg = '口座残高が取得できていないので予算判定をスキップ';
		logger.e(msg);
		errs.push('[extractBookBillTarget]'+msg);
		itemsToBookBill = targets;
	} else {
		for (var i=0; i<targets.length; i++) {
			var item = targets[i];
			if ( !item.price ) {
				var msg = '仮条件価格が取得できていないのでブックビル対象から除外 ----> '+item.name+' ('+item.code+')';
				logger.e(msg);
				errs.push('[extractBookBillTarget]'+msg)
				continue;
			}
			if ( money >= (item.price*100) ) {
				itemsToBookBill.push(item);
				money = money - item.price*100;
				logger.d('予算残高: '+money);
			} else {
				var msg = '予算オーバーのためブックビル対象から除外 ----> 予算残高: '+money+'円 '+item.name+'('+item.code+') Rank【'+item.rank+'】'+item.price+'円';
				logger.w(msg);
				notes.push(msg);
			}
		}
	}

	logger.w('BookBill対象銘柄: '+itemsToBookBill.length+'銘柄');
	logger.i(itemsToBookBill);

}

/**
 * Rankでソート
 */
function sortItems (__items) {
	for (var i=0; i<__items.length; i++) {
		var item = __items[i];
		//// 評価用に一時的なプロパティ追加		
		switch(item.rank) {
			case 'S':
			item.score = 100;
			break;
			case 'A':
			item.score = 80;
			break;
			case 'B':
			item.score = 60;
			break;
			case 'C':
			item.score = 40;
			break;
			case 'D':
			item.score = 20;
			break;
			default:
			item.score = 0;
		}
	}
	__items.sort( function (a, b) {
	    if ( a.score > b.score ) return -1;
	    if ( a.score < b.score ) return 1;
	    return 0;
	});	
}

/**
 * 抽選結果の抽出
 */
function getResults () {
	var results = [];
	var target = alreadyBookBilled || [];
	for (var i=0; i<target.length; i++) {
		var item = target[i];
		if ( item.result ) {
			results.push(item);
		}
	}
	return results;
}

/**
 銘柄コードを含むテキストからコードを抽出
*/
function getCode (text) {
	if ( !text ) return null;
		//// 銘柄コードの抽出
		var code =
		// /^\s*([0-9][0-9][0-9][0-9])/gim
		/([0-9][0-9][0-9][0-9])/gim
		.check( text );
		if ( code ) code = code[0].captures[0].trim().toOneByte()+'';
		return code;
}

function getName (text) {
	if ( !text ) return null;
	text = text.replace(/\n/gim, '').replace(/\t/gim, '');
	var name =
	/^(.+?)\s*[0-9]+/gim
	.check( text );
	if ( name ) name = name[0].captures[0].trim();
	return name;
}

/**
 * items配列の中のitemを引数のものに差し替え
 */
function updateItem (item) {
	var index = getItemIndex(item);
	if ( index != -1 ) {
		items.splice(index, 1, item);
	} else {
		logger.w('not found item to update ----> '+item.name+' ('+item.code+')');
		items.push(item);
	}
}

function getItemIndex (item) {
	var index = -1;
	for (var i=0; i<items.length; i++) {
		if (item.code === items[i].code) {
			index = i;
			break;
		}
	}
	return index;
}


module.exports = Monex;
