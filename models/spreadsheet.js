'use strict';

/**
 reference document
 https://www.npmjs.com/package/google-spreadsheet

 IPO当選管理シート
 https://docs.google.com/spreadsheets/d/1Qzwioj1sR0k5hHv2EC7ApS8sm7y6WGcfKGlPfM6hz2o/edit#gid=2146677479
 */

// var GoogleSpreadsheet = require('google-spreadsheet'),
var {GoogleSpreadsheet} = require('google-spreadsheet'),
    credentials       = require('../cred/IPOJP-2158dc0a8c75.json'),
    util              = require('../common/util'),
    param             = require('../common/param'),
    logger            = require('./logger')('spreadsheet');


var ipoSheet;

function SpreadSheet (sheetId) {
	makeInstance(sheetId);
	this.promise = Promise.resolve();
}

function makeInstance (sheetId) {
	if (ipoSheet) return; // singleton

	ipoSheet = new GoogleSpreadsheet(sheetId);
}

SpreadSheet.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);
	return this;
	/*
	 catchは呼び出し側で実装
	 cal.catch(err=>{somethingToDo});
	*/	
}

SpreadSheet.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

/**
 認証
 */
SpreadSheet.prototype.auth = function () {
	return this.then(() => {
		return new Promise((resolve, reject) => {

			// ipoSheet.useServiceAccountAuth(credentials, function(err) {
			// 	if (err) {
			// 		reject(err);
			// 		return;
			// 	}
			// 	resolve();
			// });

			(async function() {
				await ipoSheet.useServiceAccountAuth({
					client_email: credentials.client_email,
					private_key : credentials.private_key,
				});
				resolve();
			}());

		});
	});
}

/**
 SpreadSheetからIpo情報を取得
 引数: 取得するIpo情報の数
 */
SpreadSheet.prototype.fetchIpo = function (maxNum /*Number*/) {
	return this.then(() => {
		return new Promise((resolve, reject) => {

			var max = Number(maxNum);
			if ( !util.isValidNums(max) ) {
				reject('不正な入力値(数字ではありません)----> '+maxNum);
				return;
			}

			(async function() {

			  	await ipoSheet.loadInfo(); // loads document properties and worksheets
			  	const worksheet = ipoSheet.sheetsByTitle['銘柄管理'];

  				// logger.w(ipoSheet.title);
  				// logger.w(worksheet.title);

				//// 1行目はヘッダとして扱われ、
				//// 2行目以降がrowsになるが2行目はIPO管理シートでのヘッダ（項目名）なのでoffset:1で3行目から取得
				//// ----> 1行目をkeyとしてしかrowデータを取り出せないのでコメントアウト
				// const rows = await worksheet.getRows({limit:max, offset:1});
				// const rows = await worksheet.getRows();
				//
				// rows.forEach(row => {
				// 	logger.w(row['Excelへ保存する方法']);
				// });

			　
				var row_count  = worksheet.rowCount;
				// var row_offset = 2; 
				// var row_length = max+row_offset > row_count-row_offset? row_count-row_offset : max+row_offset;

			 	var row_start = 3; // 3行目から取得(2行目まではデータではない)
				var row_end   = max+row_start > row_count? row_count: max+row_start;

				//// この方法ではcellをロードできない模様
				// await worksheet.loadCells({
				// 	startRowIndex: row_start-1,
				//	endRowIndex  : row_end-1,
				//});

				var range = 'A1:'+'M'+row_end;
				// logger.w(range);

				await worksheet.loadCells(range);

				var items = [];
				/*
				  sheet.loadCellsは指定した範囲を読み込むが
				  sheet.getCellで指定範囲外を呼び出すとエラーとなる（絶対アドレスは変わらない）
				*/
				for (let i=row_start-1; i<row_end-1; i++ ) {
					// logger.w(worksheet.getCell(i, 2).value);
                    var item = {
	                    name : worksheet.getCell(i, 2).value, // access cells using a zero-based index
	                    url : worksheet.getCell(i, 2).hyperlink, // 銘柄説明記事のURL取得
		                code : worksheet.getCell(i, 3).formattedValue, // 数字として認識されるため文字列として読み出す
		                rank : worksheet.getCell(i, 4).value,
		                // TODO: unixTime変換
		                bookBillDate : {
		                    start : worksheet.getCell(i, 6).formattedValue,
		                    end   : worksheet.getCell(i, 8).formattedValue,
		                },
		                purchaseDate : {
		                　  start : worksheet.getCell(i, 10).formattedValue,
		                    end   : worksheet.getCell(i, 12).formattedValue,
		                },
		        	};

		        	items.push(item);

				}

				logger.i(items);

				resolve(items);


			}());


			/* *Deplicated

			ipoSheet.getInfo((err, info) => {
				if (err) {
					reject(err);
					return;
				}
			
				var items = [];
			
				for (var i=0; i<info.worksheets.length; i++) {
					var worksheet = info.worksheets[i];
			
					if (worksheet.title === '銘柄管理') {
			
						worksheet.getRows((err, rows) => {
							if (err) {
								reject(err);
								return;
							}
			
							max = max > rows.length? rows.length : max;
			
							for (var i=1; i<max; i++) {
								var row = rows[i];
		                        // spreadsheetのrowオブジェクトのkey(列id)は1行目のヘッダが使用される
		                        // ヘッダに空欄のセルがある場合,自動生成されたkeyとなる
		 	                       var item = {
		 	                           name : row["excelへ保存する方法"],
		 	                           code : row["別のスプレッドシートへ保存する方法"],
		 	                           rank : row._chk2m,
		 	                           // TODO: unixTime変換
		 	                           bookBillDate : {
		 	                               start : row._ckd7g,
		 	                               end   : row._cyevm,
		 	                           },
		 	                           purchaseDate : {
		 	                               start : row._d180g,
		 	                               end   : row._cssly,
		 	                           },
		 	                       };

		 	                       items.push(item);

							}

							resolve(items);

						});
					}
				}
			});
			*/
		});
	});
}

module.exports = SpreadSheet;
