'use strict';

/**
 scheduler
 */

var Calendar    = require('./calendar'),
	SpreadSheet = require('./spreadsheet'), 
	IpoSite     = require('./iposite'),
	logger      = require('./logger')('scheduler'),
	mongo       = require('./mongo')(),
	util        = require('../common/util'),
	param       = require('../common/param');

var cal,
	sheet,
	ipoSite,
	calEvents,
	ipoItems, // 新着アイテム
	/**
	 * mongoDBから読み出したitems
	 * 外部からランク判定を行う場合に使用
	 * mongoにはparamで指定したRankの銘柄のみ保存されている
	 */
	dbItems;


exports.init = function () {
	if ( !cal )   cal   = new Calendar();
	if ( !sheet ) sheet = new SpreadSheet(param.IPO_SHEET_ID);
	if ( !ipoSite ) ipoSite = new IpoSite();

	loadDbItems();

	//// 動作チェック
	// iposite = new IpoSite();
	// iposite
	// .open('https://www.ipokiso.com/company/2021/lakeel.html')
	// .getMainSec()
	// .then(mainSec => {
	// 	logger.w(mainSec);
	// })
	// .catch(err => {
	// 	logger.e(err);
	// });
	// return;
}

exports.start = function () {
	fetchCalEvents();

	logger.w(util.getCalendarPeriod(param.CAL_FETCH_DAYS));
}

//// ランク情報を返す
exports.getRank = function (code) {
	if ( !code ) return null;
	for (var i=0; i<dbItems.length; i++) {
		var item = dbItems[i];
		if ( item.code == code ) {
			return item.rank;
		}
	}
	return null;
}

//// 主幹事情報を返す
exports.getMainSec = function (code) {
	if ( !code ) return null;
	for (var i=0; i<dbItems.length; i++) {
		var item = dbItems[i];
		if ( item.code == code ) {
			return item.mainSec;
		}
	}
	return null;
}

//// 銘柄名を返す
exports.getName = function (code) {
	if ( !code ) return null;
	for (var i=0; i<dbItems.length; i++) {
		var item = dbItems[i];
		if ( item.code == code ) {
			return item.name;
		}
	}
	return null;
}


function fetchCalEvents ()  {
	cal
	// .fetchEvents(['2017-05-20T06:00:00+08:00', '2018-12-25T22:00:00+08:00'])
	.fetchEvents(util.getCalendarPeriod(param.CAL_FETCH_DAYS))
	.then(events => {
		calEvents = events;
		logger.i(events);

		scrapeIpo();

	})
	.catch(err => {
		logger.e(err);
	});
}

function scrapeIpo () {
	sheet
	.auth()
	.fetchIpo(param.IPO_FETCH_NUM)
	.then(items => {

		/**
		 IPOアイテムのフォーマット

		 [{ name: '銘柄名',
		 	url: 'url',
    		code: '2971',
		    rank: 'S',
		    bookBillDate: { start: '01/28', end: '02/01' },
		    purchaseDate: { start: '02/05', end: '02/08' } }],
		    mainSec: '主幹事証券会社',
		    date: unixTime // item登録日（mongoで使用）
		  }]
		*/

		ipoItems = items;
		logger.i(items);

		var ipoItemsNew = [];

		for (var i=0; i<ipoItems.length; i++) {
			var ipoItem = ipoItems[i];

			/**
			 *  migration (21.07.13)
			 *  mongoに過去アイテムのデータベース作成
			 */
			// var isExist = false;
			// for (var d=0; d<dbItems.length; d++) {
			// 	var dbItem = dbItems[d];
			//  	if ( dbItem.code == ipoItem.code ) {
			//  		isExist = true;
			// 	}
			// }
			// if ( !isExist ) {
			// 	ipoItem.date = util.getUnixTimeFromDateObject(new Date());	
			// 	ipoItemsNew.push(ipoItem);
			// }
			// /*
			//  if ( util.isTargetIpoRank(ipoItem.rank) ) {
			//  	ipoItem.date = util.getUnixTimeFromDateObject(new Date());	
			//  	ipoItemsNew.push(ipoItem);
			// }
			// */

	
			//// 既にCalendarに登録されておらず指定ランクのIPOのみ抽出
			//// ----> mongoDBにデータベース化するためにすべてのランクを処理に変更
			//// if ( !util.isAlreadyIpoEvent(calEvents, ipoItem.code) && util.isTargetIpoRank(ipoItem.rank) ) {

			//// カレンダ表示対象Rankにも関わらずカレンダに登録されていない、またはmongoDBに登録されていないIPOのみ抽出
			if ( !util.isAlreadyIpoEvent(calEvents, ipoItem.code, null/*isForDbItems*/) && util.isTargetIpoRank(ipoItem.rank, null/*isForBookBill*/) ||
				 !util.isAlreadyIpoEvent(dbItems, ipoItem.code, true/*isForDbItems*/)) {
				ipoItem.date = util.getUnixTimeFromDateObject(new Date());
				ipoItemsNew.push(ipoItem);
			}

		}
		ipoItems = ipoItemsNew;

		// addToCal();
		scrapeMainSec();

	});
}

//// "IPO株のはじめかた"サイトから主幹事情報を取得
function scrapeMainSec () {

	if ( ipoItems.length == 0 ) {
		logger.w('新規IPOがないので主幹事取得をスキップ');
		addToCal(); //// 不要では?
		return;
	}

	logger.w('主幹事情報を取得中....');
	var ques = [];
	for (var i=0; i<ipoItems.length; i++) {
		var ipoItem = ipoItems[i];
		ques.push(
			ipoSite
			.open(ipoItem)
			.getMainSec()
			.then(mainSec => {
				// ipoSite内でipoItemを更新している
			})
			.catch(err => {
				logger.e(err);
			})
		);
	}

	Promise.all(ques)
	.then( results => {
		logger.i('主幹事情報の取得が完了しました');
		logger.w(ipoItems);

		addToCal();

		saveItems();

	})
	.catch( err => {
		logger.e(err);
	});
}

function addToCal () {

	if ( ipoItems.length == 0 ) {
		logger.w('Calendarに登録する新規IPOはありません');
		return;
	}

	var ques = [];
	for (var i=0; i<ipoItems.length; i++) {
		var ipoItem = ipoItems[i];

		if ( util.isTargetIpoRank(ipoItem.rank) && !util.isAlreadyIpoEvent(calEvents, ipoItem.code, null/*isForDbItems*/)) {
			logger.d(ipoItem.name+'('+ipoItem.code+')をCalendarに追加中....');
			ques.push(
				cal
				.addEvent(ipoItem)
				.catch(err => {
					logger.e(err);
				})
			);
		}
	}

	Promise.all(ques)
	.then( results => {
		// logger.i(ipoItems.length+'件のIPOをCalendarに追加しました');
		logger.i(ques.length+'件のIPOをCalendarに追加しました');
	})
	.catch(err => {
		logger.e(err);
	});
}

function saveItems () {
	var ques = [];
	for (var i=0; i<ipoItems.length; i++) {
		var item = ipoItems[i];
		ques.push(
			mongo
			.saveItem(item)
			.then(code => {
			})
		);
	}

	Promise.all(ques)
	.then( results => {
		logger.i(ipoItems.length+'件のIPOをMongoDBに追加しました');
		loadDbItems(); // dbItems更新
	})
	.catch(err => {
		logger.e(err);
	});
}

function loadDbItems () {
	var days = param.DB_LOAD_DAYS;
	var fromDate = new Date();
	fromDate.setDate( fromDate.getDate() - days );

	// logger.w(util.getDateString(fromDate));

	mongo
	.loadItems(util.getUnixTimeFromDateObject(fromDate))
	.then(items => {
		dbItems = items;
		logger.w(dbItems);
	})
	.catch(err => {
		logger.e(err);
	});
}
