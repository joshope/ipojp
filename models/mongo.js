'use strict';

var mongoose = require('mongoose'),
	mongoBackup  = require('mongodb-backup'),
	mongoRestore = require('mongodb-restore'),
	param    = require('../common/param'),
	util     = require('../common/util'),
	logger   = require('./logger')('mongo'),
	agent    = require('./agent'),
	fs 	 = require('fs'),
	fse  = require('fs-extra'),
	path = require('path');

var url = 'mongodb://localhost/' + param.DB_NAME

//// mongooseバージョンの関係でWarningが出るためオプションを追加
// var db = mongoose.createConnection(url);
var db = mongoose.createConnection(url, {useNewUrlParser: true, useUnifiedTopology: true});


/**
 * Don't use mongoose Promise
 */
 mongoose.Promise = global.Promise;

 db.once('open', () => {
 	logger.i('DB connected: '+ url);
 });

 db.on('error', err => {
 	logger.e('DB connection error');
 	throw new Error(err);
 });


/**
 * make instance when required
 * generate each Mongo instance to separate Promise avoiding collesion in each requirer module
 */

function makeInstance () {
	return new Mongo();
}

function Mongo () {
	this.promise = Promise.resolve();
}

Mongo.prototype.then = function (onFulfilled, onRejected) {
	this.promise = this.promise.then(onFulfilled, onRejected);
	return this;
}

Mongo.prototype.catch = function (func) {
	this.promise = this.promise.catch(err=>{func(err);});
	return this;
}

Mongo.prototype.isExist = function (code) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			Item.count({code: code}, (err, count) => {
				if (err) {
					reject(err);
				} else {
					resolve(count>0);
				}
			});
		});
	});
}

Mongo.prototype.loadItem = function (code) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			Item.findOne({code: code}, (err, item) => {
				if (err) {
					reject(err);
				} else {
					resolve(item);
				}
			});
		});
	});
}

Mongo.prototype.loadItems = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			Item.find().exec((err, res) => {
				if (err) {
					reject(err);
				} else {
					if ( !res || res.length==0 ) logger.w('not found items');
					resolve(res);
				}
			});
		});
	});
}

Mongo.prototype.loadItems = function ( __from/*unixTime*/) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			var from =  __from? __from: 0;
			/**
			 * sort;
			 *   1: unixTimeの小さい順（古いもの順）,
			 *  -1: 新しいもの順
			 */
			Item.find({date: {$gte:from}}).sort({date:-1}).exec((err, res) => {
				if (err) {
					reject(err);
				} else {
					if ( !res || res.length==0 ) logger.w('not found items');
					resolve(res);
				}
			});
		});
	});
}

Mongo.prototype.saveItem = function (item) {
	return this
	.removeItem(item)
	.then(() => {
		return new Promise( (resolve, reject) => {
			new Item({

				code:  item.code,
				name:  item.name,
				url:   item.url,
				rank:  item.rank,
				bookBillDate: item.bookBillDate,
				purchaseDate: item.purchaseDate,
				mainSec: item.mainSec,
				date: item.date, // item登録日（unixTime）
	 
			}).save( err => {
				if (err) {
					reject(err);
				} else {
					logger.i('saved Item: '+item.code+'('+item.name+')');
					resolve(item.code);
				}
			});
		});
	});
}

Mongo.prototype.removeItem = function (item) {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			Item.remove({code: item.code}, err => {
				if (err) {
					reject(err);
				} else {
					logger.w('deleted item: '+item.code+'('+item.name+')');
					resolve();
				}
			});
		});
	});
}

//// 不正データがまれに残ることがあるため削除
//// code=nullまたはname=nullの場合(empty stringの場合も含む)
Mongo.prototype.cleanupItems = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			// AND検索の場合
			// Item.remove({code:null, name:null}, err => {
			//
			// OR検索
			Item.remove( {$or:[{code:null}, {code:''}, {name:null}, {name:''}]}, err => {
				if (err) {
					reject(err);
				} else {
					logger.w('cleanup item done!');
					resolve();
				}
			});
		});
	});
}



/***********************
 DB backup & restore
 ***********************/

Mongo.prototype.countBackupItems = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			var dirpath = agent.getBackupPath() + '/' + param.DB_NAME + '/' + param.DB_COLLECTION_NAME;
			fs.readdir(dirpath, (err, files) => {
				if (err) {
					logger.e(err);
					resolve(0);
				}
				var fileList = files.filter(file => {
					return fs.statSync(path.join(dirpath,file)).isFile() && /.*\.bson$/.test(file); // 絞り込み
				});
				resolve(fileList.length || 0);
			});
		});
	});
}

Mongo.prototype.backupDb = function () {
	return this.then(() => {
		return new Promise( (resolve,reject) => {
			var dirPath = agent.getBackupPath() + '/' + param.DB_NAME;
			fse.remove(dirPath, err => {
				if (err) {
					reject(err);
				} else {
					mongoBackup({
						uri: 'mongodb://localhost:27017/'+param.DB_NAME, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
						// root: param.DB_BACKUP_DIR,
						root: agent.getBackupPath(),
						callback: function () {
							resolve();
						}
					});
				}
			});
			// 2重でバックアップをとっておく
			var subDirName = util.getCurrentDateString('-').replace(/\s/g,'-').replace(/:/g,'-');
			// var secondBackupDir = param.DB_BACKUP_2ND_DIR+'/'+subDirName;
			var localBackupPath = param.DB_BACKUP_DIR_LOCAL+'/'+subDirName;
			//// ディレクトリがなければ作成
			fse.ensureDir(localBackupPath)
			.then(() => {
				mongoBackup({
					uri: 'mongodb://localhost:27017/'+param.DB_NAME, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
					root: localBackupPath,
					callback: function () {
						logger.w('デスクトップにもバックアップを作成しました');
					}
				});
			})
			.catch(err => {
				logger.e(err);
			});
		});
	});
};

Mongo.prototype.restoreDb = function () {
	return this.then(() => {
		return new Promise( (resolve, reject) => {
			mongoRestore({
				uri: 'mongodb://localhost:27017/'+param.DB_NAME, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
				// root: param.DB_BACKUP_DIR+'/'+param.DB_NAME,
				root: agent.getBackupPath() + '/' + param.DB_NAME,
				callback: function () {
					resolve();
				},
				drop: true,
			});
		});
	});
}




/*** Schema ***************************/

var Item = db.model('Item',
	mongoose.Schema({
		/*getItems()*/
		code:  String,
		name:  String,
		url:   String,
		rank:  String,
		bookBillDate: 
		{type:
			{}, default: null
		},
		purchaseDate:
		{type:
			{}, default: null
		},
		mainSec: String,
		date: Number,
	},
	{collection: param.DB_COLLECTION_NAME})
);


module.exports = makeInstance;