'use strict';

const Nomura = require('./include/nomura-core'),
	  logger = require('./logger')('nomura'),
	  param  = require('../common/param'),
	  util   = require('../common/util');

let nomura,
	userId;
let cb;
let isSuccess; // スクレイプ成功フラグ

const SEC_NAME = 'nomura';

exports.init = async (__userId, isDisp/*option:nm表示*/, isDebug/*option:nmデバッグモード*/, __cb/*option*/) => {
	userId = __userId;
	if ( nomura ) { // singleton (coreのpuppeteerを破棄して再生成)
		await nomura.close();
		logger.w('---- close puppeteer and recreate nomura obect');
		nomura = new Nomura(userId, isDisp, isDebug);
		// await nomura.init();
		if (__cb) __cb();
	} else {
		nomura = new Nomura(userId, isDisp, isDebug);
		// await nomura.init();
		if (__cb) __cb();
	}
}

exports.start = async (__cb) => {
	cb = __cb;
	if ( nomura.isValid ) {
		try {
			await nomura.open();
			await nomura.login();
			await nomura.loginAdditional();
			await nomura.gotoIpo();
			const items = await nomura.searchIpo();
			logger.d(items);
			await scrapeIpo();
		} catch (err) {
			logger.e(err);
			if (cb) cb({secName:SEC_NAME, isSuccess:false});
		}
	} else {
		logger.w('証券口座がないためスキップします ----> '+util.getUserName(userId, true));
		if (cb) cb({secName:SEC_NAME, isSuccess:true});
	}
	await nomura.close();
}


async function scrapeIpo() {

	var items = nomura.getItemsToBookBill(); // ブックビルできる銘柄がない場合は空配列[]が返ってくる

	isSuccess = true; // 初期化
	if ( !util.isExist(items) && util.isExist(nomura.getErrors()) ) {
		logger.w('[野村證券] スクレイプに失敗している可能性があります ----> '+util.getUserName(userId, true));
		isSuccess = false;
	}

	for (let item of items) {
		logger.d('[野村證券] ブックビルできます----> '+item.name);

		await nomura.gotoIpo();
		let code = await nomura.clickBookBill();

		if ( !code ) {
			logger.e('[野村證券] ブックビルの抽選申し込みボタンを押せていない----> '+item.name);
			continue;
		}

		logger.w('[野村證券] ブックビル申込します ----> '+item.name+'('+code+')');

		let status;
		try {
			await nomura.checkTerms();
			await nomura.checkProspectus();
			await nomura.confirmApplication();
			await nomura.executeApplication();
			status = true;
		} catch (err) {
			logger.e(err);
			status = false;
		}
 		isSuccess = isSuccess & status;
	}
	if ( !items.length ) {
		logger.i('[野村證券] ブックビルできるアイテムはありません');
	} else {
		logger.i('[野村證券] scrapingが完了しました');
	}

	if (cb) cb({secName:SEC_NAME, isSuccess:isSuccess});

}

exports.getSecName = function () {
	return SEC_NAME;
}

exports.getBookBilled = function () {
	return nomura.getBookBilled();
}

exports.getResults = function () {
	return nomura.getResults();
}

exports.getErrors = function () {
	return nomura.getErrors();
}

exports.getNotes = function () {
	return nomura.getNotes();
}

exports.isValid = function () {
	return nomura.isValid;
}

exports.isDisp = function () {
	return nomura.isDisp;
}

exports.isSuccess = function () {
	return isSuccess;
}
