'use strict';

/*****
 サーバーとクライアントでスクリプトを共用
 https://stackoverflow.com/questions/3225251/how-can-i-share-code-between-node-js-and-the-browser
 http://qiita.com/VoQn/items/325a17063c4f6c61761c

 NOTICE:
   var a=b=0は無名関数実行内 (function(){})() ではエラーとなるので注意
   var a=0, b=0; // これはエラーではない
   参考) https://stackoverflow.com/questions/27329444/why-a-is-undefined-while-b-is-3-in-var-a-b-3
 *****/

/**
 * nodeParamはparam.jsでインスタンス名を指定してclient側のlayout.pugで読み込んでいるグローバル変数
 */
var param = (typeof require === 'undefined')? nodeParam : require('./param');
var users = (typeof require === 'undefined')? {} : require('../cred/user');

(function(exports){

	/**
	 銘柄コードからアイテムを取得
	 */
	exports.findItem = function (code, items) {
		if ( this.isExist(items) ) {
			for (var i=0; i<items.length; i++) {
				var item = items[i];
				if (item.code == code) return item;
			}
		}
		return null;
	}

	/**
	 ユーザID(インスタンスID)からユーザ名を取得
	 config/user.jsを参照するためserver-sideでのみ使用可能
	 */
	// exports.getUserName = function (userId, secName, isAka/*ユーザ名を通称で返す場合*/) {
	// 	if ( this.isExist(users) && this.isExist(userId) && this.isExist(secName) ) {
	// 		try {
	// 			if ( isAka ) {
	// 				return users.sec[secName].users[userId].aka;
	// 			} else {
	// 				return users.sec[secName].users[userId].username;
	// 			}

	// 		} catch (e) {
	// 			console.log(e);
	// 			return null;
	// 		}

	// 	} else {
	// 		return null;
	// 	}
	// }

	/**
	 * ユーザID(インスタンスID)からユーザ名を取得
	 * config/user.jsを参照するためserver-sideでのみ使用可能
	 */
	exports.getUserName = function (userId, isAka/*ユーザ名を通称で返す場合*/, secName/*option*/) {
		var name;
		if ( this.isExist(userId) && this.isExist(users) ) {
			//// secNameが指定されている場合
			if ( secName ) {
				try {
					if ( isAka ) {
						name = users.sec[secName].users[userId].aka;
					} else {
						name = users.sec[secName].users[userId].username;
					}
				} catch (e) {
					console.log(e);
				}
			} else {
				Object.keys(users.sec).some(key => { // some:trueが返されるまで繰り返す
					var sec = users.sec[key];
					try {
						if ( isAka ) {
							name = sec.users[userId].aka;
						} else {
							name = sec.users[userId].username;
						}
					} catch (e) {
						console.log(e);
					}
					if ( name ) return true; // break
				});
			}
		}
		return name;
	}

	/**
	 * 証券会社名からTOP URLを取得
	 * config/user.jsを参照するためserver-sideでのみ使用可能
	 */
	exports.getTopUrl = function (secName) {
		var sec = users.sec[secName];
		var url;
		try {
			url = sec.url;
		} catch (e) {
			console.log(e);
		}
		return url;
	}

	/**
	 * Sleep期間を考慮してintervalを算出
	 */
	exports.getIntervalMin = function () {
		//// デフォルト（通常時）
		var interval = param.CHECK_INTERVAL_MIN;

		var curHour = new Date().getHours();
		var sleepPeriod = param.CHECK_SLEEP;

		if (sleepPeriod.length != 2) {
			throw new Error('sleep期間は配列2要素で指定が必要');
		}
		var sleepStart = sleepPeriod[0],
			sleepEnd   = sleepPeriod[1];
		//// 日付をまたいでいる場合
		if (sleepStart > sleepEnd) {
			if (curHour >= sleepStart ) {
				interval = ((23-curHour)+sleepEnd)*60;
			} else if (curHour < sleepEnd) {
				interval = (sleepEnd-curHour)*60;
			}
		//// 日付をまたいでいない場合
		} else {
			if (curHour >= sleepStart && curHour < sleepEnd) {
				interval = (sleepEnd-curHour)*60;
			}
		}
		return interval;
	}

	/**
	 * 第1引数のunixTimeが指定時間帯の中にあるかどうかを判定
	 * 第1引数を省略した場合は現在時間で判定
	 */
	exports.isInTime = function (targetUnixTime/*optional*/, openArray/*['9:00','9,30']*/, closeArray/*['11:30','15,00']*/) {

		if (typeof openArray != 'object' || typeof closeArray != 'object' || openArray.length != closeArray.length) {
			console.log('Warning ----> gf.config.open/closeは同じ長さの配列で設定が必要');
			return false;
		}

		var date = targetUnixTime? new Date(targetUnixTime*1000) : new Date();
		var nowH = date.getHours(),
			nowM = date.getMinutes();

		var isOpen = false;

		for (var i=0; i<openArray.length; i++) {
			var open   = openArray[i].split(':'),
				close  = closeArray[i].split(':');
			var openH  = Number(open[0]),
				openM  = Number(open[1]),
				closeH = Number(close[0]),
				closeM = Number(close[1]);

			var isInHour = nowH >= openH && nowH <= closeH;
			var isInMin = false;

			if (isInHour) {
				if ( nowH == openH && nowH == closeH ) {
					isInMin = nowM >= openM && nowM <= closeM;
				} else if ( nowH == openH ) {
					isInMin = nowM >= openM;
				} else if ( nowH == closeH ) {
					isInMin = nowM <= closeM;
				} else {
					isInMin = true;
				}
			}
			isOpen = isOpen || isInHour && isInMin;
		}
		return isOpen;
	}

	exports.getDateString = function (dateObj, __separator/*optional*/, isDateOnly/*optional*/) {
		dateObj = dateObj? dateObj: new Date();
		var y = dateObj.getFullYear(), mth = dateObj.getMonth()+1, d = dateObj.getDate(),
		    h = dateObj.getHours(), m = dateObj.getMinutes(), s = dateObj.getSeconds();

		if(mth<10){ mth = '0'+mth }; if(d<10){ d = '0'+d };
		if(h<10){ h = '0'+h };	if(m<10){ m = '0'+m };	if(s<10){ s = '0'+s };
		//return h+'時'+m+'分'+s+'秒';
		var separator = __separator || '/';
		// return y+'/'+mth+'/'+d+' '+h+':'+m;
		if (isDateOnly) {
			return y+separator+mth+separator+d;
		} else {
			return y+separator+mth+separator+d+' '+h+':'+m;
		}
	}

	/*+
	 現在時刻のテキストを返す
	 */
	exports.getCurrentDateString = function (__separator/*optional*/, isDateOnly/*optional*/) {
		var date = new Date();
		return this.getDateString(date, __separator, isDateOnly);
	}

	// exports.getCurrentDateString = function () {
	// 	var date = new Date();
	// 	var y= date.getFullYear(), mth = date.getMonth()+1, d = date.getDate(),
	// 	    h = date.getHours(), m = date.getMinutes(), s = date.getSeconds();

	// 	if(mth<10){ mth = '0'+mth }; if(d<10){ d = '0'+d };
	// 	if(h<10){ h = '0'+h };	if(m<10){ m = '0'+m };	if(s<10){ s = '0'+s };
	// 	//return h+'時'+m+'分'+s+'秒';
	// 	return y+'/'+mth+'/'+d+' '+h+':'+m;
	// }

	/*+
	 UnixTimeから時刻のテキストを返す
	 */
	// exports.getDateString = function (unixTime) {
	// 	return new Date(unixTime*1000)  // UNIX to Date
	// 		.toLocaleDateString("ja-JP",{
	// 			year: 'numeric', weekday: 'short',
	// 			month:'2-digit', day:'2-digit', hour:'2-digit', minute:'2-digit'});
	// }

	/**
	 テキストから月日時分を取得してDateオブジェクトを返す
	 テキストフォーマット M/D
	*/
	exports.getDateObjectFromText = function (string) {
		if ( !string ) return null;

		var date = 
		/([0-9０-９]+)?\/([0-9０-９]+)/gim
		.check( string );
		if ( !date ) return null;

		var month = Number(date[0].captures[0].trim().toOneByte());
		var day   = Number(date[0].captures[1].trim().toOneByte());

		var dateObj = new Date();

		var year = dateObj.getFullYear();

		//// 年をまたぐ場合を判定
		//// 年をまたぐ場合は当月が年末だけのはず
		var currentMonth = dateObj.getMonth()+1;
		if ( (currentMonth == 11 || currentMonth ==12) && (month == 1 || month == 2) ) {
			year = year+1;
		}

		// if ( month < currentMonth ) {
		// 	year = year+1;
		// }

		dateObj.setDate(1); // 月末で実行するとsetMonthがズレるのを防ぐために一旦設定
		dateObj.setMonth(month-1);
		dateObj.setDate(day);
		dateObj.setHours(0);
		dateObj.setMinutes(0);
		dateObj.setSeconds(0);
		dateObj.setFullYear(year);

		return dateObj;
	}

	/**
	 DateオブジェクトをUnixTimeに変換して返す
	*/
	exports.getUnixTimeFromDateObject = function (dateObj) {
		if ( !dateObj ) return null;
		return Math.floor(dateObj.getTime()/1000);
	}

	/**
	 テキストから月日を取得してUnixTimeを返す
	 テキストフォーマット M/D
	*/
	exports.getUnixTimeFromText = function (string) {
		if ( !string ) return null;

		var dateObj = this.getDateObjectFromText(string);
		if ( !dateObj ) return null;

		return this.getUnixTimeFromDateObject(dateObj);
	}

	/**
	 UnixTimeから日付テキストに変換して返す
	 テキストフォーマット YYYY/M/D
	*/
	exports.getDateStringFromUnixTime = function (unixTime) {
		if ( !unixTime || typeof unixTime !== 'number') return null;

		var dateObj = new Date(unixTime*1000);
		return date.getFullYear()+'/'+date.getMonth()+1+'/'+date.getDate();

	}

	/*
	 mongoでオブジェクト要素に対してnullを保存すると空オブジェクトとして保存されるためnullまたは空オブジェクトを判定
	 この問題はmongoのschemaでデフォルト設定をすることで回避できる
	*/
	exports.isExist = function (obj) {
		
		if (typeof obj === 'object') {
			return obj != null && Object.keys(obj).length != 0;
		} else {
			return obj != null && Number.isNaN(obj) == false;
		}

		/*
		// true patterns
		logger.w(util.isExist({hoge:null}));
		logger.w(util.isExist([1]));
		logger.w(util.isExist(0);
		logger.w(util.isExist(100));
		logger.w(util.isExist(''));
		logger.w(util.isExist('hoge'));	
		logger.w(util.isExist(true));
		logger.w(util.isExist(false));		

		// false patterns
		logger.w(util.isExist({}));
		logger.w(util.isExist([]));
		logger.w(util.isExist(NaN));
		logger.w(util.isExist(null));
		logger.w(util.isExist(undefined));
		*/

	}



	/*******************************************
	 Scheduler util
	 *******************************************/
	/**
	 Calendar EventがIPOイベントか判定
	 */
	exports.isIpoEvent = function (calItem) {
		var key  = calItem.description;
		return key != null && key.indexOf(param.CAL_DESC_TAG) != -1;
	}

	/**
	 銘柄コードのGoogle Calendarイベントが存在するか判定
	 CalendarではなくMongoDBデータの存在を判定する場合はisForDbItemsをセット
	 */
	exports.isAlreadyIpoEvent = function (calEvents/*[CalendarItem]*/, code/*String*/, isForDbItems/*option*/) {
		if ( typeof calEvents !== 'object' || calEvents == null) {
			console.log('<util.isAlreadyIpoEvent> 不正な引数 (calEvents) ----> '+calEvents);
			return false;
		}

		if ( code == null || code == '' ) {
			console.log('<util.isAlreadyIpoEvent> 不正な引数 (code) ----> '+code);
			return false;
		}

		// for (var i=0; i<calEvents.length; i++) {
		// 	var item = calEvents[i];
		// 	var summary = item.summary;
		// 	if ( summary != null && summary.indexOf(code) !== -1) {
		// 		return true;
		// 	}
		// }
		// return false;

		for (var i=0; i<calEvents.length; i++) {
			var item = calEvents[i];
			var summary = isForDbItems? item.code: item.summary;
			if ( summary != null && summary.indexOf(code) !== -1) {
				return true;
			}
		}
		return false;

	}

	/**
	 IPOが指定ランクか判定
	 */
	exports.isTargetIpoRank = function (rank/*string*/, isForBookBill/*option*/) {
		if ( !rank || typeof rank !== 'string' ) {
			console.log('<util.isTargetIpoRank> 不正な引数 ----> '+rank);
			return false;
		}

		// return param.IPO_RANKS.indexOf(rank) != -1;

		//// ブックビルのターゲットランクの場合
		if ( isForBookBill ) {
			return param.IPO_RANKS_BOOKBILL.includes(rank);
		//// カレンダー追加用のターゲットランクの場合
		} else {
			return param.IPO_RANKS_CAL.includes(rank);
		}
	}

	/**
	 本日を起点に指定期間前後のGoogle Calendar検索期間を返す
	 戻り値: ['yyyy-mm-ddThh:mm:ss+09:00', 'yyyy-mm-ddThh:mm:ss+09:00']
	 */
	exports.getCalendarPeriod = function (days/*num*/) {

		if ( !this.isValidNums([days]) ) {
			console.log('不正な引数(daysが数字ではない) ----> '+days);
		}

		var date  = new Date();
		date.setDate(date.getDate()-days);
		var start = this.getCalendarDateFormat(date.getMonth()+1+'/'+date.getDate(), date.getFullYear());

		date.setDate(date.getDate()+days*2);
		var end   = this.getCalendarDateFormat(date.getMonth()+1+'/'+date.getDate(), date.getFullYear());

		return [start, end];
	}



	/**
	 mm/dd書式の文字列をyyyy-mm-ddThh:mm:ss+09:00書式に変換
	 第2引数: 年の明示
	 */
	exports.getCalendarDateFormat = function(string/*mm/dd*/, year/*num*/) {

		if ( !string || typeof string !== 'string' ) {
			console.log('<util.getCalendarDateFormat>不正な引数(1st arg)----> '+string);
			return null;
		}

		var target = string.split('/');
		if ( target.length 	!= 2 ) {
			console.log('<util.getCalendarDateFormat>mm/dd書式ではない----> '+string);
			return null;
		}

		if ( year && typeof year !== 'number' ) {
			console.log('<util.getCalendarDateFormat>不正な引数(year)----> '+year)
		}

		var targetMonth  = Number(target[0]);
		var targetDate   = Number(target[1]);

		if ( !this.isValidNums([targetMonth, targetDate]) ) {
			console.log('mm/dd書式ではない(mm,ddが数字として認識できない)----> '+string);
			return null;
		}

		var date  = new Date();
		var currentYear  = date.getFullYear();
		var currentMonth = date.getMonth() + 1;

		/*
		 年が明示されていない場合
		 年をまたぐ日付の場合はYearを加算
		 Calendarに登録されておらず直近のIPOで当月よりも小さな月あれば年をまたいでいるはず
		 */
		if ( !year ) {
			//// Calendar登録に失敗していてTarget月<当月になってしまった場合も想定(20.07.25)
			//// → 年をまたぐ場合は当月が年末だけのはず
			// year = (targetMonth < currentMonth)? currentYear+1 : currentYear;

			if ( (currentMonth == 11 || currentMonth ==12) && (targetMonth == 1 || targetMonth == 2) ) {
				year = currentYear+1;
			} else {
				year = currentYear;
			}
		}

		return year+'-'+targetMonth+'-'+targetDate+'T'+'16:00:00+09:00';

	}


	exports.isValidNums = function (nums/*Array*/, isOffLog) {

		//// 引数が配列入力でない場合を想定
		if (typeof nums !== 'object') {
			return typeof nums === 'number' && !Number.isNaN(nums);
		} else if (nums.length==0) {
			return false;
		}

		var isFail = false;
		for (var i=0; i<nums.length; i++) {
			var num = nums[i];
			// isFail = isFail || 
			// 	(num === null || num === undefined || num === '' || num === NaN || 
			// 	typeof num === 'object' || typeof num === 'boolean' || Number.isNaN(Number(num)) );
			isFail = isFail || typeof num !== 'number' || Number.isNaN(num);
		}
		if ( isFail ) {
			if ( !isOffLog ) {
				for (var i=0; i<nums.length; i++) {
					console.log('<isValidNums> Fail ----> num = '+nums[i]+' , type = '+typeof nums[i]);
				}
			}
			return false;
		} else {
			return true;
		}

		/*
		logger.w('----FALSE Patterns');
		logger.w(util.isValidNums());	
		logger.w(util.isValidNums([]));
		logger.w(util.isValidNums([[]]));
		logger.w(util.isValidNums([{}]));
		logger.w(util.isValidNums(['']));
		logger.w(util.isValidNums(['1']));
		logger.w(util.isValidNums([NaN]));
		logger.w(util.isValidNums([null]));
		logger.w(util.isValidNums([undefined]));
		logger.w(util.isValidNums([true]));
		logger.w(util.isValidNums([false]));

		logger.w('----TRUE Patterns');
		logger.w(util.isValidNums(0));
		logger.w(util.isValidNums(1.1));	
		logger.w(util.isValidNums(-1.1));		
		logger.w(util.isValidNums([0]));
		logger.w(util.isValidNums([1.1]));
		logger.w(util.isValidNums([-1.1]));
		*/

	}

	exports.isValidUrl = function (url) {
		if ( !url || /http/.test(url) == false ) return false;
		return true;
	}

	/**
	 * 金額テキストから数字のみを抽出
	 */
	exports.extractNumber = function (text) {
		if ( !text ) return text;
		return text.replace(/円/gm, '')
				   .replace(/,/gm, '')
				   .trim();
	}

	/**
	 正規表現チェック拡張
	 gmオプション付きで使用しなければフリーズする
	 */
	RegExp.prototype.check = function (target) {

		if ( !target || typeof target !== 'string') return null;

		var match;
		var res = [];
		var cp = target;

		var lastIndex = 0;
		while (match = this.exec(target)) {
			var captures = [];
			for (var i=1; i<match.length; i++) {
				if (match[i]) {
					captures.push(match[i].trim());
				}
			}

			cp = cp.replace(match[0], '');

			res.push({
				hit      : match[0].trim(),
				captures : captures
			});

			res.lastIndex = lastIndex++;
		}
		res.excluded = cp.replace(/^\s*\n/gm, '').trim();
		return res.length==0? null : res;
	}

	/**
	 全角を半角に変換
	 */
	String.prototype.toOneByte = function(){
		return this.replace(/[Ａ-Ｚ ａ-ｚ ０-９ ／]/g, function(s){
	        if(s==' '){ return s};
			return String.fromCharCode(s.charCodeAt(0)-0xFEE0);
		});
	}

//// thisはクライアント側のwindowを指す
})(typeof exports === 'undefined'? this['nodeUtil']={}: exports);
