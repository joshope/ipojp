'use strict';

(function(root){
	var param = {

		APP_PORT: 3002,
		/**
		 データベース
		*/
		DB_NAME: 'ipojp',
		DB_COLLECTION_NAME: 'item',
		DB_BACKUP_DIR: {
			// path: '/Users/7stars/Google Drive',
			// App版になりDir変更
			// 備考
			// (1) Diskマウント形式になった(App起動することでマウントされクラウドバックアップ可能になる)
			// (2) マイドライブはシンボリックリンクで実態は"/Users/7stars/Google ドライブ"
			path: '/Users/7stars/Google Drive/マイドライブ',
			alt : '/Users/7stars/Google ドライブ',
		},
		DB_BACKUP_SUBDIR: 'mongo',
		DB_BACKUP_DIR_LOCAL: '/Users/7stars/Desktop/mongo', // 2重バックアップしておくためのDIR

		DB_LOAD_DAYS: 60, // item登録日(scheduler.jsで付与)を判定して読み出す場合の日数

		/**
		 証券会社
		*/
		// SEC_NAME: 'sbi',

		/**
		 Nightmare
		*/
		NM_W: 1024,
		NM_H: 640,
		NM_WAIT: 2000, // nightmareのwait時間(ms)

/**/	NM_DISPLAY: false,

		/**
		 * Puppeteer
		 */
		BROWSER: {
			DISP: true, // フルバージョンのChromeを使用
			W: 1200,
			H: 800,
			DELAY: 10,  // puppeteerの動作速度調整
		},


		/* 再スクレイプ時の設定 */
		//// 再スクレイプ時にNM_DISPAYを強制的にTrue固定
		NM_DISPLAY_AT_RESCRAPE: true,
		//// 再スクレイプ時に通常シークエンスと異なる操作を実行(nomuraで目論見書PDFを開いて手動で操作する等)
		//// NM_DISPLAY_AT_RESCRAPE=true時のみ有効
		NM_DEBUG_AT_RESCRAPE: true,


		/*********
		* timer
		**********/
/**/	USE_TIMER: true,
		CHECK_INTERVAL_MIN: 60, // 分 (スクレイピング全行程にかかる時間よりも長く設定すること)
		// CHECK_OPEN:  ['17:00'], // OPENとCLOSEの間にあればスクレイピング（セットで複数指定可能）
		// CHECK_CLOSE: ['18:00'],
		// CHECK_SLEEP: [19, 15],  // sleep開始、終了時間
		// CHECK_OPEN:  ['6:00'], // OPENとCLOSEの間にあればスクレイピング（セットで複数指定可能）
		// CHECK_CLOSE: ['7:00'],
		// CHECK_SLEEP: [8, 4],  // sleep開始、終了時間
		CHECK_OPEN:  ['7:00'], // OPENとCLOSEの間にあればスクレイピング（セットで複数指定可能）
		CHECK_CLOSE: ['8:00'],
		CHECK_SLEEP: [9, 5],  // sleep開始、終了時間

		/**
		 serverからclientにemitして指定時間レスポンスがなければdisconnect
		 nightmareのdefault=30secよりも短く設定しないとnightmareがerrをthrowしてしまう
		*/
		SIO_ACK_TIMEOUT_MS: 20000,
		/**
		 client側でsocketの再接続が必要かチェックする間隔(acknowledgeタイムアウトでdisconnectした場合に再接続)
		*/
		SIO_RECONNECTION_CHECK_INTERVAL_MS: 5000,


		/**************************
		 お知らせ確認
		***************************/
		CHECK_NOTICE: true,

		/**************************
		 Mail
		***************************/
		REPORT_SENDER      : 'joshope76@gmail.com',
		REPORT_RECEIVERS   : ['joshope76@gmail.com'],
		REPORT_SUBJECT     : 'IPOJP',


		/**************************
		 Scheduler
		***************************/
		CAL_DESC_TAG: 'IPOJP', // Calendarのメモに追加するタグ(Appによって作成されたイベントの識別に使用)
		CAL_FETCH_DAYS: 30*6,
		IPO_FETCH_NUM: 11, // 当選管理シートから取得する行数。ある程度CalendarにEventが蓄積されたら数を増やしてもOK(年をまたぐutilの日付判定がシンプルなので初期は絞っている。例えば現在12月の場合、IPO日付の月が1月も11月も翌年と判定されるため。Calendarに蓄積された銘柄が十分にあれば過去銘柄はスクリーニングされる)
		/*
		IPO当選管理シート
		https://docs.google.com/spreadsheets/d/1Qzwioj1sR0k5hHv2EC7ApS8sm7y6WGcfKGlPfM6hz2o/edit#gid=2146677479		
		*/
		IPO_SHEET_ID: '1Qzwioj1sR0k5hHv2EC7ApS8sm7y6WGcfKGlPfM6hz2o',
		IPO_RANKS_CAL: ['S', 'A', 'B'], // カレンダー追加対象






		// IPO_RANKS_BOOKBILL: ['S', 'A', 'B', 'C'], // ブックビル対象
		IPO_RANKS_BOOKBILL: ['S', 'A', 'B', 'C', 'D'], // ブックビル対象		










		CAL_EVENT_COLORS: {
			S: 11,
			A: 5,
			B: 1,
			C: 2,
			D: 8,
			// 1:ラベンダ(薄紫)
			// 2:セージ(薄緑)
			// 3:ブドウ(濃紫)
			// 4:フラミンゴ(ピンク)
			// 5:バナナ
			// 6:みかん
			// 7:ピーコック(水色)
			// 8:グラファイト(灰色)
			// 9:ブルーベリー(濃青)
			// 10:バジル(濃緑)
			// 11:トマト
		},

	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = param;
	} else {
		root['nodeParam'] = param;
	}

//// thisはクライアント側の場合windowを指す	(clientでの読み込みはlayout.pugで行なっている)
})(this);
