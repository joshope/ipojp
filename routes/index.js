var express = require('express');
var router = express.Router();
var agent = require('../models/agent');
    

/* GET home page. */
router.get('/', function(req, res, next) {
  var summary = agent.getSummary();
  res.render('index', { title: 'IPO JP', summary: summary });
});

module.exports = router;
